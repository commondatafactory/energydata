Common Data Factory
===================

CDF's aims to create idempotent repeatable data processing solutions.
Transform raw source data into clean data ready for consumption
through various API's, Map services, vector-tile set.

Containing Dutch datasets. Maps and API's are created using tegola and mapserver

 - **nlextract BAG**
 - CBS neighborhoods
 - open energy data (kleinverbruik)
 - open energy asset data
 - Open Streetmap.
 - **Bag3D**

 - sample ETL proces loading data into postgres

Overview
========

The pattern followed is to

- Find and load source data
- Transform and combine this data into a usable complete cleaned tables
- Finally create map, api and downloads and vector-tiles ready for consumption
in various viewers. https://tvw.commondatafacotory.nl
being the most important one.

![Overview](overview-2021-10-07-1131.png)

WARNING:
--------

The documentation has rough edges and is work in progress.
You will get an idea how to run all different pieces but looking
at the actual source code and make changes works best. The refresh
rates of most datasets is very low and changes every year.
For most datasets it is quite futile to completely automate
the task.


Run
===

We use docker. As a rule of thumb:

     $docker-compose pull
     $docker-compose build
     $docker-compose up

     # tegola will fail if tables are missing.


Prepared datasets
=================

 - https://commondatafactory.nl/docs/databron

Maps
====

 - neighborhood with statistical information
 - neighborhood with energy information

example commands:

    $ docker-compose run gdal bash
    $ ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" cbs_wijken_2018_2019-10-21.gml

Load cbs wijken 2018 directly into database.

    $ docker-compose run gdal ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" -nln cbs_wijken_2018 'https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs?REQUEST=GetFeature&TYPENAME=cbs_wijken_2018&SERVICE=WFS&VERSION=2.0.0'

Convert csb data to polygons that are vectortiles compatible 3875 and Polygon instead of Multisurface.

    $ psql -h 127.0.0.1 -U cdf -d cdf -a -f cbs_to_3857.sql


Bag3D
=====

import data

Download TU delft bag 3D file.

    $ docker-compose exec bash/bag3d.sh

Convert bag3D 3875 for vector tile compatibility

    $ docker-compose exec database psql -U cdf -f sql/bag3dto3875.sql

In order to do something with BAG you probably also need nlextract.

    $ docker-compose exec database bash/nlextractbag.sh


CBS data
============

Get latest cbs data.

    $ docker-compose run gdal bash/cbs_buurt.sh

Add some usefull attributes

    $ docker-compose run python python imports/import_cbs_wijkenbuurt.py

Now if you start the mapserver a wfs layer will be served.

    $ docker-compose up mapserver

Or using ogr2ogr to download cbs wijken from 2016.

    $ docker-compose run gdal ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" -nln import.cbs_wijken_2016 'https://geodata.nationaalgeoregister.nl/wijkenbuurten2016/wfs?REQUEST=GetFeature&TYPENAME=cbs_wijken_2016&SERVICE=WFS&VERSION=2.0.0'


Tegola
======

Vector Tile Server.

You might want to seed the cache of tiles to avoid long waiting times when loading maps.

    $ docker-compose run tegola cache seed --min-zoom 12 --max-zoom 16 --config /opt/tegola_config/config_gasverbruik.toml --bounds "3.31497114423, 50.803721015, 7.09205325687, 53.5104033474"

Serving the cache files from a swift storage. You might need to extract the files and upload them.

1 - copy the tiles folder (cbs, bag, pc6).
2 - cd into the copied folder.
3 - create extracted pbf files.

    $ find . ! -name "*.pbf" -follow -type f -print0 | xargs -P 10 -0 -I % bash -c 'zcat % > %.pbf'

4 - remove source tile files

    find . ! -name "*.pbf" -follow -type f -print0 | xargs -P 10 -0 -I % bash -c 'rm %'

5 - rclone all "*.pbf" files to your cloud provider to your storage.


# Example how to run import CBS data, and transform

    docker-compose up -d database
    chmod +x ./imports/cbs/import.sh
    ./imports/cbs/import.sh
    chmod +x ./imports/cbs_2019/import.sh
    ./imports/cbs_2019/import.sh
    chmod +x ./transform/tvw_tile_layers/transform.sh
    ./transform/tvw_tile_layers/transform.sh
    chmod +x ./transform/tvw_wfs_layers/transform.sh
    ./transform/tvw_wfs_layers/transform.sh

## create backups from cbs data to restore in cluster.

    pg_dump -h 127.0.0.1 -Fc -c --if-exists -U cdf -d cdf --schema 'cbs' -s > cbs_schema_wfs.gz
    pg_dump -h 127.0.0.1 -Fc -c --if-exists -U cdf -d cdf --schema 'cbs' > cbs_wfs.gz
