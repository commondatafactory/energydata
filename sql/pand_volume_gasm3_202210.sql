/*
 * Here we combine 3dbag data with 'kleinverbruik' energy data to
 * calculate gasm3 per m2 (grondbeslag) and m3 (volume).
 *
 * Author: Stephan Preeker. 2022-10
 *
 * experimental code. proof of concept.
 *
 * The coupling of kleinverbruik data with PC6 data
 * to buildings is done using python scripts.
 *
 * The resulting Kleinverbruik data is coupled to buildings of which total surface and volume
 * determined.
 */

create index on bagvector.lod12_2d_pand (identificatie);

SELECT h_dak_50p  - h_maaiveld height, area,  ((h_dak_50p  - h_maaiveld) * area ) as volume, identificatie FROM bagvector.lod12_2d_pand;


select * from  kv.verbruikpandp6v2_2022 vp;
select data->'pandencount' pc,  * from kv.energyreports_2022 e; 


/*
 * Sum pand surfaces and volume within pc6 energy area.
 * 
 */
drop table if exists into kv.gas_verbruik_2022_grondbeslag_volume;

select 
	report_id,
	avg_gasm3,
	aansluitingen,
	--pandencount as pandencount_bag3d,
	pandencount_pc6,
	pid_count_bag3d,
	totaal_grondbeslag_m2,
	totaal_volume_m3,
	(aansluitingen * avg_gasm3) as totaal_gemiddeled_verbruik, 
	(aansluitingen * avg_gasm3 ) / totaal_grondbeslag_m2 as gasm3_m2,
	(aansluitingen * avg_gasm3 ) / totaal_volume_m3  as gasm3_m3
into kv.gas_verbruik_2022_grondbeslag_volume
from (
select 
	--*,
	--extract gas m3.
	--extract aansluitingen
	(data->'gas'->>'m3')::float avg_gasm3,
	(data->'gas'->>'aansluitingen')::int aansluitingen,
	(data->'pandencount')::int pandencount_pc6,
	--h_dak_50p  - h_maaiveld height, 
	--area,  
	--((h_dak_50p  - h_maaiveld) * area ) as volume,
	panden.*,
	bag3d_pid_count.*
from kv.energyreports_2022 e,
lateral (
	select
		report_id, 
		count(*)  as pandencount, 
		sum(area) as totaal_grondbeslag_m2,
		sum( area*(h_dak_50p - h_maaiveld )) as totaal_volume_m3
	from
		kv.verbruikpandp6v2_2022 vp
		left outer join  bagvector.lod12_2d_pand ldp on ldp.identificatie  = vp.id
		where vp.report_id = e.id
		group by vp.report_id
) panden,
lateral (
	select 
		count(distinct(id)) pid_count_bag3d
	from kv.verbruikpandp6v2_2022 vp
	left outer join  bagvector.lod12_2d_pand ldp on ldp.identificatie  = vp.id
	where vp.report_id = e.id	
) bag3d_pid_count
--where e.id like '1121%'
--where e.id = '1412JX-1412JZ'
) pc6pandenagg


select count(*) from kv.gas_verbruik_2022_grondbeslag_volume gvgv;
select count(*) from kv.energyreports_2022 e2;  -- should be equal.

create index on kv.gas_verbruik_2022_grondbeslag_volume (report_id);


/* testing above query:
 * 
 * Inspect pand surfaces and volume within pc6 energy area.
 * 
 */
select
	report_id,
	avg_gasm3,
	aansluitingen,
	--pandencount as pandencount_bag3d
	pandencount_pc6,
	pc6pandenagg.*
	--totaal_grondbeslag_m2,
	--totaal_volume_m3,
	--(aansluitingen * avg_gasm3 ) / totaal_grondbeslag_m2 as gasm3_m2,
	--(aansluitingen * avg_gasm3 ) / totaal_volume_m3  as gasm3_m3
--into kv.gas_verbruik_2021_groundbeslag_volume 
from (
select 
	--*,
	--extract gas m3.
	--extract aansluitingen
	(data->'gas'->>'m3')::float avg_gasm3,
	(data->'gas'->>'aansluitingen')::int aansluitingen,
	(data->'pandencount')::int pandencount_pc6,
	--h_dak_50p  - h_maaiveld height, 
	--area,  
	--((h_dak_50p  - h_maaiveld) * area ) as volume,
	panden.*
from kv.energyreports_2022 e,
lateral (
	select
		* --,
		--distinct(id) pid_count
		--count(*)  as pandencount, 
		--sum(area) as totaal_grondbeslag_m2,
		--sum( area*(h_dak_50p - h_maaiveld )) as totaal_volume_m3
	from
		kv.verbruikpandp6v2_2022 vp
		left outer join  bagvector.lod12_2d_pand ldp on ldp.identificatie  = vp.id
		where vp.report_id = e.id
		--group by vp.report_id
) panden
--where e.id like '1121%'
where e.id = '1412JX-1412JZ'
) pc6pandenagg

select * from kv.verbruikpandp6v2_2022 v where  v.id = '0425100000010498'

select * from kv.gas_verbruik_2022_groundbeslag_volume gvgv;