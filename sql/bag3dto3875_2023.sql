 /*  2023-05.
  *
 -- convert to 3857.

ALTER TABLE "3dbag".pand3d
 ALTER COLUMN geovlak TYPE geometry(Polygon, 3857)
  USING ST_Force2D(ST_Transform(ST_SetSRID(geovlak,28992 ), 3857));

2023-09 add missing buildings.

fix relation between bag - panden, bag3d - panden, 3d geo information.

 */

 /*  The BASIS:
  *
  *  create pand table will all current buildings,
  *  on which we combine with bag 3d heights.
  * */

@set bagschema = bag202305
@set baghelp = baghelp

select * from ${bagschema}.pandactueelbestaand p;

drop table if exists bagvector.pand;
select
        p.identificatie,
        p.bouwjaar,
        ST_Transform(ST_SetSRID(geovlak ,28992 ), 3857) as geometry,
        ST_Area(geovlak) area
into bagvector.pand
from ${bagschema}.pandactueelbestaand p;

DO $do$BEGIN EXECUTE 'COMMENT ON TABLE bagvector.pand IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;



create index on bagvector.pand (identificatie);

select count(distinct(identificatie)) from ${bagschema}.pandactueelbestaand p;
select count(distinct(identificatie)) from bagvector.pand;  -- 10633138
select count(distinct(identificatie)) from bagvector.pand_energie_2021_lod132dv3; --10633138

/* need to extract bag identifiets from bag3d */
drop table if exists bagvector.bag3dkeys;
select
        fid,
        h_maaiveld,
        kas_warenhuis,
        p.dak_type,
        Split_part (identificatie , '.', 4) as pid
into bagvector.bag3dkeys
from bag3d.pand p;

--create index on bagvector.bag3dkeys (fid);
create index on bagvector.bag3dkeys (pid);
create index on bagvector.bag3dkeys (fid);

select * from bagvector.bag3dkeys;


/*
 * Postcode 6, PC6 groups 2023.
 */
DROP TABLE IF EXISTS bagvector.pc6group_2023;

select
	--row_number() over (order by postcode) as fid,
	postcode,
        ST_Transform(ST_SetSRID(geovlak ,28992 ), 3857) as geometry
INTO bagvector.pc6group_2023
FROM ${baghelp}.pc6group_all

/*
 * Add indexes and make ready for tegola
 */

-- CREATE INDEX ON bagvector.pc6group_2023 (report_id);
create index on bagvector.pcc6group_2023 using gist (geometry);

alter table bagvector.pc6group_2023 alter column geometry type geometry(multipolygon, 3857);
ALTER TABLE bagvector.pc6group_2023 ADD COLUMN fid SERIAL PRIMARY KEY;

/*
 * Postcode 6 PC6 energie area. 2022.
 *
 * Seperate action needed where we combine Panden from pc6 to polygons.
 *
 */

DROP TABLE IF EXISTS bagvector.kv_pc6group_2022;
SELECT
        report_id,
        ST_Transform(ST_SetSRID(st_multi ,28992 ), 3857) as geometry
INTO bagvector.kv_pc6group_2022
FROM kv.pc6group_2022

CREATE INDEX ON bagvector.kv_pc6group_2022 (report_id);
create index on bagvector.kv_pc6group_2022 using gist (geometry);
alter table bagvector.kv_pc6group_2022 alter column geometry type geometry(multipolygon, 3857);
ALTER TABLE bagvector.kv_pc6group_2022 ADD COLUMN id SERIAL PRIMARY KEY;


/*
 * Adress points / standplaats/ ligplaats / verblijfsobject.
 *
 * All points of all addresses.
 */

drop table if exists bagvector.puntjes;
select
        numid,
        vid,
        pid,
        lid,
        sid,
        -- a.hoofdadres, --a."typeadresseerbaarobject",
        st_transform(st_setsrid(a.geopunt, 28992), 3857) as geometry,
        openbareruimtenaam, postcode, huisnummer, huisletter, huisnummertoevoeging,
        bouwjaar,
        oppervlakte
into bagvector.puntjes
from ${baghelp}.all_pvsl_nums a

select count(*) from bagvector.puntjes;
-- add unqique id to puntjes. since ho
--ALTER TABLE bagvector.puntjes ADD COLUMN id SERIAL PRIMARY KEY;

select a.hoofdadres, min(pid), max(pid), count(*) c
from bagvector.puntjes a
group by a.hoofdadres order by c desc;

create index on bagvector.puntjes (numid);
create index on bagvector.puntjes (vid);
create index on bagvector.puntjes (pid);
create index on bagvector.puntjes (lid);
create index on bagvector.puntjes (sid);
create index on bagvector.puntjes (postcode);

create index on bagvector.puntjes using gist (geometry);
alter table bagvector.puntjes alter column geometry type geometry(point, 3857);
select count(*) from bagvector.puntjes;

/*
 *
 * extract table lod22_2d with pand data. join 3d data with pand attributes.
 *

drop table bagvector.lod22_2d_pand;

SELECT
        --b.gid,
        --b.fid,
        pand_deel_id,
        h_dak_min, h_dak_50p, h_dak_70p, h_dak_max,
        ST_Transform(ST_SetSRID(b.geometrie ,28992 ), 3857) as geometry,
        ST_Area(b.geometrie) area,
        --p.identificatie,
        --p.oorspronkelijk_bouwjaar as bouwjaar,
        p.kas_warenhuis,
        p.h_maaiveld,
        Split_part (b.identificatie , '.', 4) as identificatie
into bagvector.lod22_2d_pand
FROM bag3d.lod22_2d b
left outer join bag3d.pand p on p.fid = b.fid;
--left outer join energiepanden e on e.identificatie = Split_part (p.identificatie , '.', 4);

/* add some indexes */
alter table bagvector.lod22_2d_pand alter column geometry type geometry(polygon, 3857);
create index on bagvector.lod22_2d_pand  using gist (geometry);
create index on bagvector.lod22_2d_pand (identificatie);

**/


/*
 * extract bag3d.lod13_2d pand
 * dataset into bagvector.
 *
 * combine pand geometry with bag3d geometry.
 *
 * 1) bag3d geometetry if not present take normal bag pand geometry.
 *
 * Now we have a COMPLETE BAG dataset containing all buildings.
 */
drop table bagvector.lod13_2d_pand;

SELECT
        -- b.gid,
        -- b.fid,
        p.identificatie,
        c.pand_deel_id,
        h_dak_min, h_dak_50p, h_dak_70p, h_dak_max,
        coalesce(
                ST_Transform(ST_SetSRID(c.geometrie ,28992 ), 3857),
                p.geometry) as geometry,
        area,
        b.kas_warenhuis,
        b.h_maaiveld
into bagvector.lod13_2d_pand
FROM bagvector.pand p
left outer join bagvector.bag3dkeys b on p.identificatie = b.pid
left outer join bag3d.lod13_2d c on c.fid = b.fid;

/* add indexes and set right geometry types */
alter table bagvector.lod13_2d_pand alter column geometry type geometry(polygon, 3857);
create index on bagvector.lod13_2d_pand  using gist (geometry);
create index on bagvector.lod13_2d_pand (identificatie);

select distinct(identificatie) from bag3d.pand

/*
drop table if exists bagvector.all_pand_lod22_2d;

/* create currently active pand database */
select
        p.identificatie,
        p.bouwjaar,
        p.area,
        p2.area as polygonarea,
        coalesce( p2.geometry , p.geometry) as geometry,
        p2.pand_deel_id,
        p2.h_dak_min, p2.h_dak_50p, p2.h_dak_70p, p2.h_dak_max,
        p2.kas_warenhuis,
        p2.h_maaiveld
into bagvector.all_pand_lod22_2d
from bagvector.pand p
left outer join bagvector.lod22_2d_pand p2 on p.identificatie = p2.identificatie

/* should be around 29 million. */
select count(*) from bagvector.all_pand_lod22_2d;

alter table bagvector.all_pand_lod22_2d alter column geometry type geometry(polygon, 3857);
create index on bagvector.all_pand_lod22_2d  using gist (geometry);
create index on bagvector.all_pand_lod22_2d (identificatie);

ALTER TABLE bagvector.all_pand_lod22_2d ADD COLUMN id SERIAL PRIMARY KEY;
*/

/* OLD.
 *
 * BAG current with all buildings outer join with old energy.
 *
 * we will use python script to work on this one?
 *
 * OLD.


drop table if exists bagvector.pand_energie;
select
        p.identificatie,
        p.bouwjaar,
        p.area,
        e.gasm3_2020,
        e.group_id_2020,
        e.kwh_aansluitingen_2020,
        e.kwh_leveringsrichting_2020,
        e.kwh_2020,
        e.gas_pct_2020,
        e.gasm3_per_m2,
        e.gasm3_per_m3,
        e.gas_pct_2020,
        e.pandcount_2020,
        e.gas_aansluitingen_2020,
        e.ean_code_count,
        e.elabel_definitief,
        e.elabel_voorlopig,
        geometry
into bagvector.pand_energie
from bagvector.pand p
left outer join export.energiepanden e on e.identificatie = p.identificatie
create index on bagvector.pand_energie (identificatie);

select count(*) from bagvector.pand_energie;
*/

/*
 *
 *  create new pand table with:
 *              - gasverbruik 01-01-2021 energie data.
 *              - energie klasse 2021-05
 *              - eancodes  2020-10
 *              - woonfunctie 2023-04
 *
 *  Prerequisistes.
 *
 *  - imported kleinverbruik energy
 *  - merged kleinverbruik with panden
 *  - calculated gas usage / volume / area.
 *  - woonfunctie calculated from bag.
 *
 *
 * to be matched with 3d data.
 *
 * Peildatum energy = 2023-01-01
 *
 */

drop table if exists bagvector.pand_energie_2023v2;
select
    p.identificatie pid,
    -- vp.id,
    p.bouwjaar,
    p.area,
    e.id as report_id,
    --
    CAST(data->'gas'->>'m3' as float)::int AS gasm3_2022,
    CAST(data->'gas'->>'aansluitingen' AS float)::int AS gas_aansluitingen_2022,
    -- CAST(data->'gas'->>'leveringsrichting' as float) as gas_leveringsrichting_2021,
    CAST(data->>'pandencount' as int) as panden,
    CAST(data->'elk'->>'Kwh' as float)::int AS kwh_2022,
    CAST(data->'elk'->>'aansluitingen' AS float) AS kwh_aansluitingen_2022,
    CAST(data->'elk'->>'leveringsrichting' as float)::int as kwh_leveringsrichting_2022,
    gvgv.totaal_grondbeslag_m2 as grondbeslag_m2_p6,
    gvgv.totaal_volume_m3 as volume_m3_p6,
    gvgv.gasm3_m2 as gasm3_grondbeslag_p6,
    gvgv.gasm3_m3 as gasm3_volume_p6,
    gvgv.totaal_gemiddeled_verbruik as gasm3_totaal_p6,
    pek.avg as energieklasse_score,
    pe.ean_codes as ean_code_count,
    pws.woonfunctie_score,
    p.geometry
        --e.data --as pandcount_2021
into bagvector.pand_energie_2023v2
from bagvector.pand p
left outer join kv.verbruikpandp6v2_2022 vp on (vp.id = p.identificatie)
left outer join kv.energyreports_2022 e on (e.id = vp.report_id)                                 ---- kv data
left outer join kv.gas_verbruik_2022_grondbeslag_volume gvgv on (gvgv.report_id = vp.report_id ) ---- gasm3 / volume, grondbeslag
left outer join ean.pand_eancodes pe on (pe.pid = p.identificatie)                               ---- ean_code_count
left outer join baghelp.pand_woonfunctie_score pws on (p.identificatie = pws.pid)                ---- woonfunctie_score
left outer join workdata.pand_energieklasse_score202307 pek on (pek.pid = p.identificatie);      ---- energieklasse

create index on bagvector.pand_energie_2023v2 (pid);
create index on bagvector.pand_energie_2023v2 using gist(geometry);


select * from bagvector.pand_energie_2023v0;

--select energieklasse_score from bagvector.pand_energie_2021 pe where energieklasse_score is not null
--order by energieklasse_score desc;

--where e.id is not null
-- check total panden involved.
select count(*) from bagvector.pand_energie_2023 pe;
-- check total energy recordes matched with panden
select count(*) from bagvector.pand_energie_2023 pe where pe.kwh_2021 is not null;
-- check amount of panden with a 'living score'
select count(*) from baghelp.pand_woonfunctie_score_202304 pws where pws.woonfunctie_score > 0;
-- check total amount of enery records made.
select count(*) from kv.verbruikpandp6v2_2021 vv ;

select *
from kv.verbruikpandp6v2_2021 vv
join kv.energyreports_2021 e

select * from bagvector.pand;

--create index on bagvector.pand_energie_2021  using gist (geometry);
--create index on bagvector.pand_energie_2021 (identificatie);

select count(*) from bagvector.pand_energie_2021 pe;

drop table if exists bagvector.all_pand_energie_2021_lod13_2d;

/*
 * Combineer energie verbruik with bag3d panden. lod132d.
 * Combineer relevante CBS gegevens met panden voor DEGO.
 * 2023-10
 */
drop table if exists bagvector.pand_energie2021_cbs2020_lod132dv4;
select
        ldp.identificatie,
        pe.bouwjaar,
        pe.area,
        ldp.area as polygonarea,
        ldp.kas_warenhuis,
        ldp.geometry,
        h_dak_50p  - ldp.h_maaiveld as hoogte,
        pe.gasm3_2022,
        pe.gas_aansluitingen_2022,
        pe.report_id as group_id_2022,
        pe.panden as pandcount_2022,
        pe.kwh_aansluitingen_2022,
        pe.kwh_2022,
        pe.kwh_leveringsrichting_2022,
        pe.woonfunctie_score,
        pe.ean_code_count,
        pe.grondbeslag_m2_p6,
        pe.volume_m3_p6,
        pe.gasm3_grondbeslag_p6,
        pe.gasm3_volume_p6,
        pe.gasm3_totaal_p6,
        pe.energieklasse_score,
        cbs.pc6,
        cbs.wozwoning as woz,
        cbs.uitkminaow,
        cbs.p_koopwon,
        cbs.p_huurwon,
        cbs.won_hcorp,
        cbs.won_nbew,
        cbs.inwoner,
        cbs.inw_65pl,
        cbs.inw_4564,
        cbs.inw_2544,
        cbs.inw_1524,
        cbs.inw_014
into bagvector.pand_energie2021_cbs2020_lod132dv4
from bagvector.lod13_2d_pand ldp -- base is every building. bag3d + missing buildings.
left outer join bagvector.pand_energie_2023v0 pe on ldp.identificatie = pe.pid
left outer join cbspostcode.pand_cbs_p6 cbs on cbs.pid = pe.pid

create index on bagvector.pand_energie2021_cbs2020_lod132dv4  using gist (geometry);
create index on bagvector.pand_energie2021_cbs2020_lod132dv4 (identificatie);

/*
 * Combine cbs pc6 with panden 2022
 *
 */

drop table if exists bagvector.pand_2023_cbs_2022_cbs_lod132dv4;

create table bagvector.pand_2023_cbs_2022_cbs_lod132dv4  as (
select
    ldp.identificatie,
    -- bouwjaar,
    -- pe.area,
    -- ldp.area as polygonarea,
    ldp.kas_warenhuis,
    ldp.geometry,
    h_dak_50p  - ldp.h_maaiveld as hoogte,
    --pes.avg as energieklasse_score,
    pev.energieklasse_score,
    pev.gasm3_2022,
    pev.gasm3_grondbeslag_p6,
    pev.kwh_leveringsrichting_2022,
    pev.bouwjaar,
    pev.woonfunctie_score,
    cbs.postcode,
    cbs.aantal_inwoners,
    cbs.aantal_mannen,
    cbs.aantal_vrouwen,
    cbs.aantal_inwoners_0_tot_15_jaar,
    cbs.aantal_inwoners_15_tot_25_jaar,
    cbs.aantal_inwoners_25_tot_45_jaar,
    cbs.aantal_inwoners_45_tot_65_jaar,
    cbs.aantal_inwoners_65_jaar_en_ouder,
    cbs.percentage_geb_nederland_herkomst_nederland,
    cbs.percentage_geb_nederland_herkomst_overig_europa,
    cbs.percentage_geb_nederland_herkomst_buiten_europa,
    cbs.percentage_geb_buiten_nederland_herkomst_europa,
    cbs.percentage_geb_buiten_nederland_herkmst_buiten_europa,
    cbs.aantal_part_huishoudens,
    cbs.aantal_eenpersoonshuishoudens,
    cbs.aantal_meerpersoonshuishoudens_zonder_kind,
    cbs.aantal_eenouderhuishoudens,
    cbs.aantal_tweeouderhuishoudens,
    cbs.gemiddelde_huishoudensgrootte,
    cbs.aantal_woningen,
    cbs.aantal_woningen_bouwjaar_voor_1945,
    cbs.aantal_woningen_bouwjaar_45_tot_65,
    cbs.aantal_woningen_bouwjaar_65_tot_75,
    cbs.aantal_woningen_bouwjaar_75_tot_85,
    cbs.aantal_woningen_bouwjaar_85_tot_95,
    cbs.aantal_woningen_bouwjaar_95_tot_05,
    cbs.aantal_woningen_bouwjaar_05_tot_15,
    cbs.aantal_woningen_bouwjaar_15_en_later,
    cbs.aantal_meergezins_woningen,
    cbs.percentage_koopwoningen,
    cbs.percentage_huurwoningen,
    cbs.aantal_huurwoningen_in_bezit_woningcorporaties,
    cbs.aantal_niet_bewoonde_woningen,
    cbs.gemiddelde_woz_waarde_woning as woz,
    kc.g_wozbag as gemeente_woz,
    cbs.aantal_personen_met_uitkering_onder_aowlft
    --  geom
from bagvector.lod13_2d_pand ldp -- base is every building, bag3d + missing buildings.
left outer join ${baghelp}.pand_postcode pp on pp.pid = ldp.identificatie
left outer join cbspostcode.cbs_pc6_2022 cbs on cbs.postcode = pp.postcode
left outer join bagvector.pand_energie_2023v0 pev on (pev.pid = ldp.identificatie)
left outer join ${baghelp}.pand_gemcode_p6 pp6 on (pp6.pid=pp.pid and pp6.postcode=pp.postcode)
left outer join import.kwb_cbs_2022 kc on (kc.gwb_code = pp6.gemeentecode)
);

select count(*) from bagvector.pand_energie_2023v0 pev;
select count(*) from bagvector.pand_energie_20

select count(*) from bagvector.pand_2023_cbs_2022_cbs_lod132dv4;

select * from bagvector.pand_2023_cbs_2022_cbs_lod132dv4 pccld;
--where cbs.postcode like '1024LH';
create index on bagvector.pand_2023_cbs_2022_cbs_lod132dv4 using gist (geometry);
create index on bagvector.pand_2023_cbs_2022_cbs_lod132dv4 (postcode);
-- add serial key needed for tegola.
ALTER TABLE bagvector.pand_2023_cbs_2022_cbs_lod132dv4  ADD COLUMN fid SERIAL PRIMARY KEY;
--

/*
 * Combine cbs pc6 with panden 2021
 *
 */

drop table if exists bagvector.pand_2023_cbs_2021_cbs_lod132dv4;
create table bagvector.pand_2023_cbs_2021_cbs_lod132dv4  as (
select
    ldp.identificatie,
    -- bouwjaar,
    -- pe.area,
    -- ldp.area as polygonarea,
    ldp.kas_warenhuis,
    ldp.geometry,
    h_dak_50p  - ldp.h_maaiveld as hoogte,
    -- ldp.bouwjaar,
    pes.avg as energie_klasse_score,
    cbs.postcode,
    cbs.aantal_inwoners,
    cbs.aantal_mannen,
    cbs.aantal_vrouwen,
    cbs.aantal_inwoners_0_tot_15_jaar,
    cbs.aantal_inwoners_15_tot_25_jaar,
    cbs.aantal_inwoners_25_tot_45_jaar,
    cbs.aantal_inwoners_45_tot_65_jaar,
    cbs.aantal_inwoners_65_jaar_en_ouder,
    cbs.aantal_geboorten,
    cbs.percentage_nederlandse_achtergrond,
    cbs.percentage_westerse_migr_achtergr,
    cbs.percentage_niet_westerse_migr_achtergr,
    cbs.aantal_part_huishoudens,
    cbs.aantal_eenpersoonshuishoudens,
    cbs.aantal_meerpersoonshuishoudens_zonder_kind,
    cbs.aantal_eenouderhuishoudens,
    cbs.aantal_tweeouderhuishoudens,
    cbs.gemiddelde_huishoudensgrootte,
    cbs.aantal_woningen,
    cbs.aantal_woningen_bouwjaar_voor_1945,
    cbs.aantal_woningen_bouwjaar_45_tot_65,
    cbs.aantal_woningen_bouwjaar_65_tot_75,
    cbs.aantal_woningen_bouwjaar_75_tot_85,
    cbs.aantal_woningen_bouwjaar_85_tot_95,
    cbs.aantal_woningen_bouwjaar_95_tot_05,
    cbs.aantal_woningen_bouwjaar_05_tot_15,
    cbs.aantal_woningen_bouwjaar_15_en_later,
    cbs.aantal_meergezins_woningen,
    cbs.percentage_koopwoningen,
    cbs.percentage_huurwoningen,
    cbs.aantal_huurwoningen_in_bezit_woningcorporaties,
    cbs.aantal_niet_bewoonde_woningen,
    cbs.gemiddelde_woz_waarde_woning,
    cbs.gemiddeld_gasverbruik_woning,
    cbs.gemiddeld_elektriciteitsverbruik_woning,
    cbs.aantal_personen_met_uitkering_onder_aowlft
    --  geom
from bagvector.lod13_2d_pand ldp -- base is every building, bag3d + missing buildings.
left outer join ${baghelp}.pand_postcode pp on pp.pid = ldp.identificatie
left outer join cbspostcode.cbs_pc6_2021 cbs on cbs.postcode = pp.postcode
left outer join workdata.pand_energieklasse_score202307 pes on pes.pid = ldp.identificatie
);


--where cbs.postcode like '1024LH';
create index on bagvector.pand_2023_cbs_2021_cbs_lod132dv4 using gist (geometry);
create index on bagvector.pand_2023_cbs_2021_cbs_lod132dv4 (postcode);
-- add serial key needed for tegola.
ALTER TABLE bagvector.pand_2023_cbs_2021_cbs_lod132dv4  ADD COLUMN fid SERIAL PRIMARY KEY;
--



/*
 * Combine pand (energie+) with bag3d panden. lod22_2d.
 *

drop table if exists bagvector.pand_energie_2021_lod222d;
select
        ldp.identificatie,
        pe.bouwjaar,
        pe.area,
        ldp.area as polygonarea,
        ldp.kas_warenhuis,
        ldp.geometry,
        h_dak_50p  - ldp.h_maaiveld as hoogte,
        --- kv energie
        pe.gasm3_2021,
        pe.gas_aansluitingen_2021,
        pe.report_id as group_id_2021,
        pe.panden as pandcount_2021,
        pe.kwh_aansluitingen_2021,
        pe.kwh_2021,
        pe.kwh_leveringsrichting_2021,
        ---
        pe.woonfunctie_score,
        ---
        pe.ean_code_count,
        ---
        pe.energieklasse_score::int
into bagvector.pand_energie_2021_lod222d
from bagvector.lod22_2d_pand ldp -- base is every building.
left outer join bagvector.pand_energie_2021 pe on ldp.identificatie = pe.pid

create index on bagvector.pand_energie_2021_lod222d  using gist (geometry);
create index on bagvector.pand_energie_2021_lod222d (identificatie);
*/

--show all


/*
 * Combine pand (energie+) with bag3d panden. lod13_2d.
 *
 */
drop table if exists bagvector.pand_energie_2021_lod132d;
select
        ldp.identificatie,
        pe.bouwjaar,
        pe.area,
        ldp.area as polygonarea,
        ldp.kas_warenhuis,
        ldp.geometry,
        h_dak_50p  - ldp.h_maaiveld as hoogte,
        --- kv energie
        pe.gasm3_2021,
        pe.gas_aansluitingen_2021,
        pe.report_id as group_id_2021,
        pe.panden as pandcount_2021,
        pe.kwh_aansluitingen_2021,
        pe.kwh_2021,
        pe.kwh_leveringsrichting_2021,
        ---
        pe.woonfunctie_score,
        ---
        pe.ean_code_count,
        ---
        pe.energieklasse_score::int
into bagvector.pand_energie_2021_lod132d
from bagvector.lod13_2d_pand ldp -- base is every building.
left outer join bagvector.pand_energie_2021 pe on ldp.identificatie = pe.pid

create index on bagvector.pand_energie_2021_lod132d  using gist (geometry);
create index on bagvector.pand_energie_2021_lod132d (identificatie);


select count(*) from bagvector.pand_energie_2021_lod132d pe;
select count(*) from bag3d.lod13_2d ld;
select count(*) from bagvector.pand;
select count(*) from bagvector.lod12_2d_pand ldp;

/*
drop table if exists bagvector.all_pand_energie_2020_lod22_2d;
select
        p.identificatie,
        p.bouwjaar,
        p.area,
        p2.area as polygonarea,
        coalesce( p2.geometry , p.geometry) as geometry,
        p2.pand_deel_id,
        p2.h_dak_min, p2.h_dak_50p, p2.h_dak_70p, p2.h_dak_max,
        p2.kas_warenhuis,
        p2.h_maaiveld,
        gasm3_2020,
        group_id_2020,
        kwh_aansluitingen_2020,
        kwh_leveringsrichting_2020,
        kwh_2020,
        gas_pct_2020,
        gasm3_per_m2,
        gasm3_per_m3,
        pandcount_2020,
        gas_aansluitingen_2020,
        -- ean_code_count,
        -- elabel_voorlopig,
        -- elabel_definitief
into bagvector.all_pand_energie_2020_lod22_2d
from  bagvector.all_pand_lod22_2d p2
left outer join  bagvector.pand_energie p on p.identificatie = p2.identificatie
*/

/* should be around 29 million. */
select count(*) from bagvector.all_pand_energie_2020_lod22_2d;

alter table bagvector.all_pand_energie_2020_lod22_2d alter column geometry type geometry(polygon, 3857);
create index on bagvector.all_pand_energie_2020_lod22_2d  using gist (geometry);
create index on bagvector.all_pand_energie_2020_lod22_2d (identificatie);
ALTER TABLE bagvector.all_pand_energie_2020_lod22_2d ADD COLUMN id SERIAL PRIMARY KEY;

create index on export.energiepanden (identificatie);

select p.area, p.geometry, pw.woonfunctie_score, e.*
into export.nienedenhaagv2
from bagvector.pand p
left outer join export.energiepanden e on e.identificatie = p.identificatie
left outer join baghelp.pand_woonfunctie pw on pw.pid = p.identificatie
where p.geometry && st_makeenvelope(457322.7402,6800564.1880,499477.7613,6826743.8702)
