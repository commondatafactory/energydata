select count(*) from kv.verbruikpandp6v2_2022 vv;
select count(*) from kv.verbruikpandp6v2_2021 v;

select count(*) from kv.kleinverbruik_2022 k where productsoort = 'GAS';

select k.netbeheerder, k.woonplaats, count(*)
from kv.kleinverbruik_2021 k
left outer join kv.kleinverbruik_2022 kk on (k.woonplaats  = kk.woonplaats  and kk.productsoort = 'ELK')
where kk.woonplaats  is null and k.productsoort = 'ELK'
group by k.woonplaats, k.netbeheerder;


select * from kv.verbruikpandp6v2_2021 vv;


select * from kv.kleinverbruik_2022 k;


select * from kv.verbruikpandp6v2_2022;

analyze;


select * from

select
	ldp.identificatie,
	pe.bouwjaar,
	pe.area,
	--ldp.area as polygonarea,
	ldp.kas_warenhuis,
	ldp.geometry,
	h_dak_50p  - h_maaiveld as hoogte,

	pe.gasm3_2022,
	pe.gas_aansluitingen_2022,
	pe.report_id as group_id_2022,
	pe.panden as pandcount_2022,
	pe.kwh_aansluitingen_2022,
	pe.kwh_2022,
	pe.kwh_leveringsrichting_2022,
	pe.ean_code_count,
	pe.grondbeslag_m2_p6,
        pe.volume_m3_p6,
        pe.gasm3_grondbeslag_p6,
        pe.gasm3_volume_p6,
        pe.gasm3_totaal_p6,
	pe.energieklasse_score,
	pe.energieklasse_score::int as energie_label,
	pe.woonfunctie_score,

	cbs.pc6,
	cbs.inwoner,
	cbs.man,
	cbs.vrouw,
	cbs.inw_014,
	cbs.inw_1524,
	cbs.inw_2544,
	cbs.inw_4564,
	cbs.inw_65pl,
	cbs.p_nl_achtg,
	cbs.p_nw_mig_a,
	cbs.aantal_hh,
	cbs.tothh_eenp,
	cbs.tothh_mpzk,
	cbs.hh_eenoud,
	cbs.hh_tweeoud,
	cbs.gem_hh_gr,
	cbs.woning,
	cbs.wonvoor45,
	cbs.won_4564,
	cbs.won_6574,
	cbs.won_7584,
	cbs.won_8594,
	cbs.won_9504,
	cbs.won_0514,
	cbs.won_1524,
	cbs.won_mrgez,
	cbs.p_koopwon,
	cbs.p_huurwon,
	cbs.won_hcorp,
	cbs.won_nbew,
	cbs.wozwoning,
	cbs.uitkminaow,
	pc.buurtnaam,
	pc.buurtcode,
	pc.wijkcode,
	pc.gemeentecode,
	pc.gemeentenaam
	into export.pand_energie2022_cbs2020_lod132dv0_NL
from bagvector.lod13_2d_pand ldp
left outer join bagvector.pand_energie_2022v0 pe on ldp.identificatie = pe.pid
--left outer join bagvector.pand_energie_2021_lod132dv3 pe on pe.identificatie  = ldp.identificatie
left outer join cbspostcode.pand_cbs_p6 cbs on cbs.pid = pe.pid
left outer join baghelp.pand_buurt_wijk_gemeente_provincie pc on pc.pid = pe.pid
where (h_dak_50p  - ldp.h_maaiveld) > 2 --and polygonarea > 4
--limit 300

select count(distinct identificatie) from export.pand_energie2022_cbs2020_lod132dv0_nl;
select count(*) from export.pand_energie2022_cbs2020_lod132dv0_nl pecldn ;




