

/* match bovag garages with BAG using leveshtein */
drop table if exists bovag.garages202310;
select
	a.pid,
	a.vid,
	a.sid,
	a.lid,
	a.numid,
	a.huisnummer, a.huisletter, a.huisnummertoevoeging,
	a.postcode,
	st_transform(a.geopunt, 3857) bagpunt,
	a.oppervlakte,
	lidid::bigint as lidid,
	address,
	match_score,
	substring(address from '[0-9]+.*$') as bovag_toevoeging,
	concat(a.huisnummer, lower(a.huisletter) , lower(a.huisnummertoevoeging)) bag_toevoeging,
	telephone,
	emailaddress,
	url,
	namecorrespondance as name,
	city as bovag_plaats,
	zipcode as bovag_postcode,
	address as bovag_adres,
	st_transform(st_setsrid(st_point(b.longitude::float, b.latitude::float), 4326), 3857) as geometry
into bovag.garages202310
from import.bovag b,
lateral ( select
		--coalesce (pid, sid, lid) as matched_id,
		pa.pid,
		pa.numid,
		pa.vid,
		pa.sid,
		pa.lid,
		pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
		pa.postcode,
		pa.oppervlakte,
		levenshtein(concat(pa.huisnummer, lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(replace(trim(substring(address from '[0-9]+.*$')), ' ', ''))) match_score,
		pa.geopunt
	        from baghelp.all_pvsl_nums pa
	        where pa.postcode = replace(b.zipcode, ' ', '')
	        -- and pa.huisnummer = r.huisnummerint
	        order by levenshtein(concat(pa.huisnummer, lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(replace(trim(substring(address from '[0-9]+.*$')), ' ', '')))  asc
	        limit 1
) a
--where b.zipcode = '5628 RA'

-- CREATE INDEX ON bovag.garages202310 (id);
CREATE index on bovag.garages202310 using gist (geometry);
ALTER table bovag.garages202310 ALTER column geometry type geometry(point, 3857);
ALTER TABLE bovag.garages202310 ADD COLUMN id SERIAL PRIMARY KEY;

/*
 * Dataselection export bovag.
 */

  select
	pid,
	vid,
	sid,
	lid,
	numid,
	huisnummer, huisletter, huisnummertoevoeging,
	postcode,
	oppervlakte,
	lidid,
	address,
	match_score,
	bovag_toevoeging,
	bag_toevoeging,
	telephone,
	emailaddress,
	url,
	name,
	bovag_plaats,
	bovag_postcode,
	bovag_adres,
	st_transform(geometry, 4326) as geometry
	into bovag.garages_lambda
	from bovag.garages202310


--select * from bovag.garages202310v2 gv where postcode is null

-- where
-- gv.postcode = ;
-- select count(*) from bovag.garages202310;
-- select * from bovag.garages202310 g;

/* test address matching with bag */
--select replace(zipcode, ' ', '') postcode, substring(address from '[0-9]+.*$') as toevoegging from bovag.garages202310 g;

--select * from bovag.garages202310 g where zipcode = '3764 CG';

drop table if exists bovag.rdw202310;
select
	trim(r.postcode_numeriek || r.postcode_alfanumeriek) as postcode,
	r.huisnummer::int as huisnummerint,
	*
	into bovag.rdw202310
from import.rdw r;

create index on bovag.rdw202310 (postcode);
create index on bovag.rdw202310 (huisnummerint);

select * from bovag.rdw202310 r where r.huisnummer_toevoeging = '';

select count(*) from import.rdw;

drop table if exists bovag.rdw_levenshtein_matched;
select
    --matched_id,
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	geopunt,
	match_score
into bovag.rdw_levenshtein_matched
from (
	select  b.huisnummer, b.huisletter, b.huisnummertoevoeging,
			r.volgnummer,
			r.gevelnaam,
			r.naam_bedrijf,
			r.straat,
			r.plaats,
			r.api_bedrijf_erkenningen,
			--b.matched_id,
			b.pid,
			b.numid,
			b.vid,
			b.sid,
			b.lid,
			b.oppervlakte,
			b.geopunt,
			b.postcode,
			r.huisnummer || ' ' || huisnummer_toevoeging as rdw_toevoeging,
	        levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), lower(huisnummer_toevoeging)) match_score
	--from bovag.rdw_not_direct_matched r,
	from bovag.rdw202310 r,
	lateral ( select
				--coalesce (pid, sid, lid) as matched_id,
				pa.pid,
				pa.numid,
				pa.vid,
				pa.sid,
				pa.lid,
				pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
				pa.postcode,
				pa.oppervlakte,
				pa.geopunt
	          from baghelp.all_pvsl_nums pa
	          where pa.postcode = r.postcode
	          and pa.huisnummer = r.huisnummerint
	          order by levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(r.huisnummer_toevoeging)) limit 1
	) b
	--where r.postcode is not null and r.huisnummerint is not null and r.huisnummer_toevoeging != ''
) p
--where matched_id is not null;

/*
 * Not Matched with levenshtein.
 */
drop table if exists bovag.rdw_nobaghit;
select r.*
into bovag.rdw_nobaghit
from bovag.rdw202310 r
left outer join bovag.rdw_levenshtein_matched r2 on (r.huisnummerint=r2.huisnummer and r.postcode=r2.postcode)
where r2.huisnummer is null;

drop table if exists bovag.rdw_levenshtein_matched_no_bag;
/* now match nearby addresses  ~ 320 */
select
    --matched_id,
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	geopunt,
	match_score
into bovag.rdw_levenshtein_matched_no_bag
from (
	select  b.huisnummer, b.huisletter, b.huisnummertoevoeging,
			r.volgnummer,
			r.gevelnaam,
			r.naam_bedrijf,
			r.straat,
			r.plaats,
			r.api_bedrijf_erkenningen,
			--b.matched_id,
			b.pid,
			b.numid,
			b.vid,
			b.sid,
			b.lid,
			b.oppervlakte,
			b.geopunt,
			b.postcode,
			r.huisnummer || ' ' || huisnummer_toevoeging as rdw_toevoeging,
	        levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), lower(huisnummer_toevoeging)) + 10 match_score
	from bovag.rdw_nobaghit r,
	lateral ( SELECT * FROM (
				select * from (
				select
				--coalesce (pid, sid, lid) as matched_id,
				pa.pid,
				pa.numid,
				pa.vid,
				pa.sid,
				pa.lid,
				pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
				pa.postcode,
				pa.oppervlakte,
				pa.geopunt
	          from baghelp.all_pvsl_nums pa
	          where pa.postcode = r.postcode
	          and pa.huisnummer < r.huisnummerint
	          order by pa.huisnummer desc limit 1-- levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(r.huisnummer_toevoeging)) limit 1
			  ) lowerhuisnummer
			  UNION
			  SELECT * FROM (
				select
				--coalesce (pid, sid, lid) as matched_id,
				pa.pid,
				pa.numid,
				pa.vid,
				pa.sid,
				pa.lid,
				pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
				pa.postcode,
				pa.oppervlakte,
				pa.geopunt
	          from baghelp.all_pvsl_nums pa
	          where pa.postcode = r.postcode
	          and pa.huisnummer > r.huisnummerint
	          order by pa.huisnummer asc limit 1-- levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(r.huisnummer_toevoeging)) limit 1
			  ) higherhuisnummer
		limit 1) nearbyadres
	) b
) x

drop table if exists bovag.rdw_bag_matched_202310;

select
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	st_transform(geopunt, 3857) geometry,
	match_score
into bovag.rdw_bag_matched_202310
from (
select * from bovag.rdw_levenshtein_matched rlm
union
select * from bovag.rdw_levenshtein_matched_no_bag rlmnb
) allmatches

create index on bovag.rdw_bag_matched_202310 using gist(geometry);
alter table bovag.rdw_bag_matched_202310 alter column geometry type geometry(point, 3857);
ALTER TABLE bovag.rdw_bag_matched_202310 ADD COLUMN id SERIAL PRIMARY KEY;

/*
 *
 * Analyze / test dataset.
 */

select count(*) from bovag.rdw_levenshtein_matched rlm where rlm.match_score > 1;

select * from baghelp.all_pvsl_nums apn where postcode = '1822BT' and huisnummer=58;
select * from baghelp.all_pvsl_nums apb where postcode = '9983PM';

select * from bagv2_032022.nummeraanduidingactueelbestaand n where postcode = '1822BT' and huisnummer = 58;

vacuum bovag.rdw202310;

select * from bovag.rdw202310 r where r.huisnummer_toevoeging != '';

select count(*) from bovag.rdw_levenshtein_matched rlm;

select count(*) from baghelp.all_pvsl_nums;

select count(*) from bagv2_062022.nummeraanduiding n where postcode = '5628RE' ;

/*
 *  Data selection exports.
 *
 */
drop table if exists bovag.rdw_bag_matched_202310_lambda;

select
    id,
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	st_transform(geometry, 4326) geometry,
	match_score
into bovag.rdw_lambda
from bovag.rdw_bag_matched_202310 rbm;


