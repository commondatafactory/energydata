/* steps to create energy label database. Adding energie label scores to buildings.
 *
 * Assumes labels are loaded with provided python script into 'energielabel_definitie'
 * and 'energielabel_voorlopig'.
 *
 * should be about 3.8 and 9 million rows.
 *
 * A small amount of labels is no longer valid since we cannot couple them anymore
 * to existing buildings.
 *
 * TODO rerunnable etl script.
 * */

drop table if exists pand_vbo_nums;

/* intermediate table that relates panden - vbo - nummeraanduiding  - pvn
 * create a key that is usable with to compare with enery labels.
 * */
select
	p.identificatie as pid,
	vbo.identificatie as vid,
	na.identificatie as numid,
	na.postcode,
	na.huisnummer,
	na.huisletter,
	na.huisnummertoevoeging,
	na.gerelateerdeopenbareruimte,
	upper(concat(na.postcode, ' ', na.huisnummer, na.huisletter, na.huisnummertoevoeging)) as ekey
into pand_vbo_nums
from bag.pandactueelbestaand p
  left join bag.verblijfsobjectpandactueelbestaand vbop on  (vbop.gerelateerdpand = p.identificatie)
  left join bag.verblijfsobjectactueelbestaand vbo on (vbo.identificatie = vbop.identificatie)
  left join bag.nummeraanduidingactueelbestaand na on (na.identificatie = vbo.hoofdadres)

/* create new table that combines pvn data with labels result should be around ~8.200.000 rows*/
drop table if exists baghelp.pand_vbo_nums_labels;

select
     pid,
     vid,
     numid,
     pvn.postcode,
     pvn.huisnummer,
     pvn.ekey,
     coalesce (ed."WONING_TYPE", ev."WONING_TYPE" ) as woning_type,
     case
      when ev."VOORL_BEREKEND" = 'A' then 1
      when ev."VOORL_BEREKEND" = 'B' then 2
      when ev."VOORL_BEREKEND" = 'C' then 3
      when ev."VOORL_BEREKEND" = 'D' then 4
      when ev."VOORL_BEREKEND" = 'E' then 5
      when ev."VOORL_BEREKEND" = 'F' then 6
      when ev."VOORL_BEREKEND" = 'G' then 7
      when ev."VOORL_BEREKEND" = 'H' then 8
      else 0
    end as labelscore_voorlopig,
    case
      when ed."LABEL" = 'A' then 1
      when ed."LABEL" = 'B' then 2
      when ed."LABEL" = 'C' then 3
      when ed."LABEL" = 'D' then 4
      when ed."LABEL" = 'E' then 5
      when ed."LABEL" = 'F' then 6
      when ed."LABEL" = 'G' then 7
      when ed."LABEL" = 'H' then 8
      else 0
    end as labelscore_definitief
into baghelp.pand_vbo_nums_labels
from baghelp.pand_vbo_nums  pvn
left outer join energielabel_definitief ed
  	on (
  	   upper(concat(lpad(ed."POSTCODE_WONING"::text, 6, '0'), ' ', ed."HUISNUMMER_WONING", ed."HUISNUMMER_TOEV_WONING")) = pvn.ekey
  	)
left outer join energielabel_voorlopig ev
  	on (
  	   upper(concat(lpad(ev."POSTCODE_WONING"::text, 6, '0'), ' ', ev."HUISNUMMER_WONING", ev."HUISNUMMER_TOEV_WONING")) = pvn.ekey
  	)
where (ed."LABEL" is not null or ev."VOORL_BEREKEND" is not null)



drop table if exists pand_labelscore_definitief

select
  pid,
  array_agg(labelscore_definitief) as labels_definitief,
  avg(labelscore_definitief) as labelscore_definitief,
  count(labelscore_definitief) as label_count_definitief,
  array_agg(distinct(woning_type)) as woning_types
into pand_labelscore_definitief
from pand_vbo_nums_labels pvnl
where labelscore_definitief > 0
group by pvnl.pid
/* sanity checks that where done */

drop table if exists pand_labelscore_voorlopig

select
  pid,
  array_agg(labelscore_voorlopig) as labels_voorlopig,
  avg(labelscore_voorlopig) as labelscore_voorlopig,
  count(labelscore_voorlopig) as label_count_voorlopig,
  array_agg(distinct(woning_type)) as woning_types
into pand_labelscore_voorlopig
from pand_vbo_nums_labels pvnl
where labelscore_voorlopig > 0
group by pvnl.pid



alter table "3dbag".energiepanden add column if not exists elabel_definitief int;
alter table "3dbag".energiepanden add column if not exists elabel_voorlopig int;

UPDATE "3dbag".energiepanden p
SET elabel_definitief = s.labelscore_definitief::int
FROM pand_labelscore_definitief s
WHERE p.identificatie = s.pid;


UPDATE "3dbag".energiepanden p
SET elabel_voorlopig = s.labelscore_voorlopig::int
FROM pand_labelscore_voorlopig s
WHERE p.identificatie = s.pid;

/* check which labels do not have a num / vbo / pand relation  73.665 */
/*
select
	count(*)
from
	pand_vbo_nums  pvn
full outer join energielabel_definitief ed
  	on (
  	   upper(concat(lpad(ed."POSTCODE_WONING"::text, 6, '0'), ' ', ed."HUISNUMMER_WONING", ed."HUISNUMMER_TOEV_WONING")) = pvn.ekey
  	)
where pvn.ekey is null

*/
/* check which labels do not have a num / vbo / pand relation  70.000 niet te koppelen aan actuele numeraanduidingen */
/*
select
	count(*)
from
	bag.nummeraanduidingactueelbestaand na
full outer join energielabel_definitief ed
  	on (
  	   upper(concat(lpad(ed."POSTCODE_WONING"::text, 6, '0'), ' ', ed."HUISNUMMER_WONING", ed."HUISNUMMER_TOEV_WONING")) = upper(concat(na.postcode, ' ', na.huisnummer, na.huisletter, na.huisnummertoevoeging))
  	)
where na.postcode is null
*/
