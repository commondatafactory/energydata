select count(distinct(insolventienummer, rechtbank, publicatie, kenmerk, title)) from cir.reports r;


select count(*) from cir.reports r where eindverslag == 'true';

delete from cir.reports where id in (
	select id from (
		select *, 
			   RANK() over ( partition by insolvencynumber, court order by kenmerk, "publication", title, end_report) dupes
		from cir.reports r
	) addrank
	where dupes > 1 or "publication" <  NOW() - interval '6 month'
)

select * from (
		select *, 
			   RANK() over ( partition by insolvencynumber  order by created_at) dupes
		from cir.report_insolvency_codes ric
) bla where dupes > 1 order by dupes desc 


select count(*) from cir.report_insolvency_codes ric;


select * from cir.reports r;

select * from cir.reports r;

select count(*) from cir.report_insolvency_codes ric;
select count(*) from cir.reports r;

delete from cir.report_insolvency_codes where report_id in (
select report_id 
from cir.report_insolvency_codes ric 
left outer join cir.reports r on (r.id = ric.report_id)
where r.id is null);

-- take the newest.
update cir.report_insolvency_codes ric
set case_id = (select id from cir.raw_case rc where rc.insolvency_id = ric.insolvency_id order by rc.created_at desc limit 1)
where case_id = 0 or case_id is null;


select * from cir.raw_case;


select id, created_at from cir.raw_case rc order by created_at desc limit 1


select 
	rank() over ( partition by insolvency_id order by created_at ) dupes,
 	* 
from cir.raw_case order by insolvency_id;

select max(created_at), min(created_at), insolvency_id , count(insolvency_id) from cir.raw_case rc group by insolvency_id
--having count('insolvency_id')
order by insolvency_id;

select * from cir.report_insolvency_codes ric where case_id = 0;

select count(*) from cir.report_insolvency_codes ric  where case_id is null or case_id = 0;

select count(distinct(insolvency_id)) from cir.reports r;

select count(*) from cir.reports r;
select count(*) from cir.raw_case rc;

select count(distinct(insolvency_id)) from cir.raw_case rc;

select * from cir.reports r order by "publication" desc;

select  * from cir.reports r;
select  * from cir.report_insolvency_codes ric;



select * from ( 
	select count(*) as dupes, insolvency_id from cir.raw_case group by insolvency_id
) dup where dupes > 1 order by dupes desc;



update cir.raw_case i set publication = (select max(publication) pub from cir.reports r where r.insolvency_id  = i.insolvency_id group by insolvency_id order by pub desc limit 1)
-- where "publication" is null


select * from cir.raw_case rc;


delete from cir.raw_case where id in (
	select id from (
		select *, 
			   RANK() over ( partition by insolvency_id , publication order by id) dupes
		from cir.raw_case r
	) addrank
	where dupes > 1 or "publication" <  NOW() - interval '6 month'
)

	select *, 
			   RANK() over ( partition by insolvency_id , publication order by id) dupes
		from cir.raw_case r

		
select * from (	
select count(*) dupes, case_id , insolvency_id  from cir.report_insolvency_codes ric  group by ric.case_id , insolvency_id
) dup where dupes > 1


delete from cir.report_insolvency_codes where report_id in (
select report_id 
from cir.report_insolvency_codes ric 
left outer join cir.reports r on (r.id = ric.report_id)
where r.id is null);


select count(distinct(insolvency_id)) from cir.report_insolvency_codes ric;
select count(distinct(insolvency_id)) from cir.reports r ;
select count(distinct(insolvency_id)) from cir.raw_case rc ;

SELECT * FROM cir.raw_case WHERE "raw_case"."deleted_at" IS null

SELECT count(distinct(insolvency_id)) FROM "cir"."raw_case" WHERE "raw_case"."deleted_at" IS null;




select count(*) from cir.raw_case rc 
left outer join cir.reports r on (r.insolvency_id = rc.insolvency_id)
where r.insolvency_id is null;

select r.insolvency_id, min(r."publication"), max(r."publication") 
from cir.reports r
left outer join cir.raw_case rc  on (rc.insolvency_id = r.insolvency_id)
where rc.insolvency_id is not null
group by r.insolvency_id 

select * from (
select RANK() over ( partition by insolvency_id , publication order by created_at) dupes,
	   *
from cir.reports r 
) dup
-- where dupes > 1
order by insolvency_id, dupes

select count(*) from cir.reports r ;

select count(*) from cir.report_insolvency_codes ric;

select * from 


select dupes, * from (
		select *,
			   RANK() over ( partition by insolvencynumber, court, "publication" order by created_at) dupes
		from cir.reports r
	) addrank
	order by insolvencynumber , court
	--where dupes > 1 or publication <  NOW() - interval '6 month'

	
	select distinct(insolvency_id) from cir.reports r ;
	
update cir.raw_case set deleted_at = null;


select * from cir.raw_case rc left outer join cir.reports r  on r.insolvency_id = rc.insolvency_id where r.id is null;


select * from (
select insolvency_id,  rank() over ( partition by insolvency_id order by created_at) dupes 
from cir.raw_case rc) dupes 
where dupes > 1
;

select dupes, * from (
			select *,
			   RANK() over ( partition by insolvency_id, street, street_number, postal_code order by -id) dupes
			from cir.insolvency_locations
		) addrank

		
select * from cir.insolvency_locations_resolved ilr  --where kvk_number is null;


select count(*) from cir.insolvency_locations il;

drop table cir.insolvency_locations_resolved ;

select 
	pand.*,
	pbwgp.point,
	pbwgp.buurtcode,
	pbwgp.buurtnaam,
	pbwgp.wijkcode,
	pbwgp.wijknaam,
	pbwgp.gemeentenaam,
	pbwgp.gemeentecode,
	pbwgp.provinciecode,
	pbwgp.provincienaam
into cir.insolvency_locations_resolved
from (
	select il.*, b.pid
	from cir.insolvency_locations il, 
	lateral ( select pa.pid from baghelp.pand_addressen pa where pa.postcode = il.postal_code and pa.huisnummer = il.street_number order by pid limit 1) b 
) pand 
left outer join baghelp.pand_buurt_wijk_gemeente_provincie pbwgp on pbwgp.pid = pand.pid;
	
drop table cir.insolvency_locations_resolved ;

CREATE EXTENSION fuzzystrmatch;

select count(*) from cir.insolvency_locations_resolved_20210620 ilr;

truncate cir.insolvency_locations_resolved;
insert into cir.insolvency_locations_resolved
select 
	pand.*,
	ST_Transform(pbwgp.point, 3857) as point,
	pbwgp.buurtcode,
	pbwgp.buurtnaam,
	pbwgp.wijkcode,
	pbwgp.wijknaam,
	pbwgp.gemeentenaam,
	pbwgp.gemeentecode,
	pbwgp.provinciecode,
	pbwgp.provincienaam
from (
	select b.huisletter, b.huisnummertoevoeging, il.*, b.pid
	       , levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), concat(lower(addition1), lower(addition2))) tovoeging_score 
	from cir.insolvency_locations il,
	lateral ( select pa.pid, pa.huisletter, pa.huisnummertoevoeging 
	          from baghelp.pand_vbo_nums_labels pa 
	          where pa.postcode = il.postal_code 
	          and pa.huisnummer = il.street_number  
	          order by levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), concat(lower(il.addition1), lower(il.addition2))) limit 1
	) b 
) pand 
left outer join baghelp.pand_buurt_wijk_gemeente_provincie pbwgp on pbwgp.pid = pand.pid
on conflict do nothing;


UPDATE cir.insolvency_locations_resolved SET point = ST_Transform(point, 3857);

ALTER TABLE cir.insolvency_locations_resolved ALTER COLUMN point type geometry(Point, 3857);

select * from bag202104.nummeraanduiding n pvnl where postcode = '4815 CD' /* and huisnummer = '175' */

select * from cir.insolvency_locations_resolved;

select * 
from cir.insolvency_locations il 
left outer join cir.insolvency_locations_resolved ilr  on (ilr.insolvency_id = il.insolvency_id)
where ilr.id is null and il.street != 'Postbus'



create index on baghelp.pand_vbo_nums_labels (postcode);

select * from bag202104.nummeraanduiding n where postcode = '4815CD' and huisnummer = 175;
select * from bag202104.verblijfsobject v where v.hoofdadres =  '0758200000081093'

SELECT "reports"."id","reports"."created_at", 
        c.updated_at casedate, "reports"."updated_at",
        "reports"."deleted_at","reports"."insolvencynumber",
        "reports"."court","reports"."publication",
        "reports"."insolvency_code","reports"."title","reports"."end_report","reports"."insolvency_id"
FROM "cir"."reports" 
join lateral (
		  select id, created_at, updated_at
		  from cir.raw_case rc
		  where rc.insolvency_id = cir.reports.insolvency_id
		  order by rc.updated_at desc limit 1) c on true
WHERE c.id is null or c.updated_at < ("reports".updated_at - interval '1 day') AND "reports"."deleted_at" IS null


select * from cir.reports  where updated_at is not null;


select distinct gemeentenaam, 'https://ds.vboenergie.commondatafactory.nl/list/?format=csv&match-gemeentecode=' || gemeentecode from baghelp.buurt_wijk_gemeente_provincie bwgp order by gemeentenaam;
