/*
	create usefull tabels
	from a nlx extract dump for
	help during analyse
	
	combine bag ojects with CBS buurt / wijk / gemeente / provincie indeling.
*/

/* create fast subdivided geo lookup tables for admin areas */
drop table if exists baghelp.buurt_subdivided;
CREATE TABLE baghelp.buurt_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom, cb.buurtcode, cb.buurtnaam
FROM cbs.cbs_buurten_2021 cb;
create index on baghelp.buurt_subdivided using gist(geom);

drop table if exists baghelp.wijk_subdivided;
CREATE TABLE baghelp.wijk_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom, w.wijkcode, w.wijknaam
FROM cbs.cbs_wijken_2021 w;
create index on baghelp.wijk_subdivided using gist(geom);

drop table if exists baghelp.gemeente_subdivided;
CREATE TABLE baghelp.gemeente_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom, g.gemeentecode, g.gemeentenaam
from cbs.cdf_gem_2021 g;
create index on baghelp.gemeente_subdivided using gist(geom);

drop table if exists baghelp.provincie_subdivided;
CREATE TABLE baghelp.provincie_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom,
p.identificatie,
p.code,
p.naam
from cbs.provincies2012 p;
create index on baghelp.provincie_subdivided using gist(geom);

/*
 * Gasnet / Energie Net aan beheerder netwerk koppelen.
 */
drop table if exists baghelp.gasnetbeheer_subdivided;
create table baghelp.gasnetbeheer_subdivided as
select 
	st_subdivide(st_makevalid(geom)) as geom,
	name,
	netbeheerdercode,
	g.rowid,
	websiteuri
from import.gasnetbeheerdersvlak g;
create index on baghelp.gasnetbeheer_subdivided using gist(geom);


/* 
 * create a complete buurt - wijk - gemeente - provincie dataset
 * used for dataselection joins
 * 
 * */

drop table if exists baghelp.buurt_wijk_gemeente_provincie;

/* 
 * create baghelp.buurt_wijk_provincie table 
 * 
 * */
select b.buurtnaam, b.buurtcode, w.wijkcode, w.wijknaam, g.gemeentecode, g.gemeentenaam, p.naam, p.identificatie
into baghelp.buurt_wijk_gemeente_provincie
from cbs.cdf_buurt_2021 b
left outer join baghelp.wijk_subdivided w on  ST_Intersects(ST_PointOnSurface(b.geom), w.geom)
left outer join baghelp.gemeente_subdivided g on ST_Intersects(ST_PointOnSurface(b.geom), g.geom)
left outer JOIN baghelp.provincie_subdivided p ON ST_Intersects(ST_PointOnSurface(b.geom), p.geom);

create index on baghelp.buurt_wijk_gemeente_provincie (buurtcode);

/* should return 0 records validation */
select count(*) from baghelp.buurt_wijk_gemeente_provincie p
where not( substring(p.wijkcode, 2, 6)  = substring(p.wijkcode, 2, 6) or
  substring(p.wijkcode, 2, 4)  = substring(p.gemeentecode , 2, 4))

/* koppel nummeraanduiding aan buurten
 * gebruik num points ( standplaats  / ligplaats centroide ).
 * */
drop table if exists baghelp.num_buurt;
select 
	bs.buurtcode, 
	n.numid,
	geopunt
into baghelp.num_buurt
from  baghelp.all_pvsl_nums n
left outer join baghelp.buurt_subdivided bs on ST_within(n.geopunt, bs.geom);

create index on baghelp.num_buurt (buurtcode);
create index on baghelp.num_buurt (numid);
create index on baghelp.num_buurt using gist(geopunt);


/* 
 * koppel panden aan gasverbruik beheerders
 * maak centroidide per pand.
 * UPDATE FOR standplaats / ligplaatsen
 * */
drop table if exists baghelp.pand_point_gasnet;
select 
	pid,
	buurtcode,
	ppb.point,
	gs."name",
	gs.netbeheerdercode,
	gs.rowid as netvlakid
into baghelp.pand_point_gasnet
from baghelp.pand_point_buurt ppb
left outer join baghelp.gasnetbeheer_subdivided gs on ST_within(st_transform(point, 4326), gs.geom)
where gs."name" is not null

create index on baghelp.pand_point_gasnet (pid);


/* 
 * create pand_buurt_gemeente_provincie  
 * 
 * */
drop table if exists baghelp.num_buurt_wijk_gemeente_provincie;
select n.numid, bwgp.*
into baghelp.num_buurt_wijk_gemeente_provincie
from baghelp.num_buurt n
left outer join baghelp.buurt_wijk_gemeente_provincie bwgp on (bwgp.buurtcode = n.buurtcode)

create index numindex on baghelp.num_buurt_wijk_gemeente_provincie (numid);
create index numbuurtcodeindex on baghelp.num_buurt_wijk_gemeente_provincie (buurtcode);

select * from baghelp.num_buurt_wijk_gemeente_provincie nbwgp;
select count(*) from baghelp.num_buurt_wijk_gemeente_provincie;

-- select count(*) from baghelp.all_pvsl_nums apn;


/* 
 * Create pand - verblijfobject table helpbag table
 * 
 */


/* 
 * Create pand - verblijfsobject - nummeraanduiding helpbag table.
 * 
 * for every nummeraanduiding select 1 pand-verblijfsobject.
 */

select count(*) from baghelp.pand_vbo_nums pvn;
drop table if exists baghelp.pand_vbo_nums;

select
	pid,
	vid,
	null as sid,
	null as lid,
	oppervlakte,
	geopunt,
	n.identificatie as numid,
	n.postcode,
	openbareruimtenaam,
	n.huisnummer,
	n.huisletter,
	n.huisnummertoevoeging,
	concat(n.postcode, ' ', n.huisnummer, n.huisletter, n.huisnummertoevoeging) as adres,  -- used to match with old energy label data.
	n.gerelateerdeopenbareruimte,
	bouwjaar
into baghelp.pand_vbo_nums
from bagv2_062022.nummeraanduidingactueelbestaand n 
, lateral (
	select 
		p.identificatie as pid,
		vbo.identificatie as vid,
		vbo.oppervlakteverblijfsobject as oppervlakte,
		vbo.geopunt,
		o.openbareruimtenaam,
		p.bouwjaar
	from bagv2_062022.verblijfsobjectactueelbestaand vbo
    left outer join bagv2_062022.verblijfsobjectpandactueelbestaand vbop on  (vbop.identificatie  = vbo.identificatie)
    left outer join bagv2_062022.pandactueelbestaand p on (p.identificatie = vbop.gerelateerdpand)
    left outer join bagv2_062022.openbareruimteactueelbestaand o on (o.identificatie = n.gerelateerdeopenbareruimte)
    where vbo.hoofdadres = n.identificatie  
    limit 1
 ) vbo
 where vid is not null and n."typeadresseerbaarobject" = 'Verblijfsobject' 
 and n.identificatie is not null and n.postcode is not null; -- we need an verblijfsobject adress
 
 select count(*) from baghelp.pand_vbo_nums pvn;

select count(*) from baghelp.pand_vbo_nums pvn where postcode is null;
select count(*) from baghelp.pand_vbo_nums pvn; 

select * from bagv2_062022.nummeraanduidingactueelbestaand n where postcode is null;

create  index on bagv2_062022.verblijfsobject (hoofdadres); 
create  index on baghelp.pand_vbo_nums (numid);

/* 
 * check duplicate entries of numm id. 
 * 
 * */
select * from (
	select 
	*, 
	row_number() over  (partition by numid) as row 
	from baghelp.pand_vbo_nums pvn
) dups 
where dups.row > 1

 select * from baghelp.pand_vbo_nums where postcode = '1822BT' and huisnummer = 58;
 select * from bagv2_062022.nummeraanduiding n where postcode = '9983PM' --and huisnummer=19;
 
drop table if exists baghelp.standplaats_nums;
select
	-- p.identificatie as pid,
	null as pid,
	null as vid,
	s.identificatie as sid,
	null as lid,
	--vbo.oppervlakteverblijfsobject as oppervlakte,
	st_area(s.geovlak) as oppervlakte,
	st_pointonsurface(s.geovlak) as geopoint,
	na.identificatie as numid,
	na.postcode,
	o.openbareruimtenaam,
	na.huisnummer,
	na.huisletter,
	na.huisnummertoevoeging,
	concat(na.postcode, ' ', na.huisnummer, na.huisletter, na.huisnummertoevoeging)  as adres,  -- used to match with old energy label data.
	na.gerelateerdeopenbareruimte,
	0 as bouwjaar
	-- p.bouwjaar
into baghelp.standplaats_nums
from bagv2_062022.standplaatsactueelbestaand s
  --left join bagv2_032022.verblijfsobjectac vbop on  (vbop.gerelateerdpand = p.identificatie)
  --left join bagv2_032022.verblijfsobjectactueelbestaand vbo on (vbo.identificatie = vbop.identificatie )
  left join bagv2_062022.nummeraanduidingactueelbestaand na on (na.identificatie = s.hoofdadres and na."typeadresseerbaarobject" = 'Standplaats')
  left join bagv2_062022.openbareruimteactueelbestaand o on (o.identificatie = na.gerelateerdeopenbareruimte)
 where na.postcode  is not null; -- we need an adress
 
 drop table if exists baghelp.ligplaats_nums;

 select
    null as pid,
    null as vid,
    null as sid,
	-- p.identificatie as pid,
	l.identificatie as lid,
	st_area(l.geovlak) as oppervlakte,
	st_pointonsurface(l.geovlak) as geopoint,
	na.identificatie as numid,
	na.postcode,
	o.openbareruimtenaam,
	na.huisnummer,
	na.huisletter,
	na.huisnummertoevoeging,
	concat(na.postcode, ' ', na.huisnummer, na.huisletter, na.huisnummertoevoeging)  as adres,  -- used to match with old energy label data.
	na.gerelateerdeopenbareruimte,
	0 as bouwjaar
into baghelp.ligplaats_nums
from bagv2_062022.ligplaatsactueelbestaand l
  --left join bagv2_032022.verblijfsobjectac vbop on  (vbop.gerelateerdpand = p.identificatie)
  --left join bagv2_032022.verblijfsobjectactueelbestaand vbo on (vbo.identificatie = vbop.identificatie )
  left join bagv2_062022.nummeraanduidingactueelbestaand na on (na.identificatie = l.hoofdadres and na."typeadresseerbaarobject" = 'Ligplaats')
  left join bagv2_062022.openbareruimteactueelbestaand o on (o.identificatie = na.gerelateerdeopenbareruimte)
 where na.postcode  is not null; -- we need an adress
 
 /* 
  * combine all nummeraanduidingen with pand - vbo, standplaats, ligplaats
  * into one big table to use in all other analytics / adress correction  and matching.
  */
 drop table if exists baghelp.all_pvsl_nums;
 select * 
 into baghelp.all_pvsl_nums 
 from (
 select * from baghelp.pand_vbo_nums pvn
 union
 select * from baghelp.ligplaats_nums
 union 
 select * from baghelp.standplaats_nums
 ) a
 
 /* alles moet een postcode hebben */
 select * from baghelp.all_pvsl_nums apn
 where postcode is null;
 
 create index on baghelp.all_pvsl_nums (postcode);
 create index on baghelp.all_pvsl_nums (pid);
 create index on baghelp.all_pvsl_nums (vid);
 create index on baghelp.all_pvsl_nums (lid);
 create index on baghelp.all_pvsl_nums (sid);
 create index on baghelp.all_pvsl_nums (numid);
--= create index on baghelp.all_pvsl_nums (huisnummer);

select count(*) from baghelp.all_pvsl_nums;
select * from baghelp.all_pvsl_nums apn;
select count(*) from bagv2_062022.nummeraanduidingactueelbestaand n;

select * from baghelp.all_pvsl_nums apn;
 select * from bagv2_032022.nummeraanduidingactueelbestaand n where "typeadresseerbaarobject" = 'Standplaats';

--select count(*) from baghelp.pand_vbo_nums pvn;
/* extract all pand - postcode relations (aggregate postalcodes with pand) */
drop table if exists baghelp.pand_postcode_agg;
select p.pid, array_agg(postcode) postcode
into baghelp.pand_postcode_agg
from baghelp.pand_vbo_nums p
where postcode is not null
group by p.pid;


drop table if exists baghelp.pand_postcode;
/* create flat table with all pand - postcode relations */
select distinct p.pid, postcode
into baghelp.pand_postcode
from baghelp.pand_vbo_nums p
where postcode is not null;

create index on baghelp.pand_postcode (postcode);
create index on baghelp.pand_postcode (pid);

/* test join from pand_postcode to pand_net */
select * from baghelp.pand_postcode pp
left outer join baghelp.pand_point_gasnet g on (g.pid = pp.pid) 

select pid, unnest(postcode) postcode from baghelp.pand_postcode pp;



select * 
from kv.kleinverbruik_2021 k
where postcode_van like '1121RM%'

select 
	   Rank() over (partition by postcode_van,productsoort order by postcode_van, productsoort) as r,
	   *
from kv.kleinverbruik_2021
where postcode_van = '1121RM'

/**
 *  Woonfunctie.
 * 
 * vbo - count functies, woonfunctie (score).
 * 
 * pand - count functies, woonfunctie (score).
 *  
 */

create index on bagv2_062022.verblijfsobjectgebruiksdoel (identificatie);
create index on bagv2_062022.verblijfsobject (identificatie);

select p.identificatie pid, v2."gebruiksdoelverblijfsobject" 
from bagv2_062022.pandactueelbestaand p 
left outer join bagv2_062022.verblijfsobjectpandactueel v on (v.gerelateerdpand = p.identificatie)
left outer join bagv2_062022.verblijfsobjectactueelbestaand v3 on (v3.identificatie = v.identificatie)
left outer join bagv2_062022.verblijfsobjectgebruiksdoelactueelbestaand v2 on (v2.identificatie  = v3.identificatie)


select v.identificatie, v2."gebruiksdoelverblijfsobject" 
from bagv2_062022.verblijfsobjectactueelbestaand v 
left outer join bagv2_062022.verblijfsobjectgebruiksdoelactueelbestaand v2 on v2.identificatie = v.identificatie 

drop table if exists baghelp.vbo_woonfunctie_count;
select 
	identificatie vid,
	sum(case when v."gebruiksdoelverblijfsobject"  = 'woonfunctie' then 1 else 0 end) WoonfunctieCount,
	count(*)
into baghelp.vbo_woonfunctie_count
from bagv2_062022.verblijfsobjectgebruiksdoelactueelbestaand v group by identificatie;

create index on baghelp.vbo_woonfunctie_count (vid);

select
	pid, 
	(woonfunctiecount / functiecount * 100)::int  as woonfunctie_score 
into baghelp.pand_woonfunctie_score_202206
from (
	select 
	pid,
	--pvn.vid,
	sum(WoonfunctieCount) woonfunctiecount,
	sum(Count) functiecount
	--sum(WoonfunctieCount) as woonfunctiecount
	--sum(count)
	from baghelp.pand_vbo_nums pvn
	left outer join baghelp.vbo_woonfunctie_count wc on wc.vid = pvn.vid 
	group by pid
) c

create index on baghelp.pand_woonfunctie_score_202206 (pid);



select 
	a.hoofdadres, a."typeadresseerbaarobject",
	a.geopunt, n.huisnummer, n.huisletter, n.huisnummertoevoeging, 
	n.postcode, pvn.pid, pvn.openbareruimtenaam, pvn.oppervlakte
from bagv2_062022.adresseerbaarobjectnevenadresactueelbestaand a
left outer join bagv2_062022.nummeraanduidingactueelbestaand n on a.hoofdadres = n.identificatie
left outer join baghelp.pand_vbo_nums pvn on (pvn.numid = n.identificatie)