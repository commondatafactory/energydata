drop table if exists kv.pc6group_2024_gas;

select
	v.report_id,
	min(CAST(eg.data->'gas'->>'m3' as float)::int) AS gasm3_2023,
    --min(CAST(eg.data->'gas'->>'aansluitingen' AS float)::int) AS gas_aansluitingen_2023,
    --min(CAST(e.data->'elk'->>'Kwh' as float)::int) AS kwh_2023,
    --min(CAST(e.data->'elk'->>'aansluitingen' AS float)) AS kwh_aansluitingen_2023,
    --min(CAST(e.data->'elk'->>'productie' as float)::int) as kwh_productie_2023,
    --min(CAST(ee.data->'elk'->>'leveringsrichting' as float)::int) as kwh_leveringsrichting_2023,
	min(pg.bouwjaar) as min_bouwjaar,
	max(pg.bouwjaar) as max_bouwjaar,
	min(gvgv.gasm3_m2) as gasm3_m2,
	min(gvgv.gasm3_m3) as gasm3_m3,
	min(gvgv.totaal_grondbeslag_m2) as totaal_grondbeslag_m2,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geom	
into kv.pc6group_2024_gas
from  kv.verbruikpandp6v2_2023 v
left outer join bag202402.pandactueelbestaand pg on (pg.identificatie = v.pid)
left outer join kv.energyreports_2023 eg on (eg.id = report_id and eg."product" = 'gas')
left outer join kv.gas_verbruik_2023_grondbeslag_volume gvgv on gvgv.report_id = v.report_id
where v.product = 'gas'
group by v.report_id;

create index on kv.pc6group_2024_gas using gist(geom);
create index on kv.pc6group_2024_gas (report_id);


select * from kv.pc6group_2024_gas;

-- create index on kv.verbruikpandp6v2_2024 (id);
-- left outer join pand_postcode pp on (pp.identificatie = e.identificatie)


select * from kv.pc6group_2024_gas;


--------------- ELK --------------------------
drop table if exists kv.pc6group_2024_elk;
select
	report_id,
	-- min(CAST(eg.data->'gas'->>'m3' as float)::int) AS gasm3_2023,
    -- min(CAST(eg.data->'gas'->>'aansluitingen' AS float)::int) AS gas_aansluitingen_2023,
    -- CAST(data->'gas'->>'leveringsrichting' as float) as gas_leveringsrichting_2023,
    -- CAST(data->>'pandencount' as int) as panden,
	min(pg.bouwjaar) as min_bouwjaar,
	max(pg.bouwjaar) as max_bouwjaar,
    min(CAST(e.data->'elk'->>'Kwh' as float)::int) AS kwh_2023,
    min(CAST(e.data->'elk'->>'aansluitingen' AS float)) AS kwh_aansluitingen_2023,
    min(CAST(e.data->'elk'->>'productie' as float)::int) as kwh_productie_2023,
    --min(CAST(ee.data->'elk'->>'leveringsrichting' as float)::int) as kwh_leveringsrichting_2023,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geom	
into kv.pc6group_2024_elk
from  kv.verbruikpandp6v2_2023 v
left outer join  bag202402.pandactueelbestaand pg on (pg.identificatie = v.pid)
left outer join  kv.energyreports_2023 e on (e.id = report_id and e."product" = 'elk')
where v.product = 'elk'
group by v.report_id;

--------------------------------------------------


select * from kv.pc6group_2024_elk;

create index on kv.pc6group_2024_elk using gist(geom);
create index on kv.pc6group_2024_elk (report_id);

drop table if exists bagvector.kv_elk_pc6group_2024;
SELECT 
	report_id,
	min_bouwjaar,
	max_bouwjaar,
	kwh_2023, 
	kwh_aansluitingen_2023, 
	kwh_productie_2023, 
	ST_Transform(ST_SetSRID(geom ,28992), 3857) as geometry
into bagvector.kv_elk_pc6group_2024 
FROM kv.pc6group_2024_elk;

create index on bagvector.kv_elk_pc6group_2024 using gist (geometry);
create index on bagvector.kv_elk_pc6group_2024 (report_id);
ALTER TABLE bagvector.kv_elk_pc6group_2024  ADD COLUMN fid SERIAL PRIMARY KEY;

drop table if exists bagvector.kv_gas_pc6group_2024;
SELECT 
	report_id, 
	min_bouwjaar,
	max_bouwjaar,
	gasm3_2023, gasm3_m2, gasm3_m3, 
	totaal_grondbeslag_m2,
	ST_Transform(ST_SetSRID(geom ,28992), 3857) as geometry
into bagvector.kv_gas_pc6group_2024 
FROM kv.pc6group_2024_gas;

create index on bagvector.kv_gas_pc6group_2024 using gist (geometry);
create index on bagvector.kv_gas_pc6group_2024 (report_id);
ALTER TABLE bagvector.kv_gas_pc6group_2024  ADD COLUMN fid SERIAL PRIMARY KEY;





