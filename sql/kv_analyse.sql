.select count(*) from kv.kleinverbruik_2023 k;

select count(*) from kv.kleinverbruik_2022;


select postcode_van, netbeheerder, count(postcode_van)
from kv.kleinverbruik_2023 k
where productsoort = 'GAS'
group by postcode_van, netbeheerder
having count(postcode_van) > 1

SELECT  name,
 category,
FROM product
GROUP BY name, category
HAVING COUNT(id) > 1;



create table kv.klverbruik_counts as (
	year int
	count int
)

select 2014 as year, count(*) as count from kv.kleinverbruik_2014
union all
select 2015 as year, count(*) as count from kv.kleinverbruik_2015
union all
select 2016 as year, count(*) as count from kv.kleinverbruik_2016
union all
select 2017 as year, count(*) as count from kv.kleinverbruik_2017
union all
select 2018 as year, count(*) as count from kv.kleinverbruik_2018
union all
select 2019 as year, count(*) as count from kv.kleinverbruik_2019
union all
select 2020 as year, count(*) as count from kv.kleinverbruik_2020
union all 
select 2021 as year, count(*) as count from kv.kleinverbruik_2021
union all
select 2022 as year, count(*) as count from kv.kleinverbruik_2022
union all
select 2023 as year, count(*) as count from kv.kleinverbruik_2023
order by year;



select * from (select 2018 as year, netbeheerder, postcode_van, postcode_tot, productsoort, sjv from kv.kleinverbruik_2018 order by postcode_van limit 1000) a
union all
select * from (select 2019 as year, netbeheerder, postcode_van, postcode_tot, productsoort, sjv from kv.kleinverbruik_2019 order by postcode_van limit 1000) b
union all
select * from (select 2020 as year, netbeheerder, postcode_van, postcode_tot, productsoort, sjv from kv.kleinverbruik_2020 order by postcode_van limit 1000) c
union all
select * from (select 2021 as year, netbeheerder, postcode_van, postcode_tot, productsoort, sjv from kv.kleinverbruik_2021 order by postcode_van limit 1000) d
union all
select * from (select 2022 as year, netbeheerder, postcode_van, postcode_tot, productsoort, sjv from kv.kleinverbruik_2022 order by postcode_van limit 1000) e
union all
select * from (select 2023 as year, netbeheerder, postcode_van, postcode_tot, productsoort, sjv from kv.kleinverbruik_2023 order by postcode_van limit 1000) f


ALTER TABLE kv.kleinverbruik_2023 ADD COLUMN id SERIAL PRIMARY key;