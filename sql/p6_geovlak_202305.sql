/* creates postcde 6 areas around buildings */

@set baghelp = baghelp2023
--create schema if not exists ${baghelp};
@set bagschema = bag202305
@set cbs = cbs2022

/* create postcode 6 gebieden van gebouwen */

drop table if exists ${baghelp}.pc6group;

create table ${baghelp}.pc6group as (
select
	postcode,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(p.geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geovlak
from ${baghelp}.all_pvsl_nums n
left outer join ${bagschema}.pandactueelbestaand p on p.identificatie = n.pid
where pid is not null
--and postcode = '1121RM'
group by postcode);

/* add ligplaatsen to pc6groups */
insert into ${baghelp}.pc6group select
	postcode,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(l.geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geovlak
from ${baghelp}.all_pvsl_nums n
left outer join ${bagschema}.ligplaatsactueelbestaand l on l.identificatie = n.lid
where lid is not null
--and postcode = '1121RM'
group by postcode

/* add standplaatsen to pc6groups */
insert into ${baghelp}.pc6group
select
	postcode,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(s.geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geovlak
from ${baghelp}.all_pvsl_nums n
left outer join ${bagschema}.standplaatsactueelbestaand s on s.identificatie = n.sid
where sid is not null
--and postcode = '1121RM'
group by postcode


-- select distinct on (postcode) postcode, geovlak from ${baghelp}.pc6group order by postcode;

drop table if exists ${baghelp}.pc6group_all;
create table ${baghelp}.pc6group_all as
select
	postcode,
	st_multi(ST_MakeValid(
		ST_Union(
		    p.geovlak
		  )
		)
	) as geovlak
from ${baghelp}.pc6group p
--where postcode = '1011AE'
group by postcode

/* sanity checks */

select count(*) from ${baghelp}.pc6group_all;
select count(*) from ${baghelp}.pc6group;
/* should be the same as a count */
select count(distinct(postcode)) from ${baghelp}.pc6group_all;

select * from (select *, row_number() over (partition by postcode order by postcode) as row_num
from $baghelp.pc6group ) as sub where sub.row_num > 1;

select * from (
select
    postcode,
	geovlak,
	row_number() over (partition by geovlak order by postcode) as rn
from ${baghelp}.pc6group_all
) dups
where dups.rn > 1 and postcode like '10%'
order by geovlak




select * from ${baghelp}.pc6group where postcode = '1011AE'

select * from ${baghelp}.pc6group_all where postcode = '1011AE'

create index on ${baghelp}.pc6group_all (postcode);

create index on ${baghelp}.pc6group_all using gist( geovlak );




SELECT
	fid,
	c.postcode,
	aantal_inwoners,
	aantal_mannen,
	aantal_vrouwen,
	aantal_inwoners_0_tot_15_jaar,
	aantal_inwoners_15_tot_25_jaar,
	aantal_inwoners_25_tot_45_jaar,
	aantal_inwoners_45_tot_65_jaar,
	aantal_inwoners_65_jaar_en_ouder,
	percentage_geb_nederland_herkomst_nederland,
	percentage_geb_nederland_herkomst_overig_europa,
	percentage_geb_nederland_herkomst_buiten_europa,
	percentage_geb_buiten_nederland_herkomst_europa,
	percentage_geb_buiten_nederland_herkmst_buiten_europa,
	aantal_part_huishoudens,
	aantal_eenpersoonshuishoudens,
	aantal_meerpersoonshuishoudens_zonder_kind,
	aantal_eenouderhuishoudens,
	aantal_tweeouderhuishoudens,
	gemiddelde_huishoudensgrootte,
	aantal_woningen,
	aantal_woningen_bouwjaar_voor_1945,
	aantal_woningen_bouwjaar_45_tot_65,
	aantal_woningen_bouwjaar_65_tot_75,
	aantal_woningen_bouwjaar_75_tot_85,
	aantal_woningen_bouwjaar_85_tot_95,
	aantal_woningen_bouwjaar_95_tot_05,
	aantal_woningen_bouwjaar_05_tot_15,
	aantal_woningen_bouwjaar_15_en_later,
	aantal_meergezins_woningen,
	percentage_koopwoningen,
	percentage_huurwoningen,
	aantal_huurwoningen_in_bezit_woningcorporaties,
	aantal_niet_bewoonde_woningen,
	gemiddelde_woz_waarde_woning,
	aantal_personen_met_uitkering_onder_aowlft,
	geovlak
	--geom
FROM cbspostcode.cbs_pc6_2022 c
left outer join ${baghelp}.pc6group_all pc6 on pc6.postcode = c.postcode
-- where pc6.postcode like '1024L%'; postalcodes 'duplicate location'.


select * from ${baghelp}.pand_postcode;

select * from ${baghelp}.all_pvsl_nums where postcode like '1024L%';
select * from ${baghelp}.
--  create

select * from ${baghelp}.pc6group_all;

create table bagvector.postcode6 as (
    SELECT
    row_number() over (order by postcode) as fid,
  	postcode,
  	st_transform(geovlak, 3857) as geom
  	from ${baghelp}.pc6group_all
)

create index on bagvector.postcode6 using gist (geom);
create index on bagvector.postcode6 (postcode);
