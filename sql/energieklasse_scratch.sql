drop 	table if exists workdata.sbi_vbo;

/* we extract sbi codes from energy labels ~140K */
@set RVOCSV = v20240601_v2_csv
@set DATE = 202407
@set baghelp = baghelp

select * into workdata.sbi_vbo_${DATE} from import.${RVOCSV} where pand_sbicode != '';

select * from import.v20230703_v2_csv vvc;

select * from workdata.sbi_vbo_${DATE};

create index on import.${RVOCSV} (pand_bagverblijfsobjectid);

--select * from import.v

select count(*) from workdata.rvo;

drop table if exists workdata.sbi_vbo_pand_geo;

select pvn.*,
       pand_sbicode,
       pand_energieklasse,
       pand_gebouwklasse,
       pand_berekeningstype,
       --pand_energieprestatieindex,
       pand_eis_aandeel_hernieuwbare_energie,
       pand_temperatuuroverschrijding,
       pand_primaire_fossiele_energie_emg_forfaitair,
       pand_aandeel_hernieuwbare_energie,
       pand_aandeel_hernieuwbare_energie_emg_forfaitair ,
       pand_warmtebehoefte,
 
       -- pg.geovlak_3857,
       -- st_pointonsurface(pg.geovlak_3857) point
into workdata.sbi_vbo_pand_geo
from workdata.sbi_vbo s
left outer join ${baghelp}.pand_vbo_nums pvn on (pvn.vid = s.pand_bagverblijfsobjectid)
--left outer join ${baghelp}.pand_geovlak pg on (pvn.pid = pg.pid)

/* bag help data.. */
select p.identificatie as pid,
       p.geovlak ,
       ST_Force2D(ST_Transform(ST_SetSRID(geovlak,28992 ), 3857)) as geovlak_3857
into baghelp.pand_geovlak
from bag202101.pandactueelbestaand p


create index on baghelp.pand_geovlak (pid);

ALTER COLUMN geovlak TYPE geometry(Polygon, 3857)
  USING ST_Force2D(ST_Transform(ST_SetSRID(geovlak,28992 ), 3857));


/* merge sbi codes met raw sbi code data from the cbs */
drop table workdata.sbi_vbo_pand_geo_description;
select
	row_number() over (order by vid) as gid,
	*
into workdata.sbi_vbo_pand_geo_description
from workdata.sbi_vbo_pand_geo pg
left outer join import.sbicodes s on (pg.pand_sbicode = s.code)
where vid is not null;


drop table if exists export.sbi_vbo_pand_geo_description;
select
	gid,
	pid,
	vid,
	oppervlakte,
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	code as sbicode,
	title as activiteit,
	sbi_tree -> 'l1' ->> 0 as l1_code,
	sbi_tree -> 'l1' ->> 1 as l1_title,
	sbi_tree -> 'l2' ->> 0 as l2_code,
	sbi_tree -> 'l2' ->> 1 as l2_title,
	sbi_tree -> 'l3' ->> 0 as l3_code,
	sbi_tree -> 'l3' ->> 1 as l3_title,
	sbi_tree -> 'l4' ->> 0 as l4_code,
	sbi_tree -> 'l4' ->> 1 as l4_title,
	--geovlak_3857,
	point
into export.sbi_vbo_pand_geo_description_lambda
from workdata.sbi_vbo_pand_geo_description svpgd;

select distinct(sbi_tree -> 'l1' ->> 0) from workdata.sbi_vbo_pand_geo_description svpgd;

create index on export.sbi_vbo_pand_geo_description using gist(point);
create index on export.sbi_vbo_pand_geo_description using gist(geovlak_3857);
create index on export.sbi_vbo_pand_geo_description (l1_code);
create index on export.sbi_vbo_pand_geo_description (gid);

select count(distinct(vid)) from workdata.sbi_vbo_pand_geo_description svpgd;
select count(*) from workdata.sbi_vbo_pand_geo_description svpgd ;

/*
 * Merge energieklasse with panden 202407
 * */

select count(*) from import.${RVOCSV} vc;
select count(*) from import.${RVOCSV} vc
where vc.pand_bagverblijfsobjectid != '';

select * from import.${RVOCSV};

drop table if exists workdata.pvn_energieklasse;

/* merge labels with panden */
select *
into workdata.pvn_energieklasse_${DATE}
from baghelp.pand_vbo_nums pvn
left outer join "import".${RVOCSV} vc on vc.pand_bagverblijfsobjectid = pvn.vid
where vc.pand_bagverblijfsobjectid is not null;
 

select count(*) from workdata.pvn_energieklasse_${DATE};
select count(distinct(vid)) from workdata.pvn_energieklasse_${DATE};


select count(*) from baghelp.pand_vbo_nums pvn;
select count(distinct(pvn.vid)) from baghelp.pand_vbo_nums pvn;
-- select count(*) from workdata.pvn_energieklasse_score;
-- select avg(score) pand_energieklasse, pid from (

drop table if exists workdata.pand_energieklasse_score${DATE};

/* create pand klasse scores scores */
select
	avg(score), pid
into workdata.pand_energieklasse_score${DATE}
from (
select
   pid,
   case
   		when pand_energieklasse = 'A++++++' then 13
   		when pand_energieklasse = 'A+++++' then 12
   		when pand_energieklasse = 'A++++' then 11
   		when pand_energieklasse = 'A+++' then 10
   		when pand_energieklasse = 'A++' then 9
   		when pand_energieklasse = 'A+' then 8
   		when pand_energieklasse = 'A' then 7
   		when pand_energieklasse = 'B' then 6
   		when pand_energieklasse = 'C' then 5
   		when pand_energieklasse = 'D' then 4
   		when pand_energieklasse = 'E' then 3
   		when pand_energieklasse = 'F' then 2
		when pand_energieklasse = 'G' then 1
   		else 0
   end as score
   from workdata.pvn_energieklasse_${DATE}) as score
   group by pid

   create index on workdata.pand_energieklasse_score${DATE} (pid);

 /*
  * Here was some lost work to build woningscore maps and pand klass scoring.
  */

select distinct(pand_energieklasse) from workdata.pvn_energieklasse;

select count(*) from bagvector.pand_energie_2024v0 pev ;
select count(*) from bagvector.pand_energie_2023v2 pev ;

--create index on import.${RVOCSV} (pand_bagverblijfsobjectid);