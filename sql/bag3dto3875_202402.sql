 /*  2023-05.
  *
 -- convert to 3857.

ALTER TABLE "3dbag".pand3d
 ALTER COLUMN geovlak TYPE geometry(Polygon, 3857)
  USING ST_Force2D(ST_Transform(ST_SetSRID(geovlak,28992 ), 3857));

2023-09 add missing buildings.

fix relation between bag - panden, bag3d - panden, 3d geo information.

 */

 /*  The BASIS:
  *
  *  create pand table will all current buildings,
  *  on which we combine with bag 3d heights.
  * */

@set bagschema = bag202402
@set baghelp = baghelp

select * from ${bagschema}.pandactueelbestaand p;

drop table if exists bagvector.pand;
select
        p.identificatie,
        p.bouwjaar,
        ST_Transform(ST_SetSRID(geovlak ,28992 ), 3857) as geometry,
        ST_Area(geovlak) area
into bagvector.pand
from ${bagschema}.pandactueelbestaand p;

DO $do$ BEGIN EXECUTE 'COMMENT ON TABLE bagvector.pand IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;

create index on bagvector.pand (identificatie);

/* check counts */
select count(distinct(identificatie)) from ${bagschema}.pandactueelbestaand p;
select count(distinct(identificatie)) from bagvector.pand;  -- 10633138  -- 10908915
--select count(distinct(identificatie)) from bagvector.pand_energie_2021_lod132dv3; --10633138

/* need to extract bag identifiets from bag3d */
drop table if exists bagvector.bag3dkeys2024;
select
        fid,
        b3_h_maaiveld,
        b3_kas_warenhuis,
        p.b3_dak_type,
        p.b3_volume_lod13,
        p.b3_opp_buitenmuur,
        p.b3_opp_dak_plat,
        p.b3_opp_dak_schuin,
        p.b3_bouwlagen,
        Split_part (identificatie , '.', 4) as pid,
        identificatie
into bagvector.bag3dkeys2024
from bag3d2024.pand p;

--create index on bagvector.bag3dkeys (fid);
create index on bagvector.bag3dkeys2024 (pid);
create index on bagvector.bag3dkeys2024 (identificatie);
create index on bagvector.bag3dkeys2024 (fid);

select * from bagvector.bag3dkeys2024;

select count(*) from bagvector.bag3dkeys2024 bd;


select * from bag3d2024.pand p;

select * from bag3d2024.lod13_2d ld 
left outer join bag3d2024.pand p on p.identificatie = ld.identificatie
where p.identificatie =  'NL.IMBAG.Pand.0363100012165686'

select * from bag3d2024.lod13_2d ld 
--join bag3d2024.pand p on p.fid = ld.fid
where identificatie =  'NL.IMBAG.Pand.0363100012165686'

--'0363100012165686'

/*
 * Postcode 6, PC6 groups 2024.
 */
DROP TABLE IF EXISTS bagvector.pc6group_2024;

select
	--row_number() over (order by postcode) as fid,
	postcode,
    ST_Transform(ST_SetSRID(geovlak ,28992 ), 3857) as geometry
INTO bagvector.pc6group_2024
FROM ${baghelp}.pc6group_all;


/*
 * Add indexes and make ready for tegola
 */

-- CREATE INDEX ON bagvector.pc6group_2024 (report_id);
create index on bagvector.pc6group_2024 using gist (geometry);

alter table bagvector.pc6group_2024 alter column geometry type geometry(multipolygon, 3857);
ALTER TABLE bagvector.pc6group_2024 ADD COLUMN fid SERIAL PRIMARY KEY;

/*
 * Postcode 6 PC6 energie area. 2023. elk and gas.
 *
 * Seperate action needed where we combine Panden from pc6 to polygons.
 *
 */

DROP TABLE IF EXISTS bagvector.kv_elk_pc6group_2023;
SELECT
        report_id,
        sjv,
        
        ST_Transform(ST_SetSRID(geom ,28992 ), 3857) as geometry
INTO bagvector.kv_pc6group_2023
FROM kv.pc6group_2024_elk;

CREATE INDEX ON bagvector.kv_elk_pc6group_2023 (report_id);
create index on bagvector.kv_elk_pc6group_2023 using gist (geometry);
alter table bagvector.kv_elk_pc6group_2024 alter column geometry type geometry(multipolygon, 3857);
ALTER TABLE bagvector.kv_elk_pc6group_2024 ADD COLUMN id SERIAL PRIMARY KEY;

---

DROP TABLE IF EXISTS bagvector.kv_gas_pc6group_2023;
SELECT
        report_id,
        ST_Transform(ST_SetSRID(geom ,28992 ), 3857) as geometry
INTO bagvector.kv_gas_pc6group_2023
FROM kv.pc6group_2024_gas;

CREATE INDEX ON bagvector.kv_gas_pc6group_2023 (report_id);
create index on bagvector.kv_gas_pc6group_2023 using gist (geometry);
alter table bagvector.kv_gas_pc6group_2024 alter column geometry type geometry(multipolygon, 3857);
ALTER TABLE bagvector.kv_gas_pc6group_2024 ADD COLUMN id SERIAL PRIMARY KEY;

/* 
 * elk and gas kv points converted to bag - vector 2023.
 */

DROP TABLE IF EXISTS bagvector.kv_gas_points_2023;
SELECT  gn.numid, gn.pid, 
		gn.postcode, netvlakid, organisation, netbeheerder, 
		netgebied, straatnaam, 
		postcode_van, postcode_tot, woonplaats, 
		productsoort, verbruikssegment, 
		aansluitingen, leveringsrichting
		, fysieke_status, sjv, sjv_gemiddeld_productie, id,
		ST_Transform(ST_SetSRID(apn.geopunt ,28992), 3857) as geometry
into bagvector.kv_gas_points_2023
FROM kv.gas_nums_v2 gn
left join baghelp.all_pvsl_nums apn on apn.numid = gn.numid;

CREATE INDEX ON bagvector.kv_gas_points_2023 (numid);
create index on bagvector.kv_gas_points_2023 using gist (geometry);

/* elk points */
DROP TABLE IF EXISTS bagvector.kv_elk_points_2023;
SELECT  gn.numid, --gn.pid,
		gn.postcode, --netvlakid, 
		-- organisation, 
		netbeheerder, 
		netgebied, straatnaam, 
		postcode_van, postcode_tot, woonplaats, 
		productsoort, verbruikssegment, 
		aansluitingen, leveringsrichting
		, fysieke_status, sjv, sjv_gemiddeld_productie, id,
		ST_Transform(ST_SetSRID(apn.geopunt ,28992), 3857) as geometry
into bagvector.kv_elk_points_2023
FROM kv.elk_nums_v4 gn
left join baghelp.all_pvsl_nums apn on apn.numid = gn.numid;

CREATE INDEX ON bagvector.kv_elk_points_2023 (numid);
create index on bagvector.kv_elk_points_2023 using gist (geometry);

select count(*) from bagvector.kv_elk_points_2023;
select count(*) from bagvector.kv_gas_points_2023 kgp ;


/* 
 *  ELK - GAS PC6
 * 
 */


/*
 * Gasnetwerk aan de hand van ean code boek.
 */

select 
	organisation, gridarea as netwerkid,
	ST_Transform(ST_SetSRID(geocollection ,28992), 3857) as geometry
into bagvector.gas_netwerk_eancodeboek_202402
from eancodeboek.gridarea_geo gg;

--create index on 
CREATE INDEX ON bagvector.gas_netwerk_eancodeboek_202402 (netwerkid);
create index on bagvector.gas_netwerk_eancodeboek_202402 using gist(geometry);

/*
 * Adress points / standplaats/ ligplaats / verblijfsobject.
 *
 * All points of all addresses.
 * add gebruiksdoelen.
 */
select * from bagvector.puntjes;

drop table if exists bagvector.puntjes;
select
        numid,
        a.vid,
        pid,
        lid,
        sid,
        -- a.hoofdadres, --a."typeadresseerbaarobject",
        st_transform(st_setsrid(a.geopunt, 28992), 3857) as geometry,
        openbareruimtenaam, postcode, huisnummer, huisletter, huisnummertoevoeging,
        v.gebruiksdoelen,
        bouwjaar,
        oppervlakte
into bagvector.puntjes
from ${baghelp}.all_pvsl_nums a
left outer join ${baghelp}.vid_gebruiksdoelen v on a.vid = v.vid

select gebruiksdoelen, array_to_string(gebruiksdoelen, ';', '') from bagvector.puntjes



DO $do$ BEGIN EXECUTE 'COMMENT ON TABLE bagvector.puntjes IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;


select count(*) from bagvector.puntjes;
-- add unqique id to puntjes. since ho
--ALTER TABLE bagvector.puntjes ADD COLUMN id SERIAL PRIMARY KEY;

select a.hoofdadres, min(pid), max(pid), count(*) c
from bagvector.puntjes a
group by a.hoofdadres order by c desc;

create index on bagvector.puntjes (numid);
create index on bagvector.puntjes (vid);
create index on bagvector.puntjes (pid);
create index on bagvector.puntjes (lid);
create index on bagvector.puntjes (sid);
create index on bagvector.puntjes (postcode);

create index on bagvector.puntjes using gist (geometry);
alter table bagvector.puntjes alter column geometry type geometry(point, 3857);
select count(*) from bagvector.puntjes;

/*
 *
 * extract table lod22_2d with pand data. join 3d data with pand attributes.
 *

drop table bagvector.lod22_2d_pand;

SELECT
        --b.gid,
        --b.fid,
        pand_deel_id,
        h_dak_min, h_dak_50p, h_dak_70p, h_dak_max,
        ST_Transform(ST_SetSRID(b.geometrie ,28992 ), 3857) as geometry,
        ST_Area(b.geometrie) area,
        --p.identificatie,
        --p.oorspronkelijk_bouwjaar as bouwjaar,
        p.kas_warenhuis,
        p.h_maaiveld,
        Split_part (b.identificatie , '.', 4) as identificatie
into bagvector.lod22_2d_pand
FROM bag3d.lod22_2d b
left outer join bag3d.pand p on p.fid = b.fid;
--left outer join energiepanden e on e.identificatie = Split_part (p.identificatie , '.', 4);

/* add some indexes */
alter table bagvector.lod22_2d_pand alter column geometry type geometry(polygon, 3857);
create index on bagvector.lod22_2d_pand  using gist (geometry);
create index on bagvector.lod22_2d_pand (identificatie);

**/


/*
 * extract bag3d.lod13_2d pand
 * dataset into bagvector.
 *
 * combine pand geometry with bag3d geometry.
 *
 * 1) bag3d geometetry if not present take normal bag pand geometry.
 *
 * Now we have a COMPLETE BAG dataset containing all buildings.
 */

vacuum;

drop table bagvector.lod13_2d_pand_2024v3;

SELECT
        -- b.gid,
        -- b.fid,
        p.identificatie as pid,
        c.b3_pand_deel_id,
		b3_h_min, b3_h_50p, b3_h_70p, b3_h_max,
		b3_h_50p - b.b3_h_maaiveld as hoogte,
		b.b3_h_maaiveld,
        coalesce(
                ST_Transform(ST_SetSRID(c.geom ,28992 ), 3857),
                p.geometry) as geometry,
        area,
        b.b3_kas_warenhuis,
        b.b3_dak_type,
        b.b3_opp_dak_plat,
        b.b3_opp_dak_schuin,
        b.b3_bouwlagen,
        b.b3_opp_buitenmuur
into bagvector.lod13_2d_pand_2024v3
FROM bagvector.pand p
left outer join bagvector.bag3dkeys2024 b on p.identificatie = b.pid
left outer join bag3d2024.lod13_2d c on c.identificatie = b.identificatie;

select * from bagvector.bag3dkeys2024 bd ;

select count(*) from bagvector.pand;
select count(*) from bag3d.lod13_2d ld;

/* add indexes and set right geometry types */
alter table bagvector.lod13_2d_pand_2024v3 alter column geometry type geometry(polygon, 3857);
create index on bagvector.lod13_2d_pand_2024v3  using gist (geometry);
create index on bagvector.lod13_2d_pand_2024v3 (pid);

select count(*) from bag3d.lod13_2d;
select count(*)


select b3_h_min, b3_h_50p, b3_h_70p, b3_h_max, p.b3_h_maaiveld from bag3d2024.lod13_2d  b
left outer join bag3d2024.pand p on (p.identificatie = b.identificatie) ;


--select * from bagvector.pand;
select distinct(identificatie) from bag3d.pand


/* created in 2024 about year 2023 for the enery data.*/
drop table if exists bagvector.pand_energie_2023v1;
select distinct
    p.identificatie pid,
    -- vp.id,
    p.bouwjaar,
    p.area::int,
    pp.postcode,
    ee.id as e_report_id,
    eg.id as g_report_id,
    --
    CAST(eg.data->'gas'->>'m3' as float)::int AS gasm3_2023,
    CAST(eg.data->'gas'->>'aansluitingen' AS float)::int AS gas_aansluitingen_2023,
    -- CAST(data->'gas'->>'leveringsrichting' as float) as gas_leveringsrichting_2023,
    -- CAST(data->>'pandencount' as int) as panden,
    CAST(ee.data->'elk'->>'Kwh' as float)::int AS kwh_2023,
    CAST(ee.data->'elk'->>'aansluitingen' AS float) AS kwh_aansluitingen_2023,
    CAST(ee.data->'elk'->>'productie' as float)::int as kwh_productie_2023,
    CAST(ee.data->'elk'->>'leveringsrichting' as float)::int as kwh_leveringsrichting_2023,
    gvgv.totaal_grondbeslag_m2 as grondbeslag_m2_p6,
    gvgv.totaal_volume_m3 as volume_m3_p6,
    gvgv.gasm3_m2 as gasm3_grondbeslag_p6,
    gvgv.gasm3_m3 as gasm3_volume_p6,
    gvgv.totaal_gemiddeled_verbruik as gasm3_totaal_p6,
    pek.avg as energieklasse_score,
    pe.count as eancode_count,
    pws.woonfunctie_score,
    -- spuk.
    cbs.gemiddelde_woz_waarde_woning as woz,
    cbs.percentage_koopwoningen,
    cbs.percentage_huurwoningen,
    cbs.aantal_huurwoningen_in_bezit_woningcorporaties,
    replace(trim(xls.g_wozbag), '.', '-1')::int as gemeente_woz,
    p.geometry
    --e.data --as pandcount_2021
into bagvector.pand_energie_2023v1
from bagvector.pand p
left outer join kv.verbruikpandp6v2_2023 vg on (vg.pid = p.identificatie and vg."product"='gas')  ---- kv.gas
left outer join kv.verbruikpandp6v2_2023 ve on (ve.pid = p.identificatie and ve."product"='elk')  ---- kv.elkk
left outer join kv.energyreports_2023 eg on (eg.id = vg.report_id and eg.product='gas')           ---- kv data
left outer join kv.energyreports_2023 ee on (ee.id = ve.report_id and ee.product='elk')
-- left outer join kv.energyreports_2023 ee on (ee.id = vp.report_id and ee.product='elk')
left outer join kv.gas_verbruik_2023_grondbeslag_volume gvgv on (gvgv.report_id = vg.report_id )  ---- gasm3 / volume, grondbeslag
left outer join eancodeboek.pand_eancode_count pe on (pe.pid = p.identificatie)                   ---- ean_code_count
left outer join baghelp.pand_woonfunctie_score pws on (p.identificatie = pws.pid)                 ---- woonfunctie_score
left outer join workdata.pand_energieklasse_score202407 pek on (pek.pid = p.identificatie)        ---- energieklasse
left outer join baghelp.pand_postcode pp on pp.pid = p.identificatie                              -----combine pand_postcode
left outer join cbspostcode.cbs_pc6_2022 cbs on cbs.postcode = pp.postcode                        ---- cbs spuk fields.
left outer join baghelp.gemcode_p6 gp6 on (gp6.postcode = pp.postcode)                            ---  join gemeente code
left outer join import.kwb_cbs_2022 xls on (xls.gwb_code = gp6.gemeentecode);

create index on bagvector.pand_energie_2023v1 (pid);
create index on bagvector.pand_energie_2023v1 using gist(geometry);

/*
select pid, c 
from  (
	select pid, count(*) as c 
	from bagvector.pand_energie_2024v0 pev 
	group by pid
) pc
where c > 1;
*/

select count(*) from bagvector.pand;




select * from bagvector.pand_energie_2023v0;

--select energieklasse_score from bagvector.pand_energie_2021 pe where energieklasse_score is not null
--order by energieklasse_score desc;

--where e.id is not null
-- check total panden involved.
select count(*) from bagvector.pand_energie_2023v0 pe;
-- check total energy recordes matched with panden
select count(*) from bagvector.pand_energie_2023v0 pe where pe.kwh_2023 is not null;
-- check amount of panden with a 'living score'
select count(*) from baghelp.pand_woonfunctie_score pws where pws.woonfunctie_score > 0;
-- check total amount of enery records made.
select count(*) from kv.verbruikpandp6v2_2023 vv ;

select *
from kv.verbruikpandp6v2_2023 vv
join kv.energyreports_2023 e

select * from bagvector.pand;

/*
 * Combine cbs pc6 with panden energie 2023
 *
 */

drop table if exists bagvector.pand_2024_kv_2023_cbs_2022_lod132dv2;

create table bagvector.pand_2024_kv_2023_cbs_2022_lod132dv2  as (
select
    ldp.pid,
    pev.area,
    ldp.area as polygonarea,
    --ldp.kas_warenhuis,
    ldp.geometry,
    hoogte,
    b3_kas_warenhuis,
    b3_dak_type,
    b3_opp_dak_plat,
    b3_opp_dak_schuin,
    b3_bouwlagen,
    b3_opp_buitenmuur,
    --pes.avg as energieklasse_score,
    pev.e_report_id,
    pev.g_report_id,
    pev.postcode,
    pev.energieklasse_score,
    pev.gasm3_2023,    
    pev.volume_m3_p6,
    pev.gasm3_grondbeslag_p6,
    pev.gasm3_volume_p6,
    pev.gasm3_totaal_p6,
    -- pev.kwh_leveringsrichting_2023,
    pev.kwh_productie_2023,
    pev.kwh_2023,
    pev.bouwjaar,
    pev.woonfunctie_score,
    pev.eancode_count,
    pev.grondbeslag_m2_p6,
    -- spuk.
    pev.percentage_koopwoningen,
    pev.percentage_huurwoningen,
    pev.aantal_huurwoningen_in_bezit_woningcorporaties,
    -- pev.aantal_niet_bewoonde_woningen,
    pev.woz,
    pev.gemeente_woz
from bagvector.lod13_2d_pand_2024v3 ldp -- base is every building, bag3d + missing buildings.
-- left outer join ${baghelp}.pand_postcode pp on pp.pid = ldp.pid
--left outer join cbspostcode.cbs_pc6_2022 cbs on cbs.postcode = pp.postcode
left outer join bagvector.pand_energie_2023v1 pev on (pev.pid = ldp.pid)
--left outer join ${baghelp}.pand_gemcode_p6 pp6 on (pp6.pid=pp.pid and pp6.postcode=pp.postcode)
--left outer join import.kwb_cbs_2022 kc on (kc.gwb_code = pp6.gemeentecode)
-- where pp6.gemeentecode = 'GM0363' Amsterdam.
--where ldp.pid = '0363100012165686'
);

create index on bagvector.pand_2024_kv_2023_cbs_2022_lod132dv2 using gist (geometry);
-- add serial key needed for tegola.
ALTER TABLE bagvector.pand_2024_kv_2023_cbs_2022_lod132dv2  ADD COLUMN fid SERIAL PRIMARY KEY;

select count(*) from bagvector.pand_2023_cbs_2022_lod132dv0 pcld;
select count(*) from bagvector.pand_2024_kv_2023_cbs_2022_lod132dv0 pkcld ;
select count(*) from bagvector.pand_2024_kv_2023_cbs_2022_lod132dv1 pkcld ;

select count(*) from bagvector.pand_2024_kv_2023_cbs_2022_lod132dv2 pkcld ;

select * from bagvector.pand_2024_kv_2023_cbs_2022_lod132dv2 ;



select * from bagvector.pand_2024_kv_2023_cbs_2022_lod132dv1 pkcld where hoogte is not null;
select * from 

/* sanity checks */

select distinct count(*) from bagvector.pand_2023_cbs_2022_cbs_lod132dv4;

select count(*) from bagvector.pand_energie_2023v0 pev;
select count(*) from bagvector.pand_energie_20

select count(*) from bagvector.pand_2023_cbs_2022_cbs_lod132dv4;

select * from bagvector.pand_2023_cbs_2022_cbs_lod132dv4 pccld;
--where cbs.postcode like '1024LH';

--

/x


--where cbs.postcode like '1024LH';
create index on bagvector.pand_2023_cbs_2021_cbs_lod132dv4 using gist (geometry);
create index on bagvector.pand_2023_cbs_2021_cbs_lod132dv4 (postcode);
-- add serial key needed for tegola.
ALTER TABLE bagvector.pand_2023_cbs_2021_cbs_lod132dv4  ADD COLUMN fid SERIAL PRIMARY KEY;
--

