/*
 * Create convenience table with gemeente - wijk - buurt
 */

/* create subdivided geometry tables for fast lookup */
drop table if exists baghelp.buurt_subdivided;
CREATE TABLE baghelp.buurt_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom, cb.buurtcode, cb.buurtnaam
FROM cbs.cbs_buurten_2020 cb;
create index on baghelp.buurt_subdivided using gist(geom);

drop table if exists baghelp.wijk_subdivided;
CREATE TABLE baghelp.wijk_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom, w.wijkcode, w.wijknaam
FROM cbs.cbs_wijken_2020 w;
create index on baghelp.wijk_subdivided using gist(geom);

drop table if exists baghelp.gemeente_subdivided;
CREATE TABLE baghelp.gemeente_subdivided AS
SELECT ST_SubDivide(st_makevalid(geom)) AS geom, g.gemeentecode, g.gemeentenaam
from cbs.cdf_gem_2020 g;
create index on baghelp.gemeente_subdivided using gist(geom);


/* create pand_buurt_gemeente  ~ 10 mins. */
drop table if exists baghelp.pand_buurt_wijk_gemeente;

select p.pid, b.buurtcode, b.buurtnaam, w.wijknaam, w.wijkcode, g.gemeentenaam, g.gemeentecode
into baghelp.pand_buurt_wijk_gemeente
from baghelp.pand_points p
left outer join baghelp.buurt_subdivided b on (ST_within(p.point, b.geom))
left outer join baghelp.wijk_subdivided  w on (st_within(p.point, w.geom))
left outer join baghelp.gemeente_subdivided g on (st_within(p.point, g.geom))


create index on baghelp.pand_buurt_wijk_gemeente (pid);
create index on baghelp.pand_buurt_wijk_gemeente (buurtcode);

/* should return 0 records validation */
select count(*)
from baghelp.pand_buurt_wijk_gemeente p
where not(
 substring(p.wijkcode, 2, 6) = substring(p.wijkcode, 2, 6) or
 substring(p.wijkcode, 2, 4) = substring(p.gemeentecode , 2, 4)
)
select count(*) from baghelp.pand_buurt_wijk_gemeente;
select count(*) from baghelp.pand_points;
select count(*) from pand_vbo
