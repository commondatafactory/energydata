/* create postcode tables with aggregatable columns */
select * from public.pand_vbo_nums pvn
left outer join pand_agg_relations par on par.identificatie = pvn.pid


drop table postcode_agg_columns;
select
    pvn.postcode,
    max(ps.straat) straatnaam,
    max(gemeentecode) gemeentecode,
    max(gemeentenaam) gemeente,
    max(buurtcode) buurtcode,
    max(wijkcode) wijkcode,
    max(provinciecode) provinciecode,
    max(provincienaam) prvincienaam
into postcode_agg_columns_v3
from public.pand_vbo_nums pvn
left outer join postcode_straatnaam_v4 ps on ps.postcode = pvn.postcode
left outer join pand_agg_relations par on par.identificatie = pvn.pid
where pvn.postcode is not null
group by pvn.postcode

select count(*) from postcode_agg_columns_v4;

drop table postcode_straatnaam_v4;
select pa.postcode, max(o.openbareruimtenaam) as straat
into postcode_straatnaam_v4
from pand_addressen pa
left outer join bag.openbareruimte o on (o.identificatie = pa.straat)
where postcode is not null
group by postcode;

select count(distinct(postcode)) from postcode_straatnaam;
