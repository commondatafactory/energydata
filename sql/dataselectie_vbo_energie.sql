
--- select * from baghelp.pand_buurt_wijk_gemeente_provincie where buurtcode is null;  --- shows you windmills and other buildings in water.

/*
 * create BAG punten. point on surface polygon.
 *

drop table if exists baghelp.pand_points;
select p.identificatie pid, st_pointonsurface(p.geovlak) point , st_transform(st_pointonsurface(p.geovlak), 4326) point_4326
into baghelp.pand_points
from bagv2_062022.pandactueelbestaand p

create index on baghelp.pand_points(pid);

select count(*) from baghelp.pand_points;
*/
@set baghelp = baghelp
@set bag = bag202402

/*
 *
 * Matching NEW LABEL DATA / Energie klasses
 *
 * */

select count(*) from import.v20240601_v2_csv vc;

select * from (
	select
		row_number() over (partition by vc.pand_postcode, vc.pand_bagverblijfsobjectid order by pand_opnamedatum desc) r,
		*
		from import.v20240601_v2_csv vc
	where vc.pand_bagverblijfsobjectid != ''
) addrow where r > 1 order by pand_opnamedatum  desc

create index on import.v20240601_v2_csv (pand_bagverblijfsobjectid);
create index on import.v20240601_v2_csv (pand_bagligplaatsid);
create index on import.v20240601_v2_csv (pand_bagstandplaatsid);


/*
 * Match energyklass to nummeraanduiging avoiding duplicates
 */
create index on import.v20240601_v2_csv (pand_postcode);

drop table if exists labels.energieklasses_nums_202406v2;
select
    n.numid, ec.*
into  labels.energieklasses_nums_202406v2
from  baghelp.all_pvsl_nums n,
lateral ( 
    select * from import.v20240601_v2_csv ec
    where ec.pand_postcode = n.postcode and n.huisnummer = pand_huisnummer::int
    and (
    	(ec.pand_bagverblijfsobjectid = n.vid  and ec.pand_bagverblijfsobjectid != '')
    	or (ec.pand_bagligplaatsid = n.lid and ec.pand_bagligplaatsid != '' ) 
    	or (ec.pand_bagstandplaatsid = n.sid and ec.pand_bagstandplaatsid != '')
    )
    order by pand_opnamedatum desc
    limit 1
) ec

select numid, pand_energieklasse from labels.energieklasses_nums_202406v2 where pand_energieklasse is null;

select count(*) from labels.energieklasses_nums_202406v2 en --where en.pand_energieklasse is null;

select count(*) from baghelp.all_pvsl_nums apn;

select * from import.v20240601_v2_csv vvc;

create index on labels.energieklasses_nums_202406 (numid);
--select * from import.v20220701_csv vc where vc.pand_bagverblijfsobjectid = '0014010011076853'
select * from labels.energieklasses_nums_202307 en;


/*
 * Create gebruiksdoelen verblijfsobject datasets.
 *
 * */

drop table if exists ${baghelp}.verblijfsobjectgebruiksdoel;
select
	v.identificatie vid,
	gd.gebruiksdoelverblijfsobject
into ${baghelp}.verblijfsobjectgebruiksdoel
from ${bag}.verblijfsobjectactueelbestaand v
left outer join ${bag}.verblijfsobjectgebruiksdoelactueelbestaand gd on (v.identificatie = gd.identificatie);

create index on ${baghelp}.verblijfsobjectgebruiksdoel (vid)

select count(*) from ${baghelp}.verblijfsobjectgebruiksdoel v;
select count(*) from ${bag}.verblijfsobjectactueelbestaand v;
select count(*) from ${baghelp}.verblijfsobjectgebruiksdoel;

drop table if exists ${baghelp}.vid_gebruiksdoelen;

select
	vid, array_agg(gebruiksdoelverblijfsobject) gebruiksdoelen
into ${baghelp}.vid_gebruiksdoelen
from ${baghelp}.verblijfsobjectgebruiksdoel v
group by vid

select count(*) from baghelp.vid_gebruiksdoelen vg;

create index on ${baghelp}.vid_gebruiksdoelen (vid)


/* combination of old labels, gebruiksdoelen, admin (wijk/buurt/gemeente) */
drop table if exists workdata.dataselectie_vbo_intermediate_20210905;

select * from baghelp.vid_gebruiksdoelen vg where gebruiksdoelen != '{null}';

create index on baghelp.vid_gebruiksdoelen (vid);

create index on import.v20230703_v2_csv (pand_bagverblijfsobjectid);
create index on import.v20230703_v2_csv (pand_bagligplaatsid);
create index on import.v20230703_v2_csv (pand_bagstandplaatsid);
create index on import.v20230703_v2_csv (pand_postcode);

create index on ean.matched202110_unique (numid);
create index on bagvector.pand_energie_2021v2 (pid);

vacuum;

create index on baghelp.num_buurt_wijk_gemeente_provincie (naam);

/* the new code */

drop table if exists export.energie_202406_test;

select
	n.numid,
	n.pid,
	n.vid,
	n.lid,
	n.sid,
	n.postcode,
	n.openbareruimtenaam as straat,
	n.woonplaatsnaam,
	n.huisnummer,
	n.huisletter,
	n.huisnummertoevoeging,
	n.oppervlakte,
	case  -- woningen altijd 1.  anders gedeeld door 130.
	  when oppervlakte = 9999 then 1
	  when oppervlakte = 99999 then 1
	  when oppervlakte = 999999 then 1
	  when 'woonfunctie' = any (v.gebruiksdoelen) then 1
	  else (oppervlakte / 130)::int
	end as woningequivalent,
	v.gebruiksdoelen,
	n.bouwjaar as pand_bouwjaar,
	cp.gemiddelde_woz_waarde_woning as pc6_gemiddelde_woz_waarde_woning,
	kc.g_wozbag as gemiddelde_gemeente_woz,
	cp.percentage_koopwoningen as pc6_eigendomssituatie_perc_koop,
	cp.percentage_huurwoningen as pc6_eigendomssituatie_perc_huur,
	cp.aantal_huurwoningen_in_bezit_woningcorporaties as pc6_eigendomssituatie_aantal_woningen_corporaties,
	--
	CASE 
		WHEN env.netbeheerder = gnv.netbeheerder THEN env.netbeheerder
		WHEN env.netbeheerder IS NOT NULL AND (gnv.netbeheerder IS NULL) THEN env.netbeheerder
		WHEN env.netbeheerder IS NULL AND gnv.netbeheerder IS NOT NULL THEN gnv.netbeheerder
		WHEN env.netbeheerder != gnv.netbeheerder THEN 'elk: ' || env.netbeheerder || ' gas:' || gnv.netbeheerder
	END AS netbeheerder,
	ec.pand_energieklasse as energieklasse,
	-- ec.pand_energieindex,
	-- ec.pand_gebouwklasse,
	ec.pand_gebouwtype as woning_type,
	-- ec.pand_energieindex_met_emg_forfaitair,
	-- ec.pand_warmtebehoefte,
	-- ec.pand_aandeel_hernieuwbare_energie,
	-- ec.pand_gebruiksoppervlakte,
	ec.pand_sbicode as sbicode,
	--
	mu.count as gas_ean_count,
	---
	--ve.report_id as pc6_elk_id_2023,
	--vg.report_id as pc6_gas_id_2023,
	gvgv.totaal_grondbeslag_m2::int as p6_grondbeslag_m2,
	gnv.sjv AS 	p6_gasm3_2023,
    gnv.aansluitingen AS p6_gas_aansluitingen_2023,
    env.sjv AS p6_kwh_2023,
    --env AS p6_kwh_aansluitingen_2023,
    env.sjv_gemiddeld_productie as p6_kwh_productie_2023,
    -- env.leveringsrichting as p6_kwh_leveringsrichting_2023,
	st_transform(n.geopunt, 4326) point,
	m.buurtcode,
	m.buurtnaam,
	m.wijkcode,
	m.wijknaam,
	m.gemeentecode,
	m.gemeentenaam,
	m.naam as provincienaam,
	m.identificatie as provinciecode
into export.energie_202406_test
from baghelp.all_pvsl_nums n
left outer join cbspostcode.cbs_pc6_2022 cp on (cp.postcode=n.postcode)
left outer join baghelp.num_buurt_wijk_gemeente_provincie m on (m.numid = n.numid)
left outer join baghelp.vid_gebruiksdoelen v on (v.vid = n.vid)
left outer join eancodeboek.num_eancode_count mu on (m.numid = mu.numid)
--left outer join bagvector.pand_energie_2023v0 pe on (pe.pid = n.pid)
--left outer join kv.verbruikpandp6v2_2023 vg on (vg.pid = n.pid and vg."product"='gas' )  ---- kv.gas
--left outer join kv.verbruikpandp6v2_2023 ve on (ve.pid = n.pid and ve."product"='elk')   ---- kv.elk
left outer join kv.gas_nums_v2 gnv on (gnv.numid = n.numid)           ---- kv data
left outer join kv.elk_nums_v4 env on (env.numid = n.numid)
left outer join kv.gas_verbruik_2023_grondbeslag_volume gvgv on (gvgv.report_id = lower(gnv.productsoort) || '-' || gnv.netbeheerder || '-' || gnv.postcode_van || '-' || gnv.postcode_tot ) ---- gasm3 / volume, grondbeslag f"{product}-{p6data.netbeheerder}-{p6_from}-{p6_to}"
left outer join labels.energieklasses_nums_202406 ec on (ec.numid = n.numid) -- 202406
left outer join import.kwb_cbs_2022 kc on (kc.gwb_code = m.gemeentecode and recs = 'Gemeente') -- cbs 2022
where n.numid is not null
--and n.numid = '0518200000598142';
and (
	m.gemeentecode = 'GM0106' or m.gemeentecode = 'GM0363' 
	or m.gemeentecode = 'GM0518' or m.gemeentecode = 'GM0415') -- 'ASSEN - AMSTERDAM - DEN HAAG - LANDSMEER'.


SELECT count(*) FROM export.energie_202406_test et WHERE gebruiksdoelen IS NOT null;

-- should be identical.
select count(*) from baghelp.all_pvsl_nums apn ;
select count(*) from export.energie_202406 where energieklasse is null;

select * from export.energie_202406 --order by numid;

select count(*) from export.energie_202209_nl en;

select * from export.energie_202406_test WHERE postcode = '1121RM';

-
select * from export.energie_202207_zuid_holland ezh where postcode is null;
-- we need ~ 9560593
select count(numid) from baghelp.all_pvsl_nums apn;

select count(*) from export.energie_202207_nl;

select count(distinct(numid)) from export.energie_202207_nl

create index on export.energie_202207_nl (numid);

select * from export.energie_202207_nl;

select count(*) from (
select
	*,
	row_number () over (partition by numid order by pid) r
from export.energie_202207_nl
) addrank where r > 1


-- select count(*) from workdata.dataselectie_vbo_intermediate_20210505 dvi;
-- select * from export.dataselectie_vbo_intermediate_20210210 dvi where vid is null; -- should be 0
-- select count(*) from baghelp.pand_vbo_nums_labels pvnl where vid is null;
-- select count(*) from export.dataselectie_intermediate_vbo_20210209 div2;
/* add energy data to vbo data. */


/*
drop table if exists export.dataselectie_vbo_energie_20210505;

/* final export table for lambda db */
select
	pvnl.pid,
	pvnl.vid,
	pvnl.numid,
	pvnl.openbareruimtenaam as straat,
	pvnl.postcode,
	pvnl.huisnummer,
	pvnl.huisletter,
	pvnl.huisnummertoevoeging,
	pvnl.oppervlakte,
	case  --woningen altijd 1.  anders gedeeld door 130.
	  when oppervlakte = 9999 then 1
	  when oppervlakte = 99999 then 1
	  when oppervlakte = 999999 then 1
	  when 'woonfunctie' = any (pvnl.gebruiksdoelen) then 1
	  else (pvnl.oppervlakte / 130)::int
	end as woningequivalent,
	pvnl.woning_type,
	pvnl.labelscore_voorlopig,
	pvnl.labelscore_definitief,
	pvnl.energieklasse,
	pvnl.gemeentecode,
	pvnl.gemeentenaam,
	pvnl.buurtcode,
	pvnl.buurtnaam,
	pvnl.wijkcode,
	pvnl.wijknaam,
	pvnl.provinciecode,
	pvnl.provincienaam,
	st_transform(pvnl.point, 4326) point,

	e.ean_code_count as pand_gas_ean_aansluitingen,
	e.group_id_2020,
	e.gas_aansluitingen_2020 as p6_gas_aansluitingen_2020,
	e.gasm3_2020 as p6_gasm3_2020,
	e.kwh_2020 as p6_kwh_2020,
	e.totaal_oppervlak_m2 as p6_totaal_pandoppervlak_m2,
	pvnl.bouwjaar as pand_bouwjaar,
	e.ean_code_count as pand_gas_aansluitingen,
	pvnl.gebruiksdoelen
into export.dataselectie_vbo_energie_20210505
from workdata.dataselectie_vbo_intermediate_20210505 pvnl
left outer join cdf.energiepanden e on (e.identificatie = pvnl.pid)


select count(*) from export.dataselectie_vbo_energie_20210505 dve;  /* 9.1 million */

select count(*) from export.dataselectie_vbo_energie_20210505 dve;

/* test to find schools in beilen */
select * from export.dataselectie_vbo_energie_20210505 dve where buurtcode = 'BU17310000' and 'onderwijsfunctie' = ANY(gebruiksdoelen);

select * from export.dataselectie_vbo_energie_20210505 where gebruiksdoelen is not null
*/




/*
 * Using kleinverbruik gas data , combined with network information , we can reliably map
 * kv data to nummeraanduidingen. We use metering points data from the ean codeboek.
 */

select mp.*, k.*
into kv.gas_nums_v2
from kv.kleinverbruik_2023 k,
lateral (
    select distinct mp.numid, mp.pid, mp.postcode, mp.gridarea as netvlakid, mp.organisation,
    from eancodeboek.metering_points_nums mp
    join (
        select mp2.pid, mp2.gridarea
        from eancodeboek.metering_points_nums mp2
        left outer join eancodeboek.metering_points_nums gnet on mp2.pid = gnet.pid and gnet.gridarea = mp2.gridarea
        where mp2.postcode = postcode_van and mp2.pid is not null limit 1
    ) ncode on ( ncode.gridarea = mp.gridarea)
    where mp.postcode >= k.postcode_van
    and mp.postcode <= k.postcode_tot
    and mp.pid is not null
    order by mp.postcode
) mp
where k.productsoort = 'GAS'

select count(*) from kv.kleinverbruik_2023 k where k.productsoort = 'GAS';
select count(*) from kv.gas_nums;

/*
 * ELK net / Energie Net aan beheerder netwerk koppelen.
 * standaardiseer netbeheerder naam.
 */
drop table if exists ${baghelp}.elknetbeheer_subdivided;
create table ${baghelp}.elknetbeheer_subdivided as
select
	st_subdivide(st_makevalid(geom)) as geom,
	case name
		when 'NV Rendo' then 'rendo'
		when 'Liander N.V' then 'liander'
		when 'Coteq Netbeheer B.V.' then 'coteq'
		when 'Stedin' then 'stedin'
		when 'Enexis Netbeheer B.V.' then 'enexis'
		when 'Westland Infra Netbeheer B.V.' then 'westland'
		when 'Gesloten Distributie Systeem' then 'gds'
	else
		'OOpsie'
	end as netbeheerder,
	netbeheerdercode,
	-- netgebied,
	ogc_fid,
	websiteuri
from import.elknetbeheerdersvlak e;

create index on ${baghelp}.elknetbeheer_subdivided using gist(geom);

select * from ${baghelp}.elknetbeheer_subdivided where netbeheerder = 'Oopsie';  -- should be 0


/*
 * koppel numeraanduidingen aan elk netbeheerders.
 * */
drop table if exists ${baghelp}.num_point_elknet;
select
	apn.numid,
	apn.postcode,
	wijkcode,
	buurtcode,
	gemeentecode,
	--buurtcode,
	apn.geopunt,
	-- en."name",
	en.netbeheerder,
	apn.woonplaatsnaam,
	--en.netgebied,
	en.netbeheerdercode
	---gs.rowid as netvlakid
into ${baghelp}.num_point_elknet
from ${baghelp}.all_pvsl_nums apn
left outer join ${baghelp}.elknetbeheer_subdivided en on ST_within(st_transform(geopunt, 4326), en.geom)
left outer join baghelp.num_buurt_wijk_gemeente_provincie nbwgp on nbwgp.numid = apn.numid
where en.netbeheerdercode is not null

select * from baghelp.elknetbeheer_subdivided es;

create index on baghelp.num_point_elknet (numid);
create index on baghelp.num_point_elknet (postcode);
create index on baghelp.num_point_elknet (wijkcode);
create index on baghelp.num_point_elknet (gemeentecode);
create index on baghelp.num_point_elknet (woonplaatsnaam);
create index on baghelp.num_point_elknet (netbeheerder);

select count(*) from baghelp.num_point_elknet npe where woonplaatsnaam is null; -- should be 0
select count(*) from baghelp.all_pvsl_nums apn where woonplaatsnaam is null;  -- should be 0


select * from baghelp.num_point_elknet npe;
select * from kv.elk_nums;

/*
 * Using kleinverbruik elk data, combined with elk network information , we can reliably map
 * kv data to nummeraanduidingen. by makeing sure the postcode range in gemeente, elk-network
 *
 * ~ 8 min run for NL.
 */

--join baghelp.all_pvsl_nums apn ;
select distinct k.netbeheerder, b.netbeheerder from kv.kleinverbruik_2023 k
full outer join (
select distinct netbeheerder from baghelp.num_point_elknet en
) b on b.netbeheerder = k.netbeheerder

drop table if exists kv.elk_nums_v3;

select ne.*, k.*
into kv.elk_nums_v4
from kv.kleinverbruik_2023 k,
lateral (
SELECT npe.numid, npe.postcode, npe.gemeentecode, npe.woonplaatsnaam, npe.geopunt  --, ncode.netbeheerdercode, npe.netbeheerder as enetbeheerder
    FROM baghelp.num_point_elknet npe
    join (
        select
        	elk2.gemeentecode,
        	elk2.netbeheerdercode,
        	elk2.netbeheerder,
        	elk2.woonplaatsnaam
        from baghelp.num_point_elknet elk2
        where elk2.postcode = k.postcode_van and elk2.netbeheerder = k.netbeheerder limit 1
    ) ncode on ( npe.netbeheerder = ncode.netbeheerder and npe.woonplaatsnaam = ncode.woonplaatsnaam)
    WHERE npe.postcode >= k.postcode_van
    AND npe.postcode <= k.postcode_tot
    -- and npe.netbeheerder = k.netbeheerder
    -- AND ncode.netbeheerder is not null
    ORDER BY npe.postcode
) ne
where k.productsoort = 'ELK'
-- and k.netbeheerder = 'enexis'
and ne.numid is not null
--and postcode_van LIKE '1%'

select * from kv.kleinverbruik_2023 k where netbeheerder = 'enexis';

-- select k.woonplaats,  from kv.kleinverbruik_2023 k;

/* debug stuff */
select * from baghelp.num_point_elknet npe;

select count(*) from kv.elk_nums_v4 env;
select count(*) from baghelp.num_point_elknet npe ;

select
	--*
	count(*)
    -- numid, postcode,
    --netbeheerder,
    count
from (
select numid, postcode,
	   --netbeheerder,
	   count(*) from kv.elk_nums_v4 group by numid, postcode --, --netbeheerder
) kvcount
where count > 1
--order by numid

select * from kv.elk_nums_v2 where numid = '0559200000007178'


select count(*) from baghelp.num_point_elknet npe
union
select count(*) from kv.elk_nums_v4;

select * from baghelp.num_point_elknet npe ;
select * from kv.elk_nums;

select * from kv.kleinverbruik_2023 k where k.postcode_van >= '3161GG' and k.postcode_tot <= '3161GJ' and productsoort = 'ELK';

select numid, netbeheerder, postcode from kv.elk_nums_v2 where postcode = '3161GG' group by numid, netbeheerder, postcode;

select distinct netbeheerder from kv.kleinverbruik_2023 k ;

/*
 * 	Analyse results missing data
 *
 * */

SELECT
	apn.numid, apn.geopunt, apn.postcode, apn.gemeentecode, apn.netbeheerdercode
INTO kv_bag.nums_missing_elk
FROM baghelp.num_point_elknet apn
LEFT JOIN kv.elk_nums_v4 env ON apn.numid = env.numid
WHERE env.numid IS NULL

SELECT a.netbeheerdercode, a.total, b.missing,  b.missing::float/a.total * 100  percentage FROM (
	SELECT netbeheerdercode, count(*) total FROM baghelp.num_point_elknet npe GROUP BY netbeheerdercode
) a
LEFT JOIN (
	SELECT netbeheerdercode, count(*) missing FROM kv_bag.nums_missing_elk GROUP BY netbeheerdercode
) b ON a.netbeheerdercode = b.netbeheerdercode


SELECT * FROM eancodeboek.meteringpoints_nums_geo mng;

SELECT
	mng.numid, mng.geopunt, mng.postcode, gridarea
INTO kv_bag.nums_missing_gas_v2
FROM eancodeboek.meteringpoints_nums_geo mng
LEFT JOIN kv.gas_nums_v2 env ON mng.numid = env.numid
WHERE env.numid IS NULL

SELECT * FROM eancodeboek.gridarea_geo gg;

SELECT a.organisation, total, missing, missing::float / total  * 100  percentage FROM (
SELECT organisation , count(*) missing FROM
	kv_bag.nums_missing_gas_v2 mg
JOIN eancodeboek.gridarea_geo gg ON (gg.gridarea = mg.gridarea)
GROUP BY organisation
) a
JOIN (
	SELECT organisation, count(*) AS total FROM eancodeboek.meteringpoints_nums GROUP BY organisation
) b ON a.organisation = b.organisation


SELECT
	a.organisation, a.total, b.missing
SELECT organisation, count(*) FROM eancodeboek.metering_points_nums mpn GROUP BY organisation;

SELECT organisation, count(*) FROM kv.gas_nums gn GROUP BY organisation;


SELECT * FROM kv.elk_nums_v4 env;
SELECT * FROM kv.gas_nums gn;



