drop table if exists kv.pc6group;

select
	report_id,
	--*
	-- array_to_string(array_agg(distinct(pp.postcode)), ', ') as postcodes,
	-- max(e.gas_aansluitingen_2021) as gas_aansluitingen,
	-- max(e.kwh_aansluitingen_2021) as elk_aansluitingen,
	-- max(e.bouwjaar) as max_bouwjaar,
	-- max(e.bouwjaar) as min_bouwjaar,
	-- max(e.gasm3_2021) as gasm3_2021,
	-- max(e.gasm3_per_m2) as gasm2_per_m2,
	-- max(e.gasm3_per_m3) as gasm3_per_m3,
	-- max(e.pandcount_2021) as panden,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	)
	into kv.pc6group_2021
from  kv.verbruikpandp6v2_2021 v
left outer join  bagv2_032022.pandactueelbestaand pg on (pg.identificatie  = v.id)
--where v.report_id = '1121RM'
group by v.report_id;

select * from kv.pc6group_2021;
create index on kv.verbruikpandp6v2_2021 (id);
--left outer join pand_postcode pp on (pp.identificatie = e.identificatie)


select * from kv_pc6group_2021_v6;

SELECT * FROM kv.
