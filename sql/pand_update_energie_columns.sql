/*
 *
 *
*/

alter table "3dbag".energiepanden
add column if not exists totaal_verbruik_m3 int;
alter table "3dbag".energiepanden
add column if not exists totaal_volume_m3 int;
alter table "3dbag".energiepanden
add column if not exists totaal_oppervlak_m2 int;
alter table "3dbag".energiepanden
add column if not exists gasm3_per_m3 float;
alter table "3dbag".energiepanden
add column if not exists gasm3_per_m2 int;


UPDATE "3dbag".energiepanden p
SET totaal_verbruik_m3 = ed.totaal_verbruik_m3,
    totaal_volume_m3 = ed.totaal_volume_m3,
    totaal_oppervlak_m2 = ed.totaal_oppervlak_m2,
    gasm3_per_m2 = ed.gasm3_per_m2,
    gasm3_per_m3 = ed.gasm3_per_m3
FROM gasverbruik_by_group_id ed
WHERE p.group_id_2020 = ed.group_id_2020;
