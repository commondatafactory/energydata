/*
 * Here we combine 3dbag data with 'kleinverbruik' energy data to
 * calculate gasm3 per m2 (grondbeslag) and m3 (volume).
 *
 * Author: Stephan Preeker. 2024-06
 * *
 * The coupling of kleinverbruik data with PC6 data
 * to buildings is done using python scripts.
 *
 * The resulting Kleinverbruik data is coupled to buildings of which total surface and volume
 * determined.
 */

create index on bagvector.lod12_2d_pand (identificatie);

SELECT h_dak_50p  - h_maaiveld height, area,  ((h_dak_50p  - h_maaiveld) * area ) as volume, identificatie FROM bagvector.lod12_2d_pand;


select * from  kv.verbruikpandp6v2_2023 vp;

--select data->'pandencount' pc,  * from kv.energyreports_2022 e; 


/*
 * Sum pand surfaces and volume within pc6 energy area.
 */

drop table if exists into kv.gas_verbruik_2024_grondbeslag_volume;

select 
	report_id,
	avg_gasm3::int,
	aansluitingen,
	-- pandencount as pandencount_bag3d,
	-- pandencount_pc6,
	-- pid_count_bag3d,
	totaal_grondbeslag_m2,
	totaal_volume_m3,
	panden_count,
	-- area
	(aansluitingen * avg_gasm3) as totaal_gemiddeled_verbruik, 
	(aansluitingen * avg_gasm3 ) / totaal_grondbeslag_m2 as gasm3_m2,
	(aansluitingen * avg_gasm3 ) / totaal_volume_m3  as gasm3_m3
into kv.gas_verbruik_2023_grondbeslag_volume
from (
--SELECT * FROM (
select 
	--*,
    report_id,
	--extract gas m3.
	--extract aansluitingen
    (data->'gas'->>'m3')::float avg_gasm3,
	(data->'gas'->>'aansluitingen')::float aansluitingen,
	jsonb_array_length(DATA->'gas'->'panden') AS panden_count,
	--h_dak_50p  - h_maaiveld height, 
	totaal_grondbeslag_m2::int,
	totaal_volume_m3::int
	--area,  
	--((h_dak_50p  - h_maaiveld) * area ) as volume,
	--panden.*,
from kv.energyreports_2023 e,
lateral (
	select
		report_id, 
		count(*)  as pandencount, 
		sum(area) as totaal_grondbeslag_m2,
		sum( area*(h_dak_50p - h_maaiveld )) as totaal_volume_m3
	from
		kv.verbruikpandp6v2_2023 vp
		left outer join  bagvector.lod13_2d_pand ldp on ldp.identificatie  = vp.pid
		where vp.report_id = e.id AND product = 'gas'
		group by vp.report_id
) panden
WHERE e.product = 'gas'
--AND 
  --where e.id like '%1121RM%'
--where e.id = '1412JX-1412JZ'
) pc6pandenagg
-- WHERE pand_count != pid_count

create index on kv.gas_verbruik_2023_grondbeslag_volume (report_id);


SELECT * FROM kv.gas_verbruik_2023_grondbeslag_volume;

SELECT count(*) FROM kv.energyreports_2023 e WHERE product = 'gas';
SELECT count(*) FROM kv.kleinverbruik_2023 k WHERE k.productsoort = 'GAS';
SELECT * FROM kv.verbruikpandp6v2_2023 WHERE product = 'gas';
select count(*) from kv.gas_verbruik_2022_grondbeslag_volume gvgv;
select count(*) from kv.energyreports_2022 e2;  -- should be equal.


select * from kv.verbruikpandp6v2_2023 vv;


/* testing above query:
 * 
 * Inspect pand surfaces and volume within pc6 energy area.
 * 
 */
select
	report_id,
	avg_gasm3,
	aansluitingen,
	--pandencount as pandencount_bag3d
	pandencount_pc6,
	pc6pandenagg.*
	--totaal_grondbeslag_m2,
	--totaal_volume_m3,
	--(aansluitingen * avg_gasm3 ) / totaal_grondbeslag_m2 as gasm3_m2,
	--(aansluitingen * avg_gasm3 ) / totaal_volume_m3  as gasm3_m3
--into kv.gas_verbruik_2021_groundbeslag_volume 
from (
select 
	--*,
	--extract gas m3.
	--extract aansluitingen
	(data->'gas'->>'m3')::float avg_gasm3,
	(data->'gas'->>'aansluitingen')::int aansluitingen,
	(data->'pandencount')::int pandencount_pc6,
	--h_dak_50p  - h_maaiveld height, 
	--area,  
	--((h_dak_50p  - h_maaiveld) * area ) as volume,
	panden.*
from kv.energyreports_2022 e,
lateral (
	select
		* --,
		--distinct(id) pid_count
		--count(*)  as pandencount, 
		--sum(area) as totaal_grondbeslag_m2,
		--sum( area*(h_dak_50p - h_maaiveld )) as totaal_volume_m3
	from
		kv.verbruikpandp6v2_2022 vp
		left outer join  bagvector.lod12_2d_pand ldp on ldp.identificatie  = vp.id
		where vp.report_id = e.id
		--group by vp.report_id
) panden
--where e.id like '1121%'
where e.id = '1412JX-1412JZ'
) pc6pandenagg

select * from kv.verbruikpandp6v2_2022 v where  v.id = '0425100000010498'

select * from kv.gas_verbruik_2022_groundbeslag_volume gvgv;