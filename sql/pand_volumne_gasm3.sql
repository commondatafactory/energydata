/*
 * Here we combine 3dbag data with 'kleinverbruik' energy data to
 * calculate gasm3 per m2 and m3 (volume).
 *
 * Author: Stephan Preeker.
 *
 * experimental code. proof of concept.
 *
 * The coupling of kleinverbruik data with PC6 data
 * to buildings is done somewhere else using
 * python scripts.
 */

select * from pand_vbo_nums pvn where pvn.postcode = '1013AL' and pvn.huisnummer = 10


select group_id_2020 , geovlak, st_area(st_transform(geovlak, 26986)), * from "3dbag".energiepanden e  where e.identificatie = '0363100012237143';

/**
 * 1 m3 = 37681200 GJ
 *
 * calculate GJ per m3.
 *
 * calculate GJ per 10.000 m2.
 *
 */

select count(distinct(group_id_2020)) from "3dbag".energiepanden e;

drop table if exists gasverbruik_by_group_id;

select count(valid_calculation) from gasverbruik_by_group_id where valid_calculation is true; /* 219.000 of 319.000*/

/* calculate with the area m2 and height of 50-percentile measurements of height measurements a volume */
select
	group_id_2020,
	valid_height as valid_calculation,
	totaal_oppervlak_m2::int,
	totaal_verbruik_m3::int,
	totaal_volume_m3::int,
   	case when valid_height and totaal_volume_m3 > 0 then  totaal_verbruik_m3 / totaal_volume_m3
   	else null  -- no volume. we are invalid.'
   	end  as gasm3_per_m3,
   	case when valid_height and totaal_verbruik_m3 > 0 then  (totaal_volume_m3	/ totaal_verbruik_m3)::int
   	else null  -- no volume. we are invalid.'
   	end  as m3_per_gasm3
into gasverbruik_by_group_id
from (
select
   count(height_m) = count(identificatie) as valid_height,
   sum (
   		case when height_m is not null then totaal_oppervlak_m2 * height_m
   		else 0  -- no volume. we are invalid.'
   		end
   ) as totaal_volume_m3,
   sum(totaal_oppervlak_m2) as totaal_oppervlak_m2,
   min(gas_aansluitingen_2020) * min(gasm3_2020) as totaal_verbruik_m3, -- all the same value. min/max no matter.same kleinverbruik group.
   group_id_2020
from (
select
case when ("ground-0.40" is not null and "roof-0.50" is not null)  then  ("roof-0.50" - "ground-0.40") else null end as height_m,
identificatie,
gas_aansluitingen_2020,
gasm3_2020,
group_id_2020,
st_area(geovlak) as totaal_oppervlak_m2
from "3dbag".energiepanden e --where group_id_2020 = 559
) as group_with_height
group by group_id_2020
) as tv

alter table "3dbag".energiepanden add column m2 int;


update "3dbag".energiepanden e
	set m2 = st_area(p.geovlak)::int
from "3dbag".pand3d p
where e.identificatie = p.identificatie

/*
 * 1 m3 37681200 GJ
 *
 */

select group_id_2020 from "3dbag".energiepanden e
where identificatie = '0363100012149391'
group by e.group_id_2020


select identificatie, geovlak, st_area(st_transform(geovlak, 26986)) from "3dbag".energiepanden e  where e.identificatie = '0415100000007326';


select count(*) from "3dbag".energiepanden e where e."roof-0.50" is not null;
select count(*) from "3dbag".energiepanden e where e."ground-0.30" is null or e."roof-0.50" is null;
select count(*) from "3dbag".energiepanden e where e."roof-0.50" is null;
