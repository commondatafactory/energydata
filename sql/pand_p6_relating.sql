drop table if exists baghelp.pand_p6;

SELECT
    p.identificatie,
    -- p.geovlak,
    count(*),
    array_agg(DISTINCT a.postcode) as postcodes
into baghelp.pand_p6
FROM bag202108.adres a,
     bag202108.nummeraanduiding n,
	 bag202108.pand p,
     bag202108.verblijfsobject v,
     bag202108.verblijfsobjectpand vp
WHERE a.nummeraanduiding = n.identificatie
    -- and a.woonplaatsnaam = 'Landsmeer'
     AND n.identificatie = v.hoofdadres
     AND vp.gerelateerdpand = p.identificatie
     AND vp.identificatie = v.identificatie
GROUP BY (p.identificatie);

select count(*) from baghelp.pand_p6;
/*select count(*) from public.kleinverbruik; */


/* create  pand - postcode table */
select identificatie, unnest(postcodes) as postcode into baghelp.pand_postcode from baghelp.pand_p6;

create index on pand_postcode (postcode);
create index on pand_postcode (identificatie);
