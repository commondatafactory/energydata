select count(*) from import.woningvoorraad_gemeente_apeldoorn wga;

select count(*) from baghelp.pand_buurt_wijk_gemeente_provincie pbwgp
left outer join baghelp.pand_vbo_nums_labels pvnl on (pvnl.pid = pbwgp.pid)
where gemeentenaam = 'Apeldoorn';

create index on import.woningvoorraad_gemeente_apeldoorn (vbo_id);

drop table if exists export.apeldoorn_eigenaren_20210517;
select
    row_number() over (order by vid) as gid,
	vid,
	pvnl.pid,
	buurt_code,
	buurt as buurtnaam,
	wijkcode,
	wijknaam,
	gemeentecode,
	gemeentenaam,
	openbareruimtenaam,
	huisnummer, huisletter, huisnummertoevoeging,
	type_eigenaar, naam_niet_natuurlijk_persoon,
	wga.bouwjaar, oppervlakte,
	woning_type,
	pg.geovlak_3857
into export.apeldoorn_eigenaren_20210517
from import.woningvoorraad_gemeente_apeldoorn wga
left outer join baghelp.pand_vbo_nums_labels pvnl on pvnl.vid = wga.vbo_id
left outer join baghelp.pand_geovlak pg on (pg.pid = pvnl.pid)
left outer join baghelp.pand_buurt_wijk_gemeente_provincie pbwgp on (pbwgp.pid = pvnl.pid)


select * from export.apeldoorn_eigenaren_20210517 ae ;

create index on export.apeldoorn_eigenaren_20210517 using gist (geovlak_3857);
create index on export.apeldoorn_eigenaren_20210517 (vid);
create index on export.apeldoorn_eigenaren_20210517 (gid);

SELECT vid, pid, buurt_code, buurtnaam, wijkcode, wijknaam, gemeentecode, gemeentenaam,
    openbareruimtenaam, huisnummer, huisletter, huisnummertoevoeging,
    type_eigenaar, naam_niet_natuurlijk_persoon,
    bouwjaar, oppervlakte,
    woning_type,
	geovlak_3857 AS geom
FROM export.apeldoorn_eigenaren_20210517;


select


SELECT (ST_Dump(the_geom)).geom from mytable;

/* get bbox */
SELECT ST_Extent(st_transform(st_setsrid(geovlak_3857, 3857), 4326)) as table_extent FROM export.apeldoorn_eigenaren_20210517 ae;



select *,
	case
	  when "ground-0.40" is not null and "roof-0.75" is not null then  ("roof-0.75" - "ground-0.40")::int
	else null
from export.apeldoorn_eigenaren_20210517 ae
left outer join cdf.energiepanden e on e.pid == ae.pid
