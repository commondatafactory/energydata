/* creates postcde 6 areas around buildings */

@set baghelp = baghelp
--create schema if not exists ${baghelp};
@set bagschema = bag202402
@set cbs = cbs2022

/* create postcode 6 gebieden van gebouwen */

drop table if exists ${baghelp}.pc6group;

create table ${baghelp}.pc6group as (
select
	postcode,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(p.geovlak, 2)
				)
			, 11, 'quad_segs=1')
		    )
		)
	)
	) as geovlak
from ${baghelp}.all_pvsl_nums n
left outer join ${bagschema}.pandactueelbestaand p on p.identificatie = n.pid
where pid is not null
--and postcode = '1121RM'
group by postcode);

/* add ligplaatsen to pc6groups */
insert into ${baghelp}.pc6group select
	postcode,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(l.geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geovlak
from ${baghelp}.all_pvsl_nums n
left outer join ${bagschema}.ligplaatsactueelbestaand l on l.identificatie = n.lid
where lid is not null
--and postcode = '1121RM'
group by postcode

/* add standplaatsen to pc6groups */
insert into ${baghelp}.pc6group
select
	postcode,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(s.geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	) as geovlak
from ${baghelp}.all_pvsl_nums n
left outer join ${bagschema}.standplaatsactueelbestaand s on s.identificatie = n.sid
where sid is not null
--and postcode = '1121RM'
group by postcode


-- select distinct on (postcode) postcode, geovlak from ${baghelp}.pc6group order by postcode;

drop table if exists ${baghelp}.pc6group_all;
create table ${baghelp}.pc6group_all as
select
	postcode,
	st_multi(ST_MakeValid(
		ST_Union(
		    p.geovlak
		  )
		)
	) as geovlak
from ${baghelp}.pc6group p
--where postcode = '1011AE'
group by postcode

/* sanity checks */

select count(*) from ${baghelp}.pc6group_all;
select count(*) from ${baghelp}.pc6group;
/* should be the same as a count */
select count(distinct(postcode)) from ${baghelp}.pc6group_all;

/* many postalcodes have similare geoare since they are in a high - rise building */

select * from (select *, row_number() over (partition by postcode order by postcode) as row_num
from $baghelp.pc6group ) as sub where sub.row_num > 1;

select * from (
select
    postcode,
	geovlak,
	row_number() over (partition by geovlak order by postcode) as rn
from ${baghelp}.pc6group_all
) dups
where dups.rn > 1 and postcode like '10%'
order by geovlak



select * from ${baghelp}.pc6group where postcode = '1011AE'
select * from ${baghelp}.pc6group_all where postcode = '1011AE'

create index on ${baghelp}.pc6group_all (postcode);
create index on ${baghelp}.pc6group_all using gist( geovlak );


SELECT
	fid,
	c.postcode,
	aantal_inwoners,
	aantal_mannen,
	aantal_vrouwen,
	aantal_inwoners_0_tot_15_jaar,
	aantal_inwoners_15_tot_25_jaar,
	aantal_inwoners_25_tot_45_jaar,
	aantal_inwoners_45_tot_65_jaar,
	aantal_inwoners_65_jaar_en_ouder, percentage_geb_nederland_herkomst_nederland, percentage_geb_nederland_herkomst_overig_europa, percentage_geb_nederland_herkomst_buiten_europa,
	percentage_geb_buiten_nederland_herkomst_europa, percentage_geb_buiten_nederland_herkmst_buiten_europa, aantal_part_huishoudens, aantal_eenpersoonshuishoudens,
	aantal_meerpersoonshuishoudens_zonder_kind, aantal_eenouderhuishoudens, aantal_tweeouderhuishoudens, gemiddelde_huishoudensgrootte,
	aantal_woningen, aantal_woningen_bouwjaar_voor_1945, aantal_woningen_bouwjaar_45_tot_65, aantal_woningen_bouwjaar_65_tot_75, aantal_woningen_bouwjaar_75_tot_85,
	aantal_woningen_bouwjaar_85_tot_95,
	aantal_woningen_bouwjaar_95_tot_05,
	aantal_woningen_bouwjaar_05_tot_15,
	aantal_woningen_bouwjaar_15_en_later,
	aantal_meergezins_woningen,
	percentage_koopwoningen,
	percentage_huurwoningen,
	aantal_huurwoningen_in_bezit_woningcorporaties,
	aantal_niet_bewoonde_woningen,
	gemiddelde_woz_waarde_woning,
	aantal_personen_met_uitkering_onder_aowlft,
	geovlak
	--geom
FROM cbspostcode.cbs_pc6_2022 c
left outer join ${baghelp}.pc6group_all pc6 on pc6.postcode = c.postcode
-- where pc6.postcode like '1024L%'; postalcodes 'duplicate location'.


select * from ${baghelp}.pand_postcode;

select * from ${baghelp}.all_pvsl_nums where postcode like '1024L%';
select * from ${baghelp}.
--  create

select * from ${baghelp}.pc6group_all;

drop table if exists bagvector.postcode6;

create table bagvector.postcode6 as (
    SELECT
    row_number() over (order by postcode) as fid,
  	postcode,
  	st_transform(geovlak, 3857) as geom
  	from ${baghelp}.pc6group_all
)

create index on bagvector.postcode6 using gist (geom);
create index on bagvector.postcode6 (postcode);

/* create a table where  p6 is combined with gas network information */
drop table if exists baghelp.p6_netbeheercorrected;


select p.postcode, st_multi(st_intersection(p.geovlak, st_transform(gs.geom, 28992))) as geovlak, gs."name", gs.netbeheerdercode
--into baghelp.p6_netbeheercorrected
from baghelp.pc6group_all p
left outer join baghelp.gasnetbeheer_subdivided gs on st_within(st_transform(st_pointonsurface(p.geovlak), 4326), gs.geom)



select * from baghelp.p6_netbeheercorrected pn;

/* no postalcode should appear more then once */
select postcode, count(*) from baghelp.p6_netbeheercorrected
group by postcode
having count(postcode) > 1;

/* */
select * from import.gasnetbeheerdersvlak g;

select * from kv.kleinverbruik_2023 kv where kv.postcode_van <= '1011AB' and kv.postcode_tot >= '1011AB'

select netgebied from kv.kleinverbruik_2023 k group by netgebied;
select netbeheerder from kv.kleinverbruik_2023 group by netbeheerder;
select organisation from eancodeboek.postcode_gridarea pg group by organisation;


alter table eancodeboek.postcode_gridarea add column kv_netbeheerder varchar(50);

update eancodeboek.postcode_gridarea
set kv_netbeheerder = case
	when organisation = 'Coteq Netbeheer B.V.' then 'coteq'
	when organisation = 'Enexis B.V.' then 'enexis'
	when organisation = 'Liander N.V.' then 'liander'
	when organisation = 'N.V. RENDO' then 'rendo'
	when organisation = 'Royal Schiphol Group N.V.' then 'shiphol'
	when organisation = 'Stedin Netbeheer B.V.' then 'stedin'
	when organisation = 'Westland Infra Netbeheer B.V.' then 'westland'
	else 'unknown'
end

select * from eancodeboek.postcode_gridarea pg;

select * from baghelp.p6_netbeheercorrected;

select * from kv.kleinverbruik_2023 k
left outer join baghelp.p6_netbeheercorrected p6n on postcode_van <= p6n.postcode and postcode_tot >=p6n.postcode and p6n.netbeheerdercode = k.netbeheerder


create schema kv_bag;

-- select netbeheerder from kv.kleinverbruik_2023 group by netbeheerder;

create index on kv.kleinverbruik_2023 (postcode_van);
create index on kv.kleinverbruik_2023 (postcode_tot);
create index on kv.kleinverbruik_2023 (postcode_van, postcode_tot);
create index on kv.kleinverbruik_2023 (netbeheerder, postcode_van, postcode_tot);

create index on baghelp.p6_netbeheercorrected (postcode);
create index on baghelp.p6_netbeheercorrected (netbeheerdercode);
create index on baghelp.p6_netheheercorrected (netbeheerdercode, postcode);

--create index on kv.kleinverbruik_2023 (netbeheerder);
--create index on beghelp.

-- select * from import.gasnetbeheerdersvlak g;

-- select * from kv.kleinverbruik_2023 k ;
drop table if exists kv_bag.kleinverbruik_2023_p6_netbeheerders;
/* takes 30 mins */
explain select *
into kv_bag.kleinverbruik_2023_p6_netbeheerders
from baghelp.p6_netbeheercorrected pn
left outer join kv.kleinverbruik_2023 k on postcode_van >= pn.postcode and pn.postcode <= postcode_tot and k.netbeheerder = pn.netbeheerdercode

--limit 40000

-- where netbeheerdercode is null

select *
into kv_bag.kleinverbruik_2023_p6_netbeheerders
from baghelp.p6_netbeheercorrected pn,
lateral (
	select * from kv.kleinverbruik_2023 k where postcode_van <= postcode and postcode_tot >= postcode and k.netbeheerder = pn.netbeheerdercode
) kv limit 40000


/* seems to be very slow */
select *
into kv_bag.kleinverbruik_2023_p6_netbeheerders
from  kv.kleinverbruik_2023 k
left outer join baghelp.p6_netbeheercorrected pn on pn.postcode >= postcode_van and pn.postcode <= postcode_tot and k.netbeheerder = pn.netbeheerdercode
-- where k.postcode_van < '2000';



select netbeheerder, count(*) from kv.kleinverbruik_2023 k  group by netbeheerder;
select netbeheerdercode, count(*) from kv_bag.kleinverbruik_2023_p6_netbeheerders pn group by netbeheerdercode ;

select * from baghelp.p6_netbeheercorrected pn;



select netbeheerdercode  from baghelp.gasnetbeheer_subdivided gs group by netbeheerdercode;

select * from import.gasnetbeheerdersvlak g;

select * from kv_bag.kleinverbruik_2023_p6_netbeheerders;

select leveringsrichting, count(*) from kv_bag.kleinverbruik_2023_p6_netbeheerders group by leveringsrichting;



select response->'MeteringPoints' from import.eancodes202402responses er ;

