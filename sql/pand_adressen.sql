/*

We relate panden with addresses here. Since public energy data
is published by postal code. Buildings with no postal code 'appartently'
do not use energy.

The resulting pand_postcode tables are used to filter and select panden.

*/

/* intermediate table that relates panden - vbo - nummeraanduiding  - pvn
 * create a key that is usable with to compare with enery labels.
 * */

drop table if exists baghelp.pand_vbo_nums;

select
	p.identificatie as pid,
	vbo.identificatie as vid,
	na.identificatie as numid,
	na.postcode,
	na.huisnummer,
	na.huisletter,
	na.huisnummertoevoeging,
	na.gerelateerdeopenbareruimte,
	upper(concat(na.postcode, ' ', na.huisnummer, na.huisletter, na.huisnummertoevoeging)) as ekey
into baghelp.pand_vbo_nums
from bag202101.pandactueelbestaand p
  left join bag202101.verblijfsobjectpandactueelbestaand vbop on  (vbop.gerelateerdpand = p.identificatie)
  left join bag202101.verblijfsobjectactueelbestaand vbo on (vbo.identificatie = vbop.identificatie)
  left join bag202101.nummeraanduidingactueelbestaand na on (na.identificatie = vbo.hoofdadres);




drop table if exists baghelp.pand_postcode_agg;

select p.pid, array_agg(postcode) postcode
into baghelp.pand_postcode_agg
from baghelp.pand_vbo_nums p
where postcode is not null
group by p.pid;

drop table if exists baghelp.pand_postcode;

select p.pid, postcode
into baghelp.pand_postcode
from baghelp.pand_vbo_nums p
where postcode is not null;




select count(*) from baghelp.pand_vbo_nums where postcode is null;

drop table if exists baghelp.pand_addressen;

select p.identificatie pid, vp.identificatie vid, n.postcode, n.gerelateerdeopenbareruimte straat, n.huisnummer
into baghelp.pand_addressen
from bag202101.pandactueelbestaand p
left join bag202101.verblijfsobjectpandactueelbestaand vp on vp.gerelateerdpand = p.identificatie
left outer join bag202101.verblijfsobjectactueelbestaand v on (v.identificatie = vp.identificatie)
left outer join bag202101.nummeraanduidingactueelbestaand n on (n.identificatie = v.hoofdadres);

select count(*) from baghelp.pand_addressen;
select count(*) from bag202101.pandactueelbestaand p;

select count(*) from bag202101.verblijfsobjectactueelbestaand v
left outer join bag202101.verblijfsobjectpandactueelbestaand v2 on v2.identificatie = v.identificatie
where v2.identificatie is not null;

drop table if exists baghelp.pand_agg_adressen;

/* collect all adresses for a pand */
select
	pid,
	min(postcode) mpostcode,
	array_agg(postcode) postcodes, array_agg(straat) straat,
	array_agg(huisnummer) huisnummers
into baghelp.pand_agg_adressen
from baghelp.pand_addressen pa
left outer join bag202101.openbareruimteactueelbestaand o on (o.identificatie = pa.straat)
where postcode is not null
group by pid;

drop table if exists baghelp.pand_buurcode;
/* Do a geo query to join with buurten. */
select cb.buurtcode , cb.buurtnaam, p.identificatie as pid
into baghelp.pand_buurtcode
from cbs.cbs_buurten_2020 cb
join bag202101.pandactueelbestaand p on ST_within(st_centroid(p.geovlak), cb.geom)
