#!/bin/bash
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=database
export PGPORT=5434
export PGUSER=cdf
export PGPASSWORD=insecure

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_wfs_layers/adimin_code.sql" -v "ON_ERROR_STOP=1"
