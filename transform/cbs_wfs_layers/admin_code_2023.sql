CREATE schema if not exists cbs_wfs;

DROP TABLE IF EXISTS cbs_wfs.gemeenten_2023 CASCADE;

CREATE TABLE cbs_wfs.gemeenten_2023 AS
SELECT
    a.gemeentenaam,
    a.gemeentecode,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_huishoudens,
    a.aantal_inwoners,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(ST_SnapToGrid(ST_curvetoline(a.geom_3857), 0.1))
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom,
    b.naam as provincienaam,
    b.code as provinciecode
FROM
    cbs2023.cdf_gem a
    JOIN cbs.provincies2012 b ON ST_Intersects(ST_PointOnSurface(a.geom), b.geom);

ALTER table cbs_wfs.gemeenten_2023 add column fid serial primary key;

CREATE INDEX gemeenten_2023_geom ON cbs_wfs.gemeenten_2023 USING gist (geom);
CREATE INDEX gemeenten_2023_idx ON cbs_wfs.gemeenten_2023 (gemeentecode);

DROP TABLE IF EXISTS cbs_wfs.wijken_2023 CASCADE;

CREATE TABLE cbs_wfs.wijken_2023 AS
SELECT
    a.wijknaam,
    a.wijkcode,
    a.gemeentenaam,
    a.gemeentecode,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_huishoudens,
    a.aantal_inwoners,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(ST_SnapToGrid(ST_curvetoline(a.geom_3857), 0.1))
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom,
    b.naam as provincienaam,
    b.code as provinciecode
FROM
    cbs2023.cdf_wijk a
    JOIN cbs.provincies2012 b ON ST_Intersects(ST_PointOnSurface(a.geom), b.geom);

CREATE INDEX wijken_2023_geom ON cbs_wfs.wijken_2023 USING gist (geom);
CREATE INDEX wijken_2023_idx ON cbs_wfs.wijken_2023 (wijkcode);

ALTER table cbs_wfs.wijken_2023 add column fid serial primary key;

DROP TABLE IF EXISTS cbs_wfs.buurten_2023 CASCADE;

CREATE TABLE cbs_wfs.buurten_2023 AS
SELECT
    a.buurtnaam,
    a.buurtcode,
    w.wijknaam,
    a.wijkcode,
    a.gemeentenaam,
    a.gemeentecode,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_huishoudens,
    a.aantal_inwoners,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(ST_SnapToGrid(ST_curvetoline(a.geom_3857), 0.1))
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom,
    b.naam as provincienaam,
    b.code as provinciecode
FROM
    cbs2023.cdf_buurt a
    JOIN cbs.provincies2012 b ON ST_Intersects(ST_PointOnSurface(a.geom), b.geom)
    JOIN cbs2023.wijken w ON ST_Intersects(ST_PointOnSurface(a.geom), w.geom);

ALTER table cbs_wfs.buurten_2023 add column fid serial primary key;

CREATE INDEX buurten_2023_geom ON cbs_wfs.buurten_2023 USING gist (geom);
CREATE INDEX buurten_2023_idx on cbs_wfs.buurten_2023 (buurtcode);
