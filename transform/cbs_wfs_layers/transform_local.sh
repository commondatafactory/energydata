#!/bin/bash
set -e
set -x

# Set connection Variables
export PGDATABASE=${PGDATABASE:-cdf}
export PGHOST=${PGHOST:-127.0.0.1}
export PGPORT=${PGPORT:-5432}
export PGUSER=${PGUSER:-cdf}
export PGPASSWORD=${PGPASSWORD:-insecure}

export YEAR=${2023:-$1}

psql -f "./transform/cbs_wfs_layers/admin_code_${YEAR}.sql" -v "ON_ERROR_STOP=1"
