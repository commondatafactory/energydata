DROP TABLE IF EXISTS tiles.admin_labels_2023_14_20_uo CASCADE;

CREATE TABLE tiles.admin_labels_2023_14_20_uo (
    fid SERIAL PRIMARY KEY,
    name TEXT,
    code TEXT ,
    hierarchy INT,
    geom geometry(POINT, 3857)
);

CREATE INDEX admin_labels_2023_14_20_uo_geom ON tiles.admin_labels_2023_14_20_uo USING gist (geom);

CREATE INDEX admin_labels_2023_14_20_uo_geohash ON tiles.admin_labels_2023_14_20_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

INSERT INTO
    tiles.admin_labels_2023_14_20_uo (name, code, hierarchy, geom)
SELECT
    buurtnaam,
    buurtcode ,
    3 AS hierarchy,
    ST_PointOnSurface(geom_3857) :: geometry(POINT, 3857)
FROM
    cbs2023.cdf_buurt;


DROP TABLE IF EXISTS  tiles.admin_labels_2023_14_20 CASCADE;
CREATE TABLE tiles.admin_labels_2023_14_20 AS
  SELECT * FROM tiles.admin_labels_2023_14_20_uo
    ORDER BY ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10) COLLATE "C";

CREATE INDEX cdf_admin_labels_2023_14_20_geom
    ON tiles.admin_labels_2023_14_20 USING gist (geom);

CREATE INDEX cdf_admin_labels_2023_14_20_geohash ON tiles.admin_labels_2023_14_20 (ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10));

CLUSTER tiles.admin_labels_2023_14_20 USING cdf_admin_labels_2023_14_20_geohash;

DROP TABLE IF EXISTS tiles.admin_labels_2023_14_20_uo CASCADE;

VACUUM ANALYZE tiles.admin_labels_2023_14_20;
