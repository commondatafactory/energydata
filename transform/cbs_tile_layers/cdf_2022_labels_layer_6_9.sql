DROP TABLE IF EXISTS tiles.admin_labels_2022_6_9_uo CASCADE;

CREATE TABLE tiles.admin_labels_2022_6_9_uo (
    fid SERIAL PRIMARY KEY,
    name TEXT,
    code TEXT ,
    hierarchy INT,
    geom geometry(POINT, 3857)
);

CREATE INDEX admin_labels_2022_6_9_uo_geom ON tiles.admin_labels_2022_6_9_uo USING gist (geom);

CREATE INDEX admin_labels_2022_6_9_uo_geohash ON tiles.admin_labels_2022_6_9_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

INSERT INTO
    tiles.admin_labels_2022_6_9_uo (name, code, hierarchy, geom)
SELECT
    gemeentenaam ,
    gemeentecode,
    1 AS hierarchy,
    ST_PointOnSurface(geom_3857) :: geometry(POINT, 3857)
FROM
    cbs2022.cdf_gem;

DROP TABLE IF EXISTS  tiles.admin_labels_2022_6_9 CASCADE;

CREATE TABLE tiles.admin_labels_2022_6_9 AS
  SELECT * FROM tiles.admin_labels_2022_6_9_uo
    ORDER BY ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10) COLLATE "C";

CREATE INDEX cdf_admin_labels_2022_6_9_geom
    ON tiles.admin_labels_2022_6_9 USING gist (geom);

CREATE INDEX cdf_admin_labels_2022_6_9_geohash ON tiles.admin_labels_2022_6_9 (ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10));

CLUSTER tiles.admin_labels_2022_6_9 USING cdf_admin_labels_2022_6_9_geohash;

DROP TABLE IF EXISTS tiles.admin_labels_2022_6_9_uo CASCADE;

VACUUM ANALYZE tiles.admin_labels_2022_6_9;
