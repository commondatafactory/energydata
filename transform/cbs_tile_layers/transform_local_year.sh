#!/bin/bash
set -e
set -x

# Set default connection Variables
export PGDATABASE=${PGDATABASE:-cdf}
export PGHOST=${PGHOST:-127.0.0.1}
export PGPORT=${PGPORT:-5433}
export PGUSER=${PGUSER:-cdf}
export PGPASSWORD=${PGPASSWORD:-insecure}


# check if year argument is present
if [[ -z "$1" ]]; then
  echo "Year argument is missing"
  exit 1
fi

# check if year argument is at least 4 characters long
if [[ ${#1} -lt 4 ]]; then
  echo "Year argument is too short"
  exit 1
fi

# labels - Creating custom layers out of cbs data
psql  -f "./transform/cbs_tile_layers/cdf_$1_labels_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql  -f "./transform/cbs_tile_layers/cdf_$1_labels_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql  -f "./transform/cbs_tile_layers/cdf_$1_labels_layer_14_20.sql" -v "ON_ERROR_STOP=1"

# Admin areas - Creating custom layers out of cbs data for each year

# year $1

psql -f "./transform/cbs_tile_layers/cdf_cbs_$1_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql -f "./transform/cbs_tile_layers/cdf_cbs_$1_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql -f "./transform/cbs_tile_layers/cdf_cbs_$1_layer_13_20.sql" -v "ON_ERROR_STOP=1"

