DROP TABLE IF EXISTS tiles.admin_areas_2021_6_9_uo CASCADE;

CREATE TABLE tiles.admin_areas_2021_6_9_uo AS
SELECT
    a.gemeentenaam as name,
    a.gemeentecode as code,
    1 as hierarchy,

    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.mannen,
    a.vrouwen,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_ongehuwd,
    a.percentage_gehuwd,
    a.percentage_gescheid,
    a.percentage_verweduwd,
    a.geboorte_totaal,
    a.geboortes_per_1000_inwoners,
    a.sterfte_totaal,
    a.sterfte_relatief,
    a.aantal_huishoudens,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_westerse_migratieachtergrond,
    a.percentage_niet_westerse_migratieachtergrond,
    a.percentage_uit_marokko,
    a.percentage_uit_nederlandse_antillen_en_aruba,
    a.percentage_uit_suriname,
    a.percentage_uit_turkije,
    a.percentage_overige_nietwestersemigratieachtergrond,
    a.aantal_bedrijven_landbouw_bosbouw_visserij,
    a.aantal_bedrijven_nijverheid_energie,
    a.aantal_bedrijven_handel_en_horeca,
    a.aantal_bedrijven_vervoer_informatie_communicatie,
    a.aantal_bedrijven_financieel_onroerend_goed,
    a.aantal_bedrijven_zakelijke_dienstverlening,
    a.aantal_bedrijven_overheid_onderwijs_en_zorg,
    a.aantal_bedrijven_cultuur_recreatie_overige,
    a.aantal_bedrijfsvestigingen,
    a.woningvoorraad,
    a.gemiddelde_woningwaarde,
    a.percentage_eengezinswoning,
    a.percentage_meergezinswoning,
    a.percentage_bewoond,
    a.percentage_koopwoningen,
    a.percentage_huurwoningen,
    a.perc_huurwoningen_in_bezit_woningcorporaties,
    a.perc_huurwoningen_in_bezit_overige_verhuurders,
    a.percentage_woningen_met_eigendom_onbekend,
    a.percentage_bouwjaarklasse_tot_2000,
    a.percentage_bouwjaarklasse_vanaf_2000,
    a.percentage_leegstand_woningen,
    a.personenautos_totaal,
    a.personenautos_per_huishouden,
    a.personenautos_per_km2,
    a.motortweewielers_totaal,
    a.aantal_personenautos_met_brandstof_benzine,
    a.aantal_personenautos_met_overige_brandstof,
    a.oppervlakte_totaal_in_ha,
    a.oppervlakte_land_in_ha,
    a.oppervlakte_water_in_ha,
    a.huisartsenpraktijk_gemiddelde_afstand_in_km,
    a.huisartsenpraktijk_gemiddeld_aantal_binnen_1_km,
    a.huisartsenpraktijk_gemiddeld_aantal_binnen_3_km,
    a.huisartsenpraktijk_gemiddeld_aantal_binnen_5_km,
    a.apotheek_gemiddelde_afstand_in_km,
    a.grote_supermarkt_gemiddelde_afstand_in_km,
    a.grote_supermarkt_gemiddeld_aantal_binnen_1_km,
    a.grote_supermarkt_gemiddeld_aantal_binnen_3_km,
    a.grote_supermarkt_gemiddeld_aantal_binnen_5_km,
    a.winkels_ov_dagelijkse_levensm_gem_afst_in_km,
    a.winkels_ov_dagel_levensm_gem_aantal_binnen_1_km,
    a.winkels_ov_dagel_levensm_gem_aantal_binnen_3_km,
    a.winkels_ov_dagel_levensm_gem_aantal_binnen_5_km,
    a.warenhuis_gemiddelde_afstand_in_km,
    a.warenhuis_gemiddeld_aantal_binnen_5_km,
    a.warenhuis_gemiddeld_aantal_binnen_10_km,
    a.warenhuis_gemiddeld_aantal_binnen_20_km,
    a.cafe_gemiddelde_afstand_in_km,
    a.cafe_gemiddeld_aantal_binnen_1_km,
    a.cafe_gemiddeld_aantal_binnen_3_km,
    a.cafe_gemiddeld_aantal_binnen_5_km,
    a.cafetaria_gemiddelde_afstand_in_km,
    a.cafetaria_gemiddeld_aantal_binnen_1_km,
    a.cafetaria_gemiddeld_aantal_binnen_3_km,
    a.cafetaria_gemiddeld_aantal_binnen_5_km,
    a.restaurant_gemiddelde_afstand_in_km,
    a.restaurant_gemiddeld_aantal_binnen_1_km,
    a.restaurant_gemiddeld_aantal_binnen_3_km,
    a.restaurant_gemiddeld_aantal_binnen_5_km,
    a.hotel_gemiddelde_afstand_in_km,
    a.hotel_gemiddeld_aantal_binnen_5_km,
    a.hotel_gemiddeld_aantal_binnen_10_km,
    a.hotel_gemiddeld_aantal_binnen_20_km,
    a.kinderdagverblijf_gemiddelde_afstand_in_km,
    a.kinderdagverblijf_gemiddeld_aantal_binnen_1_km,
    a.kinderdagverblijf_gemiddeld_aantal_binnen_3_km,
    a.kinderdagverblijf_gemiddeld_aantal_binnen_5_km,
    a.buitenschoolse_opvang_gem_afstand_in_km,
    a.buitenschoolse_opvang_gemiddeld_aantal_binnen_1_km,
    a.buitenschoolse_opvang_gemiddeld_aantal_binnen_3_km,
    a.buitenschoolse_opvang_gemiddeld_aantal_binnen_5_km,
    a.basisonderwijs_gemiddelde_afstand_in_km,
    a.basisonderwijs_gemiddeld_aantal_binnen_1_km,
    a.basisonderwijs_gemiddeld_aantal_binnen_3_km,
    a.basisonderwijs_gemiddeld_aantal_binnen_5_km,
    a.voortgezet_onderwijs_gem_afstand_in_km,
    a.voortgezet_onderwijs_gemiddeld_aantal_binnen_3_km,
    a.voortgezet_onderwijs_gemiddeld_aantal_binnen_5_km,
    a.voortgezet_onderwijs_gemiddeld_aantal_binnen_10_km,
    a.vmbo_gemiddelde_afstand_in_km,
    a.vmbo_gemiddeld_aantal_binnen_3_km,
    a.vmbo_gemiddeld_aantal_binnen_5_km,
    a.vmbo_gemiddeld_aantal_binnen_10_km,
    a.havo_vwo_gemiddelde_afstand_in_km,
    a.havo_vwo_gemiddeld_aantal_binnen_3_km,
    a.havo_vwo_gemiddeld_aantal_binnen_5_km,
    a.havo_vwo_gemiddeld_aantal_binnen_10_km,
    a.brandweerkazerne_gemiddelde_afstand_in_km,
    a.zwembad_gemiddelde_afstand_in_km,
    a.kunstijsbaan_gemiddelde_afstand_in_km,
    a.bibliotheek_gemiddelde_afstand_in_km,
    a.bioscoop_gemiddelde_afstand_in_km,
    a.bioscoop_gemiddeld_aantal_binnen_5_km,
    a.bioscoop_gemiddeld_aantal_binnen_10_km,
    a.bioscoop_gemiddeld_aantal_binnen_20_km,
    a.sauna_gemiddelde_afstand_in_km,
    a.zonnebank_gemiddelde_afstand_in_km,
    a.attractiepark_gemiddelde_afstand_in_km,
    a.attractiepark_gemiddeld_aantal_binnen_10_km,
    a.attractiepark_gemiddeld_aantal_binnen_20_km,
    a.attractiepark_gemiddeld_aantal_binnen_50_km,

    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(
                    ST_SnapToGrid(ST_curvetoline(a.geom_3857), 0.0001)
                )
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom
FROM
    cbs2021.cdf_gem as a;

ALTER TABLE
    tiles.admin_areas_2021_6_9_uo
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX admin_areas_2021_6_9_uo_geom ON tiles.admin_areas_2021_6_9_uo USING gist(geom);

CREATE INDEX admin_areas_2021_6_9_uo_geohash ON tiles.admin_areas_2021_6_9_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

DROP TABLE IF EXISTS tiles.admin_areas_2021_6_9 CASCADE;

CREATE TABLE tiles.admin_areas_2021_6_9 AS
SELECT
    *
FROM
    tiles.admin_areas_2021_6_9_uo
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10) COLLATE "C";

CREATE INDEX cdf_admin_areas_2021_6_9_geom ON tiles.admin_areas_2021_6_9 USING gist (geom);

CREATE INDEX cdf_admin_areas_2021_6_9_geohash ON tiles.admin_areas_2021_6_9 (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

CLUSTER tiles.admin_areas_2021_6_9 USING cdf_admin_areas_2021_6_9_geohash;

DROP TABLE IF EXISTS tiles.admin_areas_2021_6_9_uo CASCADE;
