#!/bin/bash
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure

# labels - Creating custom layers out of cbs data
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_labels_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_labels_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_labels_layer_14_20.sql" -v "ON_ERROR_STOP=1"

# Admin areas - Creating custom layers out of cbs data for each year

# 2021

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2021_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2021_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2021_layer_13_20.sql" -v "ON_ERROR_STOP=1"


# 2020

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2020_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2020_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2020_layer_13_20.sql" -v "ON_ERROR_STOP=1"


# 2019

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2019_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2019_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2019_layer_13_20.sql" -v "ON_ERROR_STOP=1"


# 2018
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2018_layer_6_9.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2018_layer_10_13.sql" -v "ON_ERROR_STOP=1"

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./transform/cbs_tile_layers/cdf_cbs_2018_layer_13_20.sql" -v "ON_ERROR_STOP=1"
