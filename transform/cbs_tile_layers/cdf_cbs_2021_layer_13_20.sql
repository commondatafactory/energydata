DROP TABLE IF EXISTS tiles.admin_areas_2021_13_20_uo CASCADE;

CREATE TABLE tiles.admin_areas_2021_13_20_uo AS
SELECT
    a.buurtnaam as name,
    a.buurtcode as code,
    3 as hierarchy,

    wijkcode,
    gemeentecode,
    gemeentenaam,
    indelingswijziging_wijken_en_buurten,
    water,
    meest_voorkomende_postcode,
    dekkingspercentage,
    omgevingsadressendichtheid,
    stedelijkheid_adressen_per_km2,
    bevolkingsdichtheid_inwoners_per_km2,
    aantal_inwoners,
    mannen,
    vrouwen,
    percentage_personen_0_tot_15_jaar,
    percentage_personen_15_tot_25_jaar,
    percentage_personen_25_tot_45_jaar,
    percentage_personen_45_tot_65_jaar,
    percentage_personen_65_jaar_en_ouder,
    percentage_ongehuwd,
    percentage_gehuwd,
    percentage_gescheid,
    percentage_verweduwd,
    geboorte_totaal,
    geboortes_per_1000_inwoners,
    sterfte_totaal,
    sterfte_relatief,
    aantal_huishoudens,
    percentage_eenpersoonshuishoudens,
    percentage_huishoudens_zonder_kinderen,
    percentage_huishoudens_met_kinderen,
    gemiddelde_huishoudsgrootte,
    percentage_westerse_migratieachtergrond,
    percentage_niet_westerse_migratieachtergrond,
    percentage_uit_marokko,
    percentage_uit_nederlandse_antillen_en_aruba,
    percentage_uit_suriname,
    percentage_uit_turkije,
    percentage_overige_nietwestersemigratieachtergrond,
    aantal_bedrijven_landbouw_bosbouw_visserij,
    aantal_bedrijven_nijverheid_energie,
    aantal_bedrijven_handel_en_horeca,
    aantal_bedrijven_vervoer_informatie_communicatie,
    aantal_bedrijven_financieel_onroerend_goed,
    aantal_bedrijven_zakelijke_dienstverlening,
    aantal_bedrijven_overheid_onderwijs_en_zorg,
    aantal_bedrijven_cultuur_recreatie_overige,
    aantal_bedrijfsvestigingen,
    woningvoorraad,
    gemiddelde_woningwaarde,
    percentage_eengezinswoning,
    percentage_meergezinswoning,
    percentage_bewoond,
    percentage_koopwoningen,
    percentage_huurwoningen,
    perc_huurwoningen_in_bezit_woningcorporaties,
    perc_huurwoningen_in_bezit_overige_verhuurders,
    percentage_woningen_met_eigendom_onbekend,
    percentage_bouwjaarklasse_tot_2000,
    percentage_bouwjaarklasse_vanaf_2000,
    percentage_leegstand_woningen,
    personenautos_totaal,
    personenautos_per_huishouden,
    personenautos_per_km2,
    motortweewielers_totaal,
    aantal_personenautos_met_brandstof_benzine,
    aantal_personenautos_met_overige_brandstof,
    oppervlakte_totaal_in_ha,
    oppervlakte_land_in_ha,
    oppervlakte_water_in_ha,
    huisartsenpraktijk_gemiddelde_afstand_in_km,
    huisartsenpraktijk_gemiddeld_aantal_binnen_1_km,
    huisartsenpraktijk_gemiddeld_aantal_binnen_3_km,
    huisartsenpraktijk_gemiddeld_aantal_binnen_5_km,
    apotheek_gemiddelde_afstand_in_km,
    grote_supermarkt_gemiddelde_afstand_in_km,
    grote_supermarkt_gemiddeld_aantal_binnen_1_km,
    grote_supermarkt_gemiddeld_aantal_binnen_3_km,
    grote_supermarkt_gemiddeld_aantal_binnen_5_km,
    winkels_ov_dagelijkse_levensm_gem_afst_in_km,
    winkels_ov_dagel_levensm_gem_aantal_binnen_1_km,
    winkels_ov_dagel_levensm_gem_aantal_binnen_3_km,
    winkels_ov_dagel_levensm_gem_aantal_binnen_5_km,
    warenhuis_gemiddelde_afstand_in_km,
    warenhuis_gemiddeld_aantal_binnen_5_km,
    warenhuis_gemiddeld_aantal_binnen_10_km,
    warenhuis_gemiddeld_aantal_binnen_20_km,
    cafe_gemiddelde_afstand_in_km,
    cafe_gemiddeld_aantal_binnen_1_km,
    cafe_gemiddeld_aantal_binnen_3_km,
    cafe_gemiddeld_aantal_binnen_5_km,
    cafetaria_gemiddelde_afstand_in_km,
    cafetaria_gemiddeld_aantal_binnen_1_km,
    cafetaria_gemiddeld_aantal_binnen_3_km,
    cafetaria_gemiddeld_aantal_binnen_5_km,
    restaurant_gemiddelde_afstand_in_km,
    restaurant_gemiddeld_aantal_binnen_1_km,
    restaurant_gemiddeld_aantal_binnen_3_km,
    restaurant_gemiddeld_aantal_binnen_5_km,
    hotel_gemiddelde_afstand_in_km,
    hotel_gemiddeld_aantal_binnen_5_km,
    hotel_gemiddeld_aantal_binnen_10_km,
    hotel_gemiddeld_aantal_binnen_20_km,
    kinderdagverblijf_gemiddelde_afstand_in_km,
    kinderdagverblijf_gemiddeld_aantal_binnen_1_km,
    kinderdagverblijf_gemiddeld_aantal_binnen_3_km,
    kinderdagverblijf_gemiddeld_aantal_binnen_5_km,
    buitenschoolse_opvang_gem_afstand_in_km,
    buitenschoolse_opvang_gemiddeld_aantal_binnen_1_km,
    buitenschoolse_opvang_gemiddeld_aantal_binnen_3_km,
    buitenschoolse_opvang_gemiddeld_aantal_binnen_5_km,
    basisonderwijs_gemiddelde_afstand_in_km,
    basisonderwijs_gemiddeld_aantal_binnen_1_km,
    basisonderwijs_gemiddeld_aantal_binnen_3_km,
    basisonderwijs_gemiddeld_aantal_binnen_5_km,
    voortgezet_onderwijs_gem_afstand_in_km,
    voortgezet_onderwijs_gemiddeld_aantal_binnen_3_km,
    voortgezet_onderwijs_gemiddeld_aantal_binnen_5_km,
    voortgezet_onderwijs_gemiddeld_aantal_binnen_10_km,
    vmbo_gemiddelde_afstand_in_km,
    vmbo_gemiddeld_aantal_binnen_3_km,
    vmbo_gemiddeld_aantal_binnen_5_km,
    vmbo_gemiddeld_aantal_binnen_10_km,
    havo_vwo_gemiddelde_afstand_in_km,
    havo_vwo_gemiddeld_aantal_binnen_3_km,
    havo_vwo_gemiddeld_aantal_binnen_5_km,
    havo_vwo_gemiddeld_aantal_binnen_10_km,
    brandweerkazerne_gemiddelde_afstand_in_km,
    zwembad_gemiddelde_afstand_in_km,
    kunstijsbaan_gemiddelde_afstand_in_km,
    bibliotheek_gemiddelde_afstand_in_km,
    bioscoop_gemiddelde_afstand_in_km,
    bioscoop_gemiddeld_aantal_binnen_5_km,
    bioscoop_gemiddeld_aantal_binnen_10_km,
    bioscoop_gemiddeld_aantal_binnen_20_km,
    sauna_gemiddelde_afstand_in_km,
    zonnebank_gemiddelde_afstand_in_km,
    attractiepark_gemiddelde_afstand_in_km,
    attractiepark_gemiddeld_aantal_binnen_10_km,
    attractiepark_gemiddeld_aantal_binnen_20_km,
    attractiepark_gemiddeld_aantal_binnen_50_km,

    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(ST_SnapToGrid(ST_curvetoline(geom_3857), 0.0001))
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom
FROM
    cbs2021.cdf_buurt as a;

ALTER TABLE
    tiles.admin_areas_2021_13_20_uo
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX admin_areas_2021_13_20_uo_geom ON tiles.admin_areas_2021_13_20_uo USING gist(geom);

CREATE INDEX admin_areas_2021_13_20_uo_geohash ON tiles.admin_areas_2021_13_20_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 6)
);

DROP TABLE IF EXISTS tiles.admin_areas_2021_13_20 CASCADE;

CREATE TABLE tiles.admin_areas_2021_13_20 AS
SELECT
    *
FROM
    tiles.admin_areas_2021_13_20_uo
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 6) COLLATE "C";

CREATE INDEX cdf_admin_areas_2021_13_20_geom ON tiles.admin_areas_2021_13_20 USING gist (geom);

CREATE INDEX cdf_admin_areas_2021_13_20_geohash ON tiles.admin_areas_2021_13_20 (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 6)
);

CLUSTER tiles.admin_areas_2021_13_20 USING cdf_admin_areas_2021_13_20_geohash;

DROP TABLE IF EXISTS tiles.admin_areas_2021_13_20_uo CASCADE;
