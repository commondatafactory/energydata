DROP TABLE IF EXISTS tiles.admin_areas_2020_13_20_uo CASCADE;

CREATE TABLE tiles.admin_areas_2020_13_20_uo AS
SELECT
    a.buurtnaam as name,
    a.buurtcode as code,
    3 as hierarchy,

    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.mannen,
    a.vrouwen,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_ongehuwd,
    a.percentage_gehuwd,
    a.percentage_gescheid,
    a.percentage_verweduwd,
    a.aantal_huishoudens,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_westerse_migratieachtergrond,
    a.percentage_niet_westerse_migratieachtergrond,
    a.percentage_uit_marokko,
    a.percentage_uit_nederlandse_antillen_en_aruba,
    a.percentage_uit_suriname,
    a.percentage_uit_turkije,
    a.percentage_overige_nietwestersemigratieachtergrond,
    a.oppervlakte_totaal_in_ha,
    a.oppervlakte_land_in_ha,
    a.oppervlakte_water_in_ha,

    a.aantal_bedrijven_landbouw_bosbouw_visserij,
    a.aantal_bedrijven_nijverheid_energie,
    a.aantal_bedrijven_handel_en_horeca,
    a.aantal_bedrijven_vervoer_informatie_communicatie,
    a.aantal_bedrijven_financieel_onroerend_goed,
    a.aantal_bedrijven_zakelijke_dienstverlening,
    a.aantal_bedrijven_cultuur_recreatie_overige,
    a.aantal_bedrijfsvestigingen,
    a.woningvoorraad,
    a.gemiddelde_woningwaarde,
    a.percentage_eengezinswoning,
    a.percentage_meergezinswoning,
    a.percentage_bewoond,
    a.percentage_koopwoningen,
    a.percentage_huurwoningen,
    a.perc_huurwoningen_in_bezit_woningcorporaties,
    a.perc_huurwoningen_in_bezit_overige_verhuurders,
    a.percentage_woningen_met_eigendom_onbekend,
    a.percentage_bouwjaarklasse_tot_2000,
    a.percentage_bouwjaarklasse_vanaf_2000,
    a.percentage_leegstand_woningen,

    a.gemiddeld_gasverbruik_totaal,
    a.gemiddeld_gasverbruik_appartement,
    a.gemiddeld_gasverbruik_tussenwoning,
    a.gemiddeld_gasverbruik_hoekwoning,
    a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    a.gemiddeld_gasverbruik_vrijstaande_woning,
    a.gemiddeld_gasverbruik_huurwoning,
    a.gemiddeld_gasverbruikkoopwoning,
    a.gemiddeld_elektriciteitsverbruik_totaal,
    a.gemiddeld_elektriciteitsverbruik_appartement,
    a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    a.gem_elektriciteitsverbruik_vrijstaande_woning,
    a.gemiddeld_elektriciteitsverbruik_huurwoning,
    a.gemiddeld_elektriciteitsverbruikkoopwoning,

    a.aantal_personen_met_een_ao_uitkering_totaal,
    a.aantal_personen_met_een_ww_uitkering_totaal,
    a.aantal_personen_met_een_algemene_bijstandsuitkering_totaal,
    a.aantal_personen_met_een_aow_uitkering_totaal,
    a.personenautos_totaal,
    a.personenautos_per_huishouden,
    a.personenautos_per_km2,
    a.motortweewielers_totaal,
    a.aantal_personenautos_met_brandstof_benzine,
    a.aantal_personenautos_met_overige_brandstof

    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(ST_SnapToGrid(ST_curvetoline(geom_3857), 0.0001))
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom
FROM
    cbs.cdf_buurt_2020 as a;

ALTER TABLE
    tiles.admin_areas_2020_13_20_uo
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX admin_areas_2020_13_20_uo_geom ON tiles.admin_areas_2020_13_20_uo USING gist(geom);

CREATE INDEX admin_areas_2020_13_20_uo_geohash ON tiles.admin_areas_2020_13_20_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 6)
);

DROP TABLE IF EXISTS tiles.admin_areas_2020_13_20 CASCADE;

CREATE TABLE tiles.admin_areas_2020_13_20 AS
SELECT
    *
FROM
    tiles.admin_areas_2020_13_20_uo
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 6) COLLATE "C";

CREATE INDEX cdf_admin_areas_2020_13_20_geom ON tiles.admin_areas_2020_13_20 USING gist (geom);

CREATE INDEX cdf_admin_areas_2020_13_20_geohash ON tiles.admin_areas_2020_13_20 (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 6)
);

CLUSTER tiles.admin_areas_2020_13_20 USING cdf_admin_areas_2020_13_20_geohash;

DROP TABLE IF EXISTS tiles.admin_areas_2020_13_20_uo CASCADE;
