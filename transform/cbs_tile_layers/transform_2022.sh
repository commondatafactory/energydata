#!/bin/bash
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=database
export PGPORT=5432
export PGUSER=cdf
export PGPASSWORD=insecure

# labels - Creating custom layers out of cbs data
docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_tile_layers/cdf_labels_layer_6_9.sql" -v "ON_ERROR_STOP=1"

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_tile_layers/cdf_labels_layer_10_13.sql" -v "ON_ERROR_STOP=1"

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_tile_layers/cdf_labels_layer_14_20.sql" -v "ON_ERROR_STOP=1"

# Admin areas - Creating custom layers out of cbs data for each year

# 2021

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_tile_layers/cdf_cbs_2021_layer_6_9.sql" -v "ON_ERROR_STOP=1"

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_tile_layers/cdf_cbs_2021_layer_10_13.sql" -v "ON_ERROR_STOP=1"

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/transform/cbs_tile_layers/cdf_cbs_2021_layer_13_20.sql" -v "ON_ERROR_STOP=1"


