DROP TABLE IF EXISTS tiles.admin_areas_2018_10_13_uo CASCADE;


CREATE TABLE tiles.admin_areas_2018_10_13_uo AS
SELECT
    a.wijknaam as name,
    a.wijkcode as code,
    2 as hierarchy,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.aantal_huishoudens,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,

    a.aantal_inkomensontvangers,
    a.gemiddeld_inkomen_per_inwoner,
    a.gemiddeld_inkomen_per_inkomensontvanger,
    a.percentage_personen_met_laag_inkomen,
    a.percentage_personen_met_hoog_inkomen,
    a.percentage_huishoudens_met_laag_inkomen,
    a.percentage_huishoudens_met_hoog_inkomen,

    a.aantal_bedrijven_landbouw_bosbouw_visserij,
    a.aantal_bedrijven_nijverheid_energie,
    a.aantal_bedrijven_handel_en_horeca,
    a.aantal_bedrijven_vervoer_informatie_communicatie,
    a.aantal_bedrijven_financieel_onroerend_goed,
    a.aantal_bedrijven_zakelijke_dienstverlening,
    a.aantal_bedrijven_cultuur_recreatie_overige,
    a.aantal_bedrijfsvestigingen,

    a.percentage_huishoudens_met_lage_koopkracht,
    a.aantal_personen_met_een_ao_uitkering_totaal,
    a.aantal_personen_met_een_ww_uitkering_totaal,
    a.aantal_personen_met_een_algemene_bijstandsuitkering_totaal,
    a.aantal_personen_met_een_aow_uitkering_totaal,

    a.totaal_diefstal,
    a.vernieling_misdrijf_tegen_openbare_orde,
    a.gewelds_en_seksuele_misdrijven,

    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.woningvoorraad,
    a.gemiddelde_woningwaarde,
    a.percentage_koopwoningen,
    a.percentage_huurwoningen,
    a.perc_huurwoningen_in_bezit_woningcorporaties,
    a.perc_huurwoningen_in_bezit_overige_verhuurders,
    a.percentage_woningen_met_eigendom_onbekend,
    a.percentage_bouwjaarklasse_tot_2000,
    a.percentage_bouwjaarklasse_vanaf_2000,
    a.gemiddeld_gasverbruik_appartement,
    a.gemiddeld_gasverbruik_tussenwoning,
    a.gemiddeld_gasverbruik_hoekwoning,
    a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    a.gemiddeld_gasverbruik_vrijstaande_woning,
    a.gemiddeld_gasverbruik_huurwoning,
    a.gemiddeld_gasverbruikkoopwoning,
    a.gemiddeld_elektriciteitsverbruik_totaal,
    a.gemiddeld_elektriciteitsverbruik_appartement,
    a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    a.gem_elektriciteitsverbruik_vrijstaande_woning,
    a.gemiddeld_elektriciteitsverbruik_huurwoning,
    a.gemiddeld_elektriciteitsverbruikkoopwoning,

    b.h50_i25_wpg,
    b.h50_i25_wpa,
    b.er8p_i_wpg,
    b.er8p_i_wpa,
    b.aandeel_koopwoningen,
    b.financieel_wpg,
    b.m5000,
    b.m10000,
    b.m15000,
    b.m20000,
    b.m25000,
    b.m30000,
    b.m35000,
    b.m40000,
    b.m45000,
    b.m50000,

    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(ST_SnapToGrid(ST_curvetoline(a.geom_3857),0.0001))
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom
FROM
    cbs.cdf_wijk_2018 as a
    LEFT JOIN armoede.energiearmoede as b
    ON a.wijkcode = b.regio_code ;

ALTER TABLE
    tiles.admin_areas_2018_10_13_uo
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX admin_areas_2018_10_13_uo_geom ON tiles.admin_areas_2018_10_13_uo USING gist(geom);

CREATE INDEX admin_areas_2018_10_13_uo_geohash ON tiles.admin_areas_2018_10_13_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

DROP TABLE IF EXISTS  tiles.admin_areas_2018_10_13 CASCADE;
CREATE TABLE tiles.admin_areas_2018_10_13 AS
  SELECT * FROM tiles.admin_areas_2018_10_13_uo
    ORDER BY ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10) COLLATE "C";

CREATE INDEX tiles_admin_areas_2018_10_13_geom
    ON tiles.admin_areas_2018_10_13 USING gist (geom);

CREATE INDEX cdf_admin_areas_2018_10_13_geohash ON tiles.admin_areas_2018_10_13 (ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10));

CLUSTER tiles.admin_areas_2018_10_13 USING cdf_admin_areas_2018_10_13_geohash;

DROP TABLE IF EXISTS tiles.admin_areas_2018_10_13_uo CASCADE;
