DROP TABLE IF EXISTS tiles.admin_areas_2023_6_9_uo CASCADE;

CREATE TABLE tiles.admin_areas_2023_6_9_uo AS
SELECT
    a.gemeentenaam as name,
    a.gemeentecode as code,
    1 as hierarchy,

    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.mannen,
    a.vrouwen,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_ongehuwd,
    a.percentage_gehuwd,
    a.percentage_gescheid,
    a.percentage_verweduwd,
    a.aantal_huishoudens,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_met_herkomstland_nederland,
    a.percentage_met_herkomstland_uit_europa_excl_nl,
    a.percentage_met_herkomstland_buiten_europa,
    a.percentage_geb_in_nl_met_herkomstland_nederland,
    a.perc_geb_in_nl_met_herkomstland_in_europa_ex_nl,
    a.perc_geb_in_nl_met_herkomstland_buiten_europa,
    a.perc_geb_buiten_nl_met_herkomstlnd_in_europa_ex_nl,
    a.perc_geb_buiten_nl_met_herkomstlnd_buiten_europa,
    a.oppervlakte_totaal_in_ha,
    a.oppervlakte_land_in_ha,
    a.oppervlakte_water_in_ha,
    a.jrstatcode,
    a.jaar,

    ST_Multi(
        ST_CollectionExtract(
            ST_MakeValid(
                ST_RemoveRepeatedPoints(
                    ST_SnapToGrid(ST_curvetoline(a.geom_3857), 0.0001)
                )
            ),
            3
        )
    ) :: geometry(MULTIPOLYGON, 3857) as geom
FROM
    cbs2023.cdf_gem as a;

/* add primary key */
ALTER TABLE
    tiles.admin_areas_2023_6_9_uo
ADD
    COLUMN fid SERIAL PRIMARY KEY;

/* add some indexes */
CREATE INDEX admin_areas_2023_6_9_uo_geom ON tiles.admin_areas_2023_6_9_uo USING gist(geom);

CREATE INDEX admin_areas_2023_6_9_uo_geohash ON tiles.admin_areas_2023_6_9_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

DROP TABLE IF EXISTS tiles.admin_areas_2023_6_9 CASCADE;

/* final table */
CREATE TABLE tiles.admin_areas_2023_6_9 AS
SELECT
    *
FROM
    tiles.admin_areas_2023_6_9_uo
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10) COLLATE "C";

/* create some indexes */

CREATE INDEX cdf_admin_areas_2023_6_9_geom ON tiles.admin_areas_2023_6_9 USING gist (geom);

CREATE INDEX cdf_admin_areas_2023_6_9_geohash ON tiles.admin_areas_2023_6_9 (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

CLUSTER tiles.admin_areas_2023_6_9 USING cdf_admin_areas_2023_6_9_geohash;

DROP TABLE IF EXISTS tiles.admin_areas_2023_6_9_uo CASCADE;
