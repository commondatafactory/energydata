DROP TABLE IF EXISTS tiles.admin_labels_2023_10_13_uo CASCADE;

CREATE TABLE tiles.admin_labels_2023_10_13_uo (
    fid SERIAL PRIMARY KEY,
    name TEXT,
    code TEXT,
    hierarchy INT,
    geom geometry(POINT, 3857)
);

CREATE INDEX admin_labels_2023_10_13_uo_geom ON tiles.admin_labels_2023_10_13_uo USING gist (geom);

CREATE INDEX admin_labels_2023_10_13_uo_geohash ON tiles.admin_labels_2023_10_13_uo (
    ST_GeoHash(ST_Transform(ST_Envelope(geom), 4326), 10)
);

INSERT INTO
    tiles.admin_labels_2023_10_13_uo (name, code, hierarchy, geom)
SELECT
    wijknaam as name ,
    wijkcode as code,
    2 AS hierarchy,
    ST_PointOnSurface(geom_3857)::geometry(POINT, 3857)
FROM
    cbs2023.cdf_wijk;

DROP TABLE IF EXISTS  tiles.admin_labels_2023_10_13 CASCADE;
CREATE TABLE tiles.admin_labels_2023_10_13 AS
  SELECT * FROM tiles.admin_labels_2023_10_13_uo
    ORDER BY ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10) COLLATE "C";

CREATE INDEX cdf_admin_labels_2023_10_13_geom
    ON tiles.admin_labels_2023_10_13 USING gist (geom);

CREATE INDEX cdf_admin_labels_2023_10_13_geohash ON tiles.admin_labels_2023_10_13 (ST_GeoHash(ST_Transform(ST_Envelope(geom),4326),10));

CLUSTER tiles.admin_labels_2023_10_13 USING cdf_admin_labels_2023_10_13_geohash;

DROP TABLE IF EXISTS tiles.admin_labels_2023_10_13_uo CASCADE;

VACUUM ANALYZE tiles.admin_labels_2023_10_13;
