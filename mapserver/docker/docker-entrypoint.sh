#!/usr/bin/env bash

set -e
set -u

echo Creating configuration files

CBS_DB_HOST=${CBS_DB_HOST:-databse}
CBS_DB_PORT=${CBS_DB_PORT:-5432}
CBS_DB_NAME=${CBS_DB_NAME:-cdf}
CBS_DB_USER=${CBS_DB_USER:-${CBS_DB_NAME}}
CBS_DB_PASSWORD=${CBS_DB_PASSWORD:-insecure}

cat > /srv/mapserver/connection_cdf.inc <<EOF
CONNECTIONTYPE postgis
CONNECTION "host=${CBS_DB_HOST} dbname=${CBS_DB_NAME} user=${CBS_DB_USER} password=${CBS_DB_PASSWORD} port=${CBS_DB_PORT}"
PROCESSING "CLOSE_CONNECTION=DEFER"
EOF

ENERGY_DB_HOST=${ENERGY_DB_HOST:-database}
ENERGY_DB_PORT=${ENERGY_DB_PORT:-5432}
ENERGY_DB_NAME=${ENERGY_DB_NAME:-cdf}
ENERGY_DB_USER=${ENERGY_DB_USER:-${ENERGY_DB_NAME}}
ENERGY_DB_PASSWORD=${ENERGY_DB_PASSWORD:-insecure}

cat > /srv/mapserver/connection_energy.inc <<EOF
CONNECTIONTYPE postgis
CONNECTION "host=${ENERGY_DB_HOST} dbname=${ENERGY_DB_NAME} user=${ENERGY_DB_USER} password=${ENERGY_DB_PASSWORD} port=${ENERGY_DB_PORT}"
PROCESSING "CLOSE_CONNECTION=DEFER"
EOF


# Configure apache to redirect errors to stderr.
# The mapserver will redirect errors to apache errorstream (see header.inc and private/header.inc)
# and apache will then redirect this to stderr, which will then be redirected to syslog/kibana.
# ref: http://mapserver.org/optimization/debugging.html#steps-to-enable-mapserver-debugging
#      https://serverfault.com/questions/711168/writing-apache2-logs-to-stdout-stderr
sed -i 's/ErrorLog .*/ErrorLog \/dev\/stderr/' /etc/apache2/apache2.conf

# Replace actual location of the mapserver depending on the environment
# sed -i 's#MAP_URL_REPLACE#'"$MAP_URL"'#g' /srv/mapserver/topografie.map /srv/mapserver/topografie_wm.map
# sed -i 's#LEGEND_URL_REPLACE#'"$LEGEND_URL"'#g' /srv/mapserver/topografie.map /srv/mapserver/topografie_wm.map

mkdir -p /srv/mapserver/sld
python3 /srv/mapserver/tools/make_mapfile_config.py > /srv/mapserver/sld/config.json

echo Starting server
apachectl -D FOREGROUND
