#==============================================================================
#
# header.map
# doel: algemene header-informatie dat voor alle map-bestanden gelijk is.
#
#==============================================================================


  #=============================================================================
  # MAP: debug
  #=============================================================================


  # Redirect errors to apache errorstream
  # ENABLE for debugging purposes - quite spammy
  CONFIG   "MS_ERRORFILE" stderr
  # CONFIG   "MS_ERRORFILE" "/srv/mapserver/ms_error.txt"

  DEBUG    5
  # DEBUG VALUES 1, 2, 3, 4 5
  # For errors from underlying GDAL (shows SQL query issues) -- is spammy
  CONFIG "CPL_DEBUG" "ON"
  CONFIG "PROJ_DEBUG" "ON"

  #=============================================================================
  # MAP: algemeen uitvoer
  #=============================================================================

  EXTENT     368732.2244 6567469.4703 812678.4847 7110478.1192

  STATUS      ON

  PROJECTION
    "init=epsg:3857"
  END

  WEB
    METADATA
      "ows_srs"                      "EPSG:28992 EPSG:4326 EPSG:3857 EPSG:4258"
      "ows_feature_info_mime_type"   "application/json"
      "ows_encoding"                 "UTF-8"
      "wfs_getfeature_formatlist"    "gml,geojson,csv,shapezip"
      "ows_return_srs_as_urn"        "FALSE"

      # Metadata verplicht om aan de OGC standaard te voldoen:
      "ows_contactorganization"      "VNG Realisatie"
      "ows_contactperson"            "Common Data Factory"
      "ows_contactorganization"      "VNG Realisatie"
      "ows_contactposition"          "Tech lead"
      "ows_role"                     "Tech lead"
      "ows_contactelectronicmailaddress" "stephan.preeker@vng.nl"
      "ows_hoursofservice"           "8:00-17:00"
      "ows_contactinstructions"      "Send an email for requests, feedback, help, or issues with the data or service. He will get in touch as soon as possible."
      "ows_address"                  "Nassaulaan 12"
      "ows_postcode"                 "2514 JS"
      "ows_city"                     "The Hague"
      "ows_stateorprovince"          "Zuid-Holland"
      "ows_country"                  "The Netherlands"
      "ows_service_onlineresource"   "https://commondatafactory.nl"
    END
    IMAGEPATH                        "/srv/mapserver/tmp_img/"
    IMAGEURL                         "/ms_tmp/"
    BROWSEFORMAT                     "image/svg+xml"
  END

  #-------------------------------------------------------------------
  # UITVOERFORMATEN
  #-------------------------------------------------------------------

  OUTPUTFORMAT
    NAME           "shapezip"
    DRIVER         "OGR/ESRI Shapefile"
    FORMATOPTION   "STORAGE=memory"
    FORMATOPTION   "FORM=zip"
    FORMATOPTION   "FILENAME=result.zip"
  END

  OUTPUTFORMAT
    NAME           "geojson"
    DRIVER         "OGR/GEOJSON"
    MIMETYPE       "application/json; subtype=geojson"
    FORMATOPTION   "LCO:COORDINATE_PRECISION=8"
    FORMATOPTION   "STORAGE=stream"
    FORMATOPTION   "FORM=simple"
  END

  OUTPUTFORMAT
    NAME           "csv"
    DRIVER         "OGR/CSV"
    MIMETYPE       "text/csv"
    FORMATOPTION   "LCO:GEOMETRY=AS_WKT"
    FORMATOPTION   "STORAGE=memory"
    FORMATOPTION   "FORM=simple"
    FORMATOPTION   "FILENAME=result.csv"
  END

  OUTPUTFORMAT
    NAME           "gml"
    DRIVER         "OGR/GML"
    FORMATOPTION   "STORAGE=filesystem"
    FORMATOPTION   "FORM=multipart"
    FORMATOPTION   "FILENAME=result.gml"
  END

  OUTPUTFORMAT
    NAME           "spatialzip"
    DRIVER         "OGR/SQLITE"
    MIMETYPE       "application/zip"
    FORMATOPTION   "DSCO:SPATIALITE=YES"
    FORMATOPTION   "STORAGE=memory"
    FORMATOPTION   "FORM=zip"
    FORMATOPTION   "FILENAME=result.db.zip"
  END
