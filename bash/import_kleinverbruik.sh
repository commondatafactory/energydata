#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

python imports/import_kleinverbruik.py 2021 &
python imports/import_kleinverbruik.py 2020 &
python imports/import_kleinverbruik.py 2019 &
python imports/import_kleinverbruik.py 2018 &
python imports/import_kleinverbruik.py 2017 &
python imports/import_kleinverbruik.py 2016 &
python imports/import_kleinverbruik.py 2015 &
python imports/import_kleinverbruik.py 2014
