#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

# ~ takes about 4 minutes each since we update 6.000.000
# rows.

python imports/update_pand3d_energy.py 2014,2015,2016,2017,2018,2019,2020,2021
# python imports/update_pand3d_energy.py 2014
# python imports/update_pand3d_energy.py 2015
# python imports/update_pand3d_energy.py 2016
# python imports/update_pand3d_energy.py 2017
# python imports/update_pand3d_energy.py 2018
# python imports/update_pand3d_energy.py 2019
# python imports/update_pand3d_energy.py 2020
