#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

cp -r tiles/$1/* tiles/$1_e/

cd tiles/$1_e

# extract tiles
find . ! -name "*.pbf" ! -name "*.json" -follow -type f -print0 | xargs -P 10 -0 -I % bash -c 'zcat % > %.pbf'

# remove old cruft
find . ! -name "*.pbf" ! -name "*.json" -follow -type f -print0 | xargs -P 10 -0 -I % bash -c 'rm %'
