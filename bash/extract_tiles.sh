#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

cp -r tiles/"$1"/* tiles/"$1"_e/
cd tiles/"$1"_e

# extract tiles and remove source file
find . ! -name "*.pbf" ! -name "*.json" -follow -type f -print0 | xargs -P 30 -0 -I % bash -c 'zcat % > %.pbf; rm %'
# find . ! -name "*.pbf" ! -name "*.json" -follow -type f -print0 | xargs -P 20 -0 -I % bash -c 'rm %'
