#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing


ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" -nln provinciegrenzen 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wfs?service=WFS&request=GetFeature&TYPENAME=bestuurlijkegrenzen:provincies&VERSION=2.0.0&count=20000' -skipfailures
# importeer buurt/wijk
# python load_wfs_postgres.py https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs cbs_wijken_2018 28992
