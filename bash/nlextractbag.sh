#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

POSTGRES_USER=cdf

cd /rawdata

# download
#filename=bag-laatst.backup
#wget -N https://data.nlextract.nl/bag/postgis/$filename

echo "Restoring dump in database"
pg_restore -c --if-exists -j 4 -h 127.0.0.1 -d cdf --no-owner --role=cdf -U $POSTGRES_USER /rawdata/$filename
