#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=${PGDATABASE:-cdf}
export PGHOST=${PGHOST:-127.0.0.1}
export PGPORT=${PGPORT:-5432}
export PGUSER=${PGUSER:-cdf}
export PGPASSWORD=${PGPASSWORD:-insecure}
export VERSION=v1



psql -f "imports/cbs_2024/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get CBS source data if not already downloaded.
wget -nc -O rawdata/cbs_2024_${VERSION}.zip https://geodata.cbs.nl/files/Wijkenbuurtkaart/WijkBuurtkaart_2024_$VERSION}.zip || true

# unzip if not already unzipped
unzip -n rawdata/cbs_2024_${VERSION}.zip -d rawdata/wijkbuurtkaart_2024_$VERSION

#Get spatial import datasets
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} port=${PGPORT:-5432} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" rawdata/wijkbuurtkaart_2024_${VERSION}/wijkenbuurten_2024_v1.gpkg -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs2024 -lco OVERWRITE=YES

# Get province data
# docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://geodata.nationaalgeoregister.nl/cbsprovincies/wfs?request=getFeature&service=wfs&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=cbsprovincies:cbsprovincies2012&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -nln provincies2012
# ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://service.pdok.nl/kadaster/bestuurlijkegebieden/wfs/v1_0?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=bestuurlijkegebieden:Provinciegebied&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -lco OVERWRITE=YES -nln  provincies2012

# Creating and merging cbs data
psql -f "./imports/cbs_2024/cbs.sql" -v "ON_ERROR_STOP=1"
