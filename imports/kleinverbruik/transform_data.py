"""
Dataframe columns should be transformed to postgres compatible
types.
"""

import datetime
import logging

import numpy as np
from sqlalchemy import String, Numeric, Float, DateTime

log = logging.getLogger(__name__)


def change_dtypes(dtype):
    '''Change the pandas type to a sqlalchemy type
    '''
    if dtype in [object, str]:
        return String
    if dtype in [np.int64, int]:
        return Numeric
    if dtype in [np.float64, float]:
        return Float
    if dtype == datetime.datetime:
        return DateTime

    log.debug("unable to transform columntyp %s", dtype)
    return String


def get_columntypes(inputdict):
    '''Create dictionary with columnname and sql type.'''
    resultdict = {}
    for column, dtype in inputdict.items():
        resultdict[column] = change_dtypes(dtype)
    return resultdict
