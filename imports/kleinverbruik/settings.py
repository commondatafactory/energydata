"""
General settings.

logging.
"""

import logging

logging.basicConfig(
    # filename='application.log',
    level=logging.DEBUG,
    format='[%(asctime)s] %(levelname)-8s-{%(pathname)s:%(lineno)d} - %(message)s',  # noqa
    datefmt='%Y-%m-%d %H:%M:%S',
)

pand3dtable = '"3dbag".energiepanden'

basispand = "workdata.pand"
bagsource = "bagv2_032022.pandbestaandactueel"

# to os path stuff..
rawdata = 'rawdata'
