"""
TODO ALTERNATIVE: use pgfutter and do merging of data in SQL.

Imports for about 490.000 postal codes yearly energy usage.

Import KV / Kleinverbruik / Small usage energy consumption
information from all mayor energy net operators.

The CSV's are manually downloaded and standardized in
a directory for each year.

Here we extract, load, transform the csv's and store all records in
one big database table for each year.

We make sure all column names are the same and there are no
null values.

known dutch energy network providers.

Stedin,
Enexis,
Westland,
Enduris,
Coteq,
Liander,
Rendo

source: https://partnersinenergie.nl

"""

import argparse
import os
import logging
from sqlalchemy import text
import pandas as pd

import setup_db
import transform_data


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('importkv')


SCHEMA = 'kv'

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
parent_DIR = os.path.normpath(os.path.join(THIS_DIR, os.pardir))
BASE_DIR = os.path.normpath(os.path.join(parent_DIR, os.pardir))

DATA_DIR = os.path.join(BASE_DIR, 'rawdata', 'kleinverbruik')

config = 'docker'

netbeheerders = [
    'stedin',
    'coteq',
    'rendo',
    'westland',
    'liander',
    'enexis'
]


def change_columnnames(columns):
    '''
    Refactor column names
      - prevent sql errors.
      - make sure column names are the same
    '''
    new_columns = []
    for column in columns:
        new_columns.append(
            column.strip()
            .lower()
            .replace(' ', '_')
            .replace('%', '')
            .replace('(', '_')
            .replace(')', '_')
            .replace('_perc', '')
            .replace('_aantal', '')
            .replace('aantal_', '')
        )
    return new_columns


def read_csv(year, path):
    '''read csv and check for delimeter.'''
    encoding = 'unicode_escape'

    if int(year) > 2021:
        if 'liander' in path.lower():
            encoding = 'utf8'
        if 'zeeland' in path.lower():
            encoding = 'utf8'

    # default
    df = pd.read_csv(
        path, sep='\t', encoding=encoding, decimal=',', thousands='.')

    if int(year) >= 2023:
        if 'liander' in path.lower():
            df = pd.read_csv(
                path, sep='\t', encoding=encoding, decimal='.')

    # if parsing of columns went wrong.
    if len(df.columns) == 1:
        df = pd.read_csv(path, sep=';', encoding=encoding, decimal=',')

    if len(df.columns) == 1:
        df = pd.read_csv(path, sep=',', encoding=encoding, decimal=',')

    log.debug(path)
    log.debug(df.shape)

    if len(df.columns) < 8:
        raise Exception('invalid csv')

    return df


drop_exceptions = [
    'SOORT_AANSLUITING_PERC',
]

drop_columns = [
    'meetverantwoordelijke',    # not always present
    'landcode',                 # always NL not always present
    # 'netgebied',                # not needed and present everywhere
    'soort_aansluiting_naam',   # not present everywhere
    'soort_aansluiting',        #
    'defintieve_aansl__nrm_',   # not presnet everywhere
    'gemiddeld_telwielen',      # not present everywhere
    'sjv_laag_tarief',          # not present everywhere
    'sja_laag_tarief',          # not present everywhere
    'slimme_meter',             # not present everywhere
    'standaarddeviatie',
    'aantal_productie',
    'productie',
    # 'sjv_gemiddeld_productie'
]


def sanitize(df):
    """Cleanup invalid rows.
    """
    df.drop(df[df.postcode_van.isnull()].index, inplace=True)


def whitespace_remover(dataframe):
    """
    remove extra leading
    and tailing whitespace from the data.
    pass dataframe as a parameter here
    """

    # iterating over the columns
    for i in dataframe.columns:
        # checking datatype of each columns
        if dataframe[i].dtype == 'object':
            # applying strip function on column
            dataframe[i] = dataframe[i].map(str.strip)
        else:
            # if condn. is False then it will do nothing.
            pass


def normalize_columns(df, filename):
    """Make sure we get the same columns over all csv files.
    """

    log.debug(df.columns)

    # remove columns before normalization
    for should_remove in drop_exceptions:
        if should_remove in df.columns:
            df.drop(columns=should_remove, inplace=True)

    # normalize columns
    df.columns = change_columnnames(df.columns)

    # make sure values are sane.
    sanitize(df)

    # drop normalized columns
    for should_remove in drop_columns:
        if should_remove in df.columns:
            df.drop(columns=should_remove, inplace=True)

    for column in df.columns:
        if column.startswith('unnamed'):
            df.drop(columns=column, inplace=True)

    # standaard jaarverbruik
    if 'sjv_gemiddeld' in df.columns:
        df.rename(columns={'sjv_gemiddeld': 'sjv'}, inplace=True)

    # standaard jaarverbruik
    if 'sja_gemiddeld' in df.columns:
        df.rename(columns={'sja_gemiddeld': 'sjv'}, inplace=True)

    if 'sjv_gemiddeld_productie' not in df.columns:
        df['sjv_gemiddeld_productie'] = 0

    # add standardized netbeheerder name
    nb_found = ""

    for nb in netbeheerders:
        if nb in filename.lower():
            nb_found = nb
            break

    if not nb_found:
        log.fatal("netbeheerder not matching known values %s", netbeheerders)

    df['netbeheerder'] = nb_found

    log.error(df['sjv'].dtypes)
    log.debug('Mean sjv %d', df.sjv.mean())


def read_files(year, datadir):
    '''Read all csv-files from specified location'''
    result = pd.DataFrame()
    for filename in os.listdir(datadir):

        # to test a specific failing file.
        # if not filename.startswith('Alian'):
        #     log.debug("skipping %s", filename)
        #     continue

        log.debug("working on %s", filename)

        if filename.endswith('.csv'):
            file_path = os.path.join(datadir, filename)
            new_df = read_csv(year, file_path)
            normalize_columns(new_df, filename)

            # strip some white space.
            for stringcolumn in [
                    'postcode_van', 'postcode_tot', 'straatnaam',
                    'woonplaats',
                    'verbruikssegment', 'productsoort']:
                # try:
                # new_df[stringcolumn] = new_df[stringcolumn].str.strip().replace(' ', '')
                new_df[stringcolumn] = new_df[stringcolumn].str.replace(r'\s+', '', regex=True)
                # except AttributeError:
                #     log.error('FAILED column %s string tripping stringcolumn', stringcolumn)
                #     log.error(new_df[stringcolumn])
                #     raise

            # drop duplicate rows. (liander has those).
            new_df = new_df.drop_duplicates()
            # print(new_df.head())
            # log.debug("find 1121RM")
            # x = new_df[new_df['postcode_van'].str.startswith("1121RM", na=False)]
            # print(x.head())

            if len(result) == 0:
                result = new_df
                log.debug('RESULT: %s %s', result.shape, len(result.columns))
                continue

            log.debug('DIFF %s (should be empty)', set(new_df.columns) - set(result.columns))

            result = pd.concat(
                [result, new_df], ignore_index=True, sort=False)

            log.debug('RESULT: %s %s', result.shape, len(result.columns))
            assert len(result.columns) == 13

    return result


def load_csv_into_database(year):
    """ETL

    extract csv into dataframe, tranform en load into database
    """
    engine = setup_db.get_engine()

    path = os.path.join(DATA_DIR, year)
    filedf = read_files(year, path)
    log.debug(filedf.shape)
    dtypedict = transform_data.get_columntypes(filedf.dtypes)
    kv_table = f"kleinverbruik_{year}"

    log.debug('loading kleinverbruik into database %s.%s', SCHEMA, kv_table)

    filedf.to_sql(
        kv_table,
        engine,
        if_exists='replace',
        schema=SCHEMA,
        index=False,
        chunksize=5000,
        dtype=dtypedict,
    )


def conver_table_columns(year):
    """Table columns need indexes / data type fixing to int
    """

    idx_name = f"kleinverbruik_{year}"
    kv_table = f"{SCHEMA}.kleinverbruik_{year}"

    engine = setup_db.get_engine()
    log.debug('converting table columns')

    drop_id = f"ALTER TABLE {kv_table} DROP IF EXISTS id"
    add_id_sql = f"ALTER TABLE {kv_table} ADD COLUMN id SERIAL PRIMARY KEY"

    drop_const = f"""
        ALTER TABLE {kv_table} DROP CONSTRAINT
        IF EXISTS unicquepostcodevan_{idx_name};
    """

    add_unique_sql = f"""
        ALTER TABLE {kv_table}
        ADD CONSTRAINT unicquepostcodevan_{idx_name}
        UNIQUE (netgebied, postcode_van, productsoort);
    """

    type_check = """
    SELECT data_type FROM information_schema.columns
    WHERE table_name = '{table}' and column_name = '{column}';
    """

    floatcolumns = [
        'aansluitingen',
        'leveringsrichting',
        'fysieke_status',
        'sjv',
        'sjv_gemiddeld_productie',
    ]

    with engine.connect() as con:
        for sqltext in [drop_id, add_id_sql, drop_const, add_unique_sql]:
            con.execute(text(sqltext))
            con.commit()

        for column in floatcolumns:
            # check if column type is numeric
            result = con.execute(text(
                type_check.format(table=idx_name, column=column)))
            con.commit()

            column_type = result.first()[0]
            log.info('TYPE %s', column_type)

            if column_type not in ['numeric', 'double precision', 'integer']:
                log.fatal("invalid column_type")


def main():
    """import kv data"""
    setup_db.test_db()
    parser = argparse.ArgumentParser()   # noqa
    parser.add_argument("year", type=int, help="import specific year")
    args = parser.parse_args()  # noqa
    year = f"{args.year}"
    log.info('working on %s', year)
    load_csv_into_database(year)
    conver_table_columns(args.year)


if __name__ == '__main__':
    main()
