"""
OUTDATED use as inspiration. now we use pure sql.

bag3d.Pand3D / pand3dtable table contains gas
end elk columns for the past years

Once all KV kleinverbruik / small-usage data is loaded. We update the
electricity standard year usage and gas standard year usage columns
with the correct data.
"""

import logging
import argparse

from imports.settings import pand3dtable
import setup_db
import models

log = logging.getLogger(__name__)


def create_pandenergy_table(session):
    """
    Create panden table wich have enery data.

    ~ 6.000.000 rows instead of 10.000.000
    """

    sql = f"""
    drop table if exists {pand3dtable}
    select p3d.*, EXTRACT(year from p3d.bouwjaar) as bouwjaar
    into {pand3dtable}
    from gas_verbruik_2020 gv
    left join  "3dbag".pand3d p3d on (gv.id = p3d.identificatie)

    create index on {pand3dtable} (identificatie);
    create index on {pand3dtable} using gist  (geovlak);
    """
    log.debug('create new %s for which energy data exists', pand3dtable)
    session.execute(sql)
    session.commit()
    log.debug('done creating %s', pand3dtable)


def ensure_energy_columns_present(year, session):
    """
    Check if columns are present
    """

    sql = f"""
    alter table {pand3dtable}
    add column if not exists gasm3_{year} int;

    alter table {pand3dtable}
    add column if not exists kwh_{year} int;

    alter table {pand3dtable}
    add column if not exists kwh_leveringsrichting_{year} int;

    alter table {pand3dtable}
    add column if not exists pandcount_{year} int;

    alter table {pand3dtable}
    add column if not exists gas_aansluitingen_{year} int;

    alter table {pand3dtable}
    add column if not exists gas_pct_{year} int;

    alter table {pand3dtable}
    add column if not exists kwh_aansluitingen_{year} int;

    alter table {pand3dtable}
    add column if not exists group_id_{year} int;
    """

    log.debug('check energy column presence')
    session.execute(sql)
    session.commit()
    log.debug('done check energy column presence')


def update_enery_columns(yeararg, session):
    """
    update enery column for year(s) x
    """
    years = yeararg.split(',')
    update_enery_columns_all(years, session)


def update_enery_columns_all(years, session):
    """
    Update multuple energy columns for years at once
    """
    updatefields = []
    joins = []

    for i, year in enumerate(years):
        updatesql = f"""
            gasm3_{year} = (v{i}.data->'gas'->>'m3')::numeric::int,
            kwh_{year} = (v{i}.data->'elk'->>'Kwh')::numeric::int,
            kwh_aansluitingen_{year} = (v{i}.data->'elk'->>'aansluitingen')::numeric::int,
            kwh_leveringsrichting_{year} = (v{i}.data->'elk'->>'leveringsrichting')::numeric::int,
            gas_aansluitingen_{year} = (v{i}.data->'gas'->>'aansluitingen')::numeric::int,
            gas_pct_{year} = CASE
                WHEN (v{i}.data->'gas'->>'aansluitingen')::numeric > 0
                THEN round( 1.0 / ((v{i}.data->>'pandencount')::numeric / (v{i}.data->'gas'->>'aansluitingen')::numeric) * 100)
            ELSE 0
            END,
            pandcount_{year} = (v{i}.data->>'pandencount')::numeric::int,
            group_id_{year} = (v{i}.data->>'idr')::int
        """  # noqa
        updatefields.append(updatesql)

        jointable = f"""
            left outer join verbruikpandp6_{year} v{i} on (v{i}.id = po.identificatie)
        """

        joins.append(jointable)

    updatefields = ",".join(updatefields)
    joins = "".join(joins)

    sql = f"""
    UPDATE {pand3dtable} p
    SET
        {updatefields}
    FROM {pand3dtable} po
        {joins}
    WHERE p.identificatie = po.identificatie;
    """  # noqa

    # print(sql)

    log.debug('update pand3d energy column(s) %s', years)
    session.execute(sql)
    session.commit()
    log.debug('done update pand3d energy column(s) %s', years)


def setup():
    """parse year and configure the db connection"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--new', action='store_true', help="create new panden table")
    parser.add_argument("year", help="update specific year")
    parser.add_argument(
        '--columns', action='store_true',
        help="create columns on pand3d table"
    )

    args = parser.parse_args()
    engine = setup_db.get_engine()
    session = models.set_session(engine)
    return args, session


if __name__ == '__main__':
    ARGS, SESSION = setup()
    if ARGS.new:
        create_pandenergy_table(SESSION)
    elif ARGS.columns:
        years = ARGS.year.split(',')
        for y in years:
            ensure_energy_columns_present(y, SESSION)
    else:
        update_enery_columns(ARGS.year, SESSION)
