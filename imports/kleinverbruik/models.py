"""
SQLAlchemy database models used.

Whenever a new year is released add the new table names here.
"""

import logging
import argparse
import asyncio

from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy import BigInteger, Float
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Sequence
from sqlalchemy import create_engine, Enum

# from aiopg.sa import create_engine as aiopg_engine
# from sqlalchemy.engine.url import URL

from sqlalchemy_utils.functions import database_exists
from sqlalchemy_utils.functions import create_database
from sqlalchemy_utils.functions import drop_database

import setup_db


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)

Base = declarative_base()

Session = sessionmaker()


session = []


def create_db(section="test"):
    """Create the database
    """
    CONF = setup_db.make_conf(section)
    LOG.info("created database")
    if not database_exists(CONF):
        create_database(CONF)


def drop_db(section="test"):
    """Cleanup
    """
    LOG.info("drop database")
    CONF = setup_db.make_conf(section)
    if database_exists(CONF):
        drop_database(CONF)


def make_engine(section="dev"):
    """create engine from configuration"""
    CONF = setup_db.make_conf(section)
    engine = create_engine(CONF)
    return engine


def set_session(engine):
    """set globale session"""
    global session
    Session.configure(bind=engine)
    # create a configured "session" object for tests
    session = Session()
    return session


class VerbruikPerPandenP6():
    """
    183.876 panden / 18.412 p6 / ~25 in amsterdam
    ~verbruik per 25 huishoudens
    per pand verzameling verbruiksgegevens samen gebracht.

    - meerder p6 verbruiks gegevens in een groot pand.
    - per p6 betrokken panden naar een energie rapport

    table with pand_id -> rapport_id
    """

    __tablename__ = "verbruikpandp6"
    __table_args__ = {"schema": "kv"}

    id = Column(Integer, Sequence('vbp6_id_seq'), primary_key=True)
    pid = Column(String, index=True)
    netbeheerder = Column(String, nullable=False)
    product = Column(Enum('gas', 'elk', name='product'), nullable=False)
    report_id = Column(String, index=True)


class VerbruikPerPandenP6_2023(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6v2_2023"


class VerbruikPerPandenP6_2022(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6v2_2022"


class VerbruikPerPandenP6_2021(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6v2_2021"


class VerbruikPerPandenP6_2020(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6v2_2020"


class VerbruikPerPandenP6_2019(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6v2_2019"


class VerbruikPerPandenP6_2018(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6v2_2018"


class VerbruikPerPandenP6_2017(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6v2_2017"


class VerbruikPerPandenP6_2016(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6v2_2016"


class VerbruikPerPandenP6_2015(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6v2_2015"


class VerbruikPerPandenP6_2014(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6v2_2014"


class VerbruikPerPandenP6_test(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6v2_test"


class EnergyRapport():
    """
    A collection of P6 records
    form a report of gas and electricity usage.

    Reports about the same building are merged together.

    We store the complete reports here. to link up to panden.
    """
    __table_args__ = {"schema": "kv"}

    id = Column(String, primary_key=True)
    product = Column(Enum('gas', 'elk', name='product'), nullable=False)
    data = Column(JSONB)


class Energyreports_2023(Base, EnergyRapport):
    __tablename__ = "energyreports_2023"

class Energyreports_2022(Base, EnergyRapport):
    __tablename__ = "energyreports_2022"

class Energyreports_2021(Base, EnergyRapport):
    __tablename__ = "energyreports_2021"

class Energyreports_2020(Base, EnergyRapport):
    __tablename__ = "energyreports_2020"

class Energyreports_2019(Base, EnergyRapport):
    __tablename__ = "energyreports_2019"

class Energyreports_2018(Base, EnergyRapport):
    __tablename__ = "energyreports_2018"


class Kleinverbruik():
    """
    Kleinverbruikers informatie

    source from opendata netbeheerders.
    """
    __tablename__ = "kleinverbruik_2019"
    __table_args__ = {"schema": "kv"}

    id = Column(BigInteger, primary_key=True)
    netbeheerder = Column(String)
    netgebied = Column(String)
    straatnaam = Column(String)
    postcode_van = Column(String, index=True)
    postcode_tot = Column(String, index=True)
    woonplaats = Column(String)
    # landcode = Column(String)
    productsoort = Column(String)
    verbruikssegment = Column(String)
    aansluitingen = Column(Integer)
    leveringsrichting = Column(Float)
    fysieke_status = Column(Integer)
    # defintieve_aansl_nrm_field = Column(Float)
    # soort_aansluiting = Column(Float)
    # soort_aansluiting = Column(String)
    sjv = Column(Float)
    sjv_gemiddeld_productie = Column(Float)
    # sjv_laag_tarief_perc = Column(Float)
    # slimme_meter_perc = Column(Float)


class Kleinverbruik2023(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2023"


class Kleinverbruik2022(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2022"


class Kleinverbruik2021(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2021"

class Kleinverbruik2020(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2020"


class Kleinverbruik2019(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2019"


class Kleinverbruik2018(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2018"


class Kleinverbruik2017(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2017"


class Kleinverbruik2016(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2016"


class Kleinverbruik2015(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2015"


class Kleinverbruik2014(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2014"


class Kleinverbruik2013(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2013"


class Kleinverbruik2012(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2012"


class Kleinverbruik2011(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2011"


class Kleinverbruiktest(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_test"


ENDPOINT_MODEL = {
    "verbruikpandp6_2023v2": VerbruikPerPandenP6_2023,
    "verbruikpandp6_2022v2": VerbruikPerPandenP6_2022,
    "verbruikpandp6_2021v2": VerbruikPerPandenP6_2021,
    "verbruikpandp6_2020v2": VerbruikPerPandenP6_2020,
    "verbruikpandp6_2019v2": VerbruikPerPandenP6_2019,
    "verbruikpandp6_2018v2": VerbruikPerPandenP6_2018,
    "verbruikpandp6_2017v2": VerbruikPerPandenP6_2017,
    "verbruikpandp6_2016v2": VerbruikPerPandenP6_2016,
    "verbruikpandp6_2015v2": VerbruikPerPandenP6_2015,
    "verbruikpandp6_2014v2": VerbruikPerPandenP6_2014,
    "verbruikpandp6_testv2": VerbruikPerPandenP6_test,

    "energyreports_2023": Energyreports_2023,
    "energyreports_2022": Energyreports_2022,
    "energyreports_2021": Energyreports_2021,
    "energyreports_2020": Energyreports_2020,
    "energyreports_2019": Energyreports_2019,
    "energyreports_2018": Energyreports_2018,


    "kleinverbruik_2023": Kleinverbruik2023,
    "kleinverbruik_2022": Kleinverbruik2022,
    "kleinverbruik_2021": Kleinverbruik2021,
    "kleinverbruik_2020": Kleinverbruik2020,
    "kleinverbruik_2019": Kleinverbruik2019,
    "kleinverbruik_2018": Kleinverbruik2018,
    "kleinverbruik_2017": Kleinverbruik2017,
    "kleinverbruik_2016": Kleinverbruik2016,
    "kleinverbruik_2015": Kleinverbruik2015,
    "kleinverbruik_2014": Kleinverbruik2014,
    "kleinverbruik_2013": Kleinverbruik2013,
    "kleinverbruik_2012": Kleinverbruik2012,
    "kleinverbruik_2011": Kleinverbruik2011,
    "kleinverbruik_test": Kleinverbruiktest,
}


async def main(args):
    """Main."""
    engine = setup_db.get_engine()

    if args.drop:
        # resets everything
        LOG.warning("DROPPING ALL DEFINED TABLES")
        Base.metadata.drop_all(engine)

    LOG.warning("CREATING DEFINED TABLES")

    # recreate tables
    Base.metadata.create_all(engine)


if __name__ == "__main__":
    desc = "Create/Drop defined model tables."
    inputparser = argparse.ArgumentParser(desc)

    inputparser.add_argument(
        "--drop", action="store_true", default=False, help="Drop existing"
    )

    ARGS = inputparser.parse_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(ARGS))
