"""
Create database engine connection
"""

import logging
import os
import configparser
import sqlalchemy
import psycopg2

from sqlalchemy import text
from sqlalchemy import event
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

log = logging.getLogger(__name__)


def test_db():
    """test database connection"""

    engine = get_engine()
    connection = engine.connect()
    try:
        connection.execute(text("Select 1;"))
    except (psycopg2.OperationalError,
            sqlalchemy.exc.OperationalError) as e:
        log.error(e)
        log.fatal("database connection failed")

    log.info("database connection ok")
    connection.close()


def make_conf(section, environment_overrides=()):
    """Create connectionstring for database

    with information specified in config.ini"""
    config_auth = configparser.ConfigParser()
    config_auth.read(os.path.join(BASE_DIR, "config.ini"))
    db = {
        'host': config_auth.get(section, "host"),
        'port': config_auth.get(section, "port"),
        'database': config_auth.get(section, "dbname"),
        'username': config_auth.get(section, "user"),
        'password': config_auth.get(section, "password"),
    }

    # override defaults with environment settings
    for var, env in environment_overrides:
        if os.getenv(env):
            db[var] = os.getenv(env)

    CONF = URL.create(
        "postgresql",
        username=db['username'],
        password=db['password'],
        host=db['host'],
        port=db['port'],
        database=db['database'],
    )
    print(CONF)
    print(db['password'])

    # host, port, name = db['host'], db['port'], db['database']
    return CONF


def get_engine(user='dev'):
    """get current engine"""
    pg_url = make_conf(user)
    log.info(pg_url)
    engine = create_engine(pg_url, connect_args={'sslmode': 'disable'})
    log.info("testing database connection")

    # set current schema to KV. (kleinverbruik)
    @event.listens_for(engine, "connect", insert=True)
    def set_current_schema(dbapi_connection, connection_record):
        cursor = dbapi_connection.cursor()
        schema_name = 'kv'
        cursor.execute("SET SESSION search_path='%s'" % schema_name)
        cursor.close()

    return engine
