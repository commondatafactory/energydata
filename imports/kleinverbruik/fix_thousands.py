"""
Fix shitty enexis thousands formatting in csv.

"""
import os
import logging
import argparse

import pandas as pd


THIS_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.normpath(os.path.join(THIS_DIR, os.pardir))
DATA_DIR = os.path.join(BASE_DIR, 'rawdata', 'kleinverbruik')

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


def read_csv(path):
    '''read shitty csv'''
    df = pd.read_csv(path, sep='\t', encoding='unicode_escape', decimal=',')
    return df


def fixnumber(text):
    """
    1.2 -> 1200
    998 -> 998
    12.345 -> 12345
    12.34  -> 12340
    4.3    -> 4300
    """
    if '.' not in text:
        return text

    t, d = text.split('.')
    d = d.ljust(3, '0')
    return f'{t}{d}'


def fix_csv_thousands(df):
    """convert SJV_GEMIDDELD to proper formatting
    """
    df, path = load_csv(args.year)
    df['SJV_GEMIDDELD'] = df['SJV_GEMIDDELD'].apply(fixnumber)
    df.to_csv(path + '.fixed', sep='\t', decimal=",")


def load_csv(year):
    """load a target csv"""
    datadir = os.path.join(DATA_DIR, year)
    for filename in os.listdir(datadir):
        if filename.endswith('.csvx'):
            file_path = os.path.join(datadir, filename)
            df = read_csv(file_path)
            return df, file_path

    raise Exception('could not find csv')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()   # noqa
    parser.add_argument("year", help="import specific year")
    args = parser.parse_args()  # noqa
    fix_csv_thousands(args.year)
