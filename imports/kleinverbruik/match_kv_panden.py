"""
Collect Energy use data on groups of buildings usually postalcode 6 (1234AB)

TODO. ligplaatsen / standplaatsen

Note Regardig the "Energieverbruik" map layer creation process:

* Data sources:
    * Liander/Stedin/Enexis/Rendo/Westland/Coteq/Enduris
      provided dataset with energy usage
      aggregated on postalcode. P6 -> ABCD12
      (kleinverbruik=smallusers) aka KV.
      Note that sometimes the smallest aggregation
      has a few postcodes. If very few people live in a given
      postcode area they are combined to avoid spilling
      personally identifiable data.

    * Basisregistraties Adressen en Gebouwen (BAG) using nlextract

    * prepared tables 'baghelp' derived from nlxextract

    * eancodeboek scraped gas connection data from NL from eancodeboek.nl

* General point about Postcode (6), these are not areas! (Hence the
  link to panden.)

* For every row in the KV data the postcodes are retrieved along
  with the amount of energy used.

* For every Postcode 6 the related panden (~ buildings) are retrieved
  along with their geometry. Note: it is possible that a pand can
  have several postcodes in case of large appartment blocks ---
  and it is possible for a postcode 6 to have several buildings(panden).
  so there is a N to N relation ship.

* We combine KV information into building either one KV row matches
  buildings or multiple KV rows go into one building.

* Final Map now shows the aggregate energy usage per postcode 6 (or several
  postcode 6s for large buildings) colorcoded for each year

"""

import logging
import argparse

from sqlalchemy import text


import setup_db
import models

LOG = logging.getLogger(__name__)
ENGINE = setup_db.get_engine()
SESSION = models.set_session(ENGINE)

TRACKER = {
    'double-buildings': 0,   # buildings.
    'double-netbeheerder': 0
}


def make_empty_p6_rapport(product, p6data):
    """P6 template to fill with relevant data

    Each measurement is in aggregation of 10-25 connections
    to assure privacy of citizens.
    """

    gas = {               # gas related measurements
            'aansluitingen': 0,
            'm3': 0,
            'leveringsrichting': 0,
            'panden': [],
            'postcodes': set(),
            'netvlakid': "",
            'netbeheerder': "",
    }

    elk = {             # electricity related measurements
            'aansluitingen': 0,
            'Kwh': 0,
            'leveringsrichting': 0,
            'panden': [],
            'postcodes': set(),
            'netvlakid': "",
            'netbeheerder': "",
            'productie': 0,
    }

    gaselk = {
        'gas': gas,
        'elk': elk,
    }

    normal = {
        'product': product,
        'p6_from': p6data.postcode_van,
        'p6_to': p6data.postcode_tot,
     }

    normal[product] = gaselk[product]

    return normal


def retrieve_elk_panden(p6_from, p6_to):
    """
    Retrieve valid nums, panden between p6_from and p6_to.
    belonging to the same network area = netbeheerder vlak.
    """

    sql = f"""
    SELECT distinct p.pid, p.postcode, elk.netbeheerdercode as netvlakid, elk.netbeheerdercode, elk.name as organisation
    FROM baghelp.pand_postcode p
    left outer join baghelp.pand_point_elknet elk on elk.pid = p.pid
    join (
        select elk2.netbeheerdercode, elk2.name as organisation
        from baghelp.pand_postcode p2
        left outer join baghelp.pand_point_elknet elk2 on p2.pid = elk2.pid
        where p2.postcode = '{p6_from}' limit 1
    ) ncode on ( elk.netbeheerdercode = ncode.netbeheerdercode)
    WHERE p.postcode >= '{p6_from}'
    AND p.postcode <= '{p6_to}'
    AND p.pid is not null
    ORDER BY p.postcode;
    """

    with ENGINE.connect() as con:
        rs = con.execute(text(sql))

    return rs


def retrieve_gas_panden(p6_from, p6_to):
    """
    Retrieve valid nums, panden between p6_from and p6_to.
    belonging to the same GAS network area = netbeheerder vlak.
    using the eancodeboek dataset.

    - Considers the presence of gas connection!!

    Find P6 from and P6 to in the same network.
    """

    sql = f"""
    select distinct mp.pid, mp.postcode, mp.gridarea as netvlakid, mp.organisation
    from eancodeboek.metering_points_nums mp
    join (
        select mp2.pid, mp2.gridarea
        from eancodeboek.metering_points_nums mp2
        left outer join eancodeboek.metering_points_nums gnet on mp2.pid = gnet.pid and gnet.gridarea = mp2.gridarea
        where mp2.postcode = '{p6_from}' and mp2.pid is not null limit 1
    ) ncode on ( ncode.gridarea = mp.gridarea)
    where mp.postcode >= '{p6_from}'
    and mp.postcode <= '{p6_to}'
    and mp.pid is not null
    order by mp.postcode
    """

    with ENGINE.connect() as con:
        rs = con.execute(text(sql))

    return rs


# Store all panden - report relations.
PAND_RAPPORTS = {
    "gas": {},
    "elk": {},
}


def link_panden_rapport(product, p6_rapport, panden):
    """Link pand data <-> p6 rapport a
    """

    if product not in ['gas', 'elk']:
        raise ValueError('product not in gas / elk')

    for pand in panden:
        # store all involved postcodes
        p6_rapport[product]['postcodes'].update([pand.postcode])

        if not p6_rapport[product].get('netvlakid'):
            p6_rapport[product]['netvlakid'] = pand.netvlakid
            p6_rapport[product]['netbeheerder'] = pand.organisation

        pand_rapports = PAND_RAPPORTS[product]

        r = pand_rapports.get(pand.pid, [])      # find existing relations
        r.append(p6_rapport)                     # add p6_rapport
        pand_rapports[pand.pid] = r

        r_panden = p6_rapport[product]['panden']
        r_panden.append(pand.pid)                # add pid to p6_rapport

    return panden.rowcount


def update_rapport(rapport, p6data):
    """Update rapport with energy usage information
    """
    r = rapport

    if p6data.aansluitingen is None:
        LOG.debug('skipping record with missing aansluitingen')
        return

    if p6data.productsoort == 'GAS':
        r['gas']['aansluitingen'] += p6data.aansluitingen
        r['gas']['m3'] += p6data.sjv
        r['gas']['leveringsrichting'] += p6data.leveringsrichting or 100
    else:
        r['elk']['aansluitingen'] += p6data.aansluitingen
        r['elk']['Kwh'] += p6data.sjv
        r['elk']['leveringsrichting'] += p6data.leveringsrichting or 100
        r['elk']['productie'] += p6data.sjv_gemiddeld_productie or 0


RETRIEVE_PANDEN = {
    'gas': retrieve_gas_panden,
    'elk': retrieve_elk_panden,
}

def _add_panden(p6data, p6_rapports):
    """
    Lookup panden in database related to given kleinverbruik p6data.

    within p6_to - p6_from find the involed buildings / AKA BAG objects.

    - store reporst with panden in database.
    - store panden with relations to kv reports.
    """

    product = p6data.productsoort.lower()
    # remove white space if present.
    p6_from = "".join(p6data.postcode_van.split())

    # coteq gives us weird information missing postcode_tot.
    if not p6data.postcode_tot:
        logging.error('missing postcode_tot %s', p6data)
        p6_to = p6_from
    else:
        # remove white space.
        p6_to = "".join(p6data.postcode_tot.split())

    # find all panden involved in van p6 -> tot p6
    xpanden = RETRIEVE_PANDEN[product](p6_from, p6_to)

    # check the amount of buildings invoved in postalcode range.
    # some have large amount of buildings invoved indicating errors at borders.
    # of networks

    if xpanden.rowcount > 350:
        LOG.error(
            "large group in %s %s %s: %d",
            product, p6_from, p6_to, xpanden.rowcount)

    # more then x amount of buildings in a kv measurement is useless
    if xpanden.rowcount > 400:
        if p6_from != p6_to:
            LOG.error(
                "skipping large group in %s %s %s: %d",
                product, p6_from, p6_to, xpanden.rowcount)
            return 0

    if p6data.aansluitingen is None:
        LOG.error(
            "missing 'aansluitingen' in %s %s", p6_from, p6_to)
        return 0

    rapport = make_empty_p6_rapport(product, p6data)

    # add all panden information to this
    count_found_panden = link_panden_rapport(product, rapport, xpanden)
    update_rapport(rapport, p6data)

    rkey = f"{product}-{p6data.netbeheerder}-{p6_from}-{p6_to}"

    if p6_rapports.get(rkey):
        logging.fatal("%s duplicate?", rkey)

    p6_rapports[rkey] = rapport

    # usually just around ~10
    return count_found_panden


def add_panden_to_kv():
    """
    Add pand/building information to all P6 energy/kleinverbruik areas

    So find all relatons between

    KV-usage -> [pand, pand, ....]

    Solve issues where multiple energy records overlap with buildings.
    Merge or keep energy records separated.
    """

    LOG.info('add pand information to low usage / klein verbruik p6 records')

    # P6 reports stores all relations from kv data to buildings.
    p6_rapports = {}

    SESSION.query(VERBRUIK_PAND_MODEL).delete()
    SESSION.query(ENERGY_REPORT).delete()

    matched = 1

    q1 = (
        SESSION.query(KVMODEL)
        .where(KVMODEL.postcode_van == KVMODEL.postcode_tot)
        .order_by(KVMODEL.postcode_van))

    q2 = (
        SESSION.query(KVMODEL)
        .where(KVMODEL.postcode_van != KVMODEL.postcode_tot)
        .order_by(KVMODEL.postcode_van))

    limit = 100000000
    i = 0

    # first we do 'small postcode ranges.
    for p6data in q1:
        i += 1
        found_panden = _add_panden(p6data, p6_rapports)
        matched += found_panden

        if matched % 1000 == 0:
            LOG.debug(matched)

        if limit < i:
            i = 0
            break

    # later we do the larger postcode ranges.
    for p6data in q2:
        i += 1
        found_panden = _add_panden(p6data, p6_rapports)
        matched += found_panden

        if matched % 1000 == 0:
            LOG.debug(matched)

        if limit < i:
            break

    logging.debug('matched %s', matched)

    # return all p6 reports connected to panden.
    return p6_rapports


def add_rapport(master, rapport):
    """
    Add rapport data to the master aggregate rapport
    """
    m, r = master, rapport

    m['rapport_count'] += 1
    # m['pandencount'] += len(r['panden'])
    # m['postcodes'].update(r['postcodes'])

    m['gas']['aansluitingen'].append((r['gas']['aansluitingen']))
    m['gas']['m3'].append((r['gas']['m3']))
    m['gas']['leveringsrichting'].append((r['gas']['leveringsrichting']))
    m['gas']['postcodes'].update(r['gas']['postcodes'])

    m['elk']['aansluitingen'].append((r['elk']['aansluitingen']))
    m['elk']['Kwh'].append((r['elk']['Kwh']))
    m['elk']['leveringsrichting'].append((r['elk']['leveringsrichting']))
    m['elk']['productie'].append((r['elk']['productie']))
    m['elk']['postcodes'].update(r['elk']['postcodes'])


def _save_pand_record(rkey, rapport):
    """
    For each building store a relation to an Eneergy Report.

    ~10 Building in P6 area share the same report.
    """
    product = 'elk'

    if 'gas' in rapport:
        product = 'gas'

    rapport[product]['postcodes'] = list(
        rapport[product]['postcodes']
    )

    er = ENERGY_REPORT(
            id=rkey,
            product=product,
            data=rapport,
    )

    SESSION.add(er)

    for pand_id in rapport[product]['panden']:

        vp6 = VERBRUIK_PAND_MODEL(
            pid=pand_id,
            product=product,
            netbeheerder=rapport[product]['netbeheerder'],
            report_id=rkey,
        )

        SESSION.add(vp6)


def fill_panden_usage_table(p6_rapports):
    """
    For all 6p - pand relations create single energy usage information
    """

    LOG.error('clearing usage data table')
    SESSION.query(VERBRUIK_PAND_MODEL).delete()
    SESSION.commit()

    LOG.info("save usage information with pand ids")

    i = 0

    for rkey, rapport in p6_rapports.items():
        i += 1
        product = 'elk'
        if 'gas' in rapport:
            product = 'gas'

        if not rapport[product]['panden']:
            # skip rapport without panden
            LOG.debug('could not find panden for %s', rkey)
            continue

        _save_pand_record(rkey, rapport)

        # every x records store records.
        if i % 300 == 0:
            LOG.debug("%s %s", i, rapport[product]['postcodes'])
            SESSION.commit()

    # commit pending records.
    SESSION.commit()

    count = SESSION.query(VERBRUIK_PAND_MODEL).count()

    LOG.info('double skipped / merged %d buildings', TRACKER['double-buildings'])
    LOG.info('double netbeheerder %d buildings', TRACKER['double-netbeheerder'])
    LOG.info('P6 pand geo objects %d', count)


def pand_kleinverbruik_map():
    """
    - Create aggregated information from kv data wich collects
    all involved buildings / panden.

    - Create a pand-id kv data information table

    - Create gas and electricity usage tables
    """
    # create rapports by connecting kv information to buildings.
    p6_rapports = add_panden_to_kv()
    # store in DB
    fill_panden_usage_table(p6_rapports)


KVMODEL = []
VERBRUIK_PAND_MODEL = []
ENERGY_MODEL = []


def setup():
    """parse year and configure the db tables used"""
    parser = argparse.ArgumentParser()
    parser.add_argument("year", help="import specific year")
    args = parser.parse_args()
    kvmodel = models.ENDPOINT_MODEL[f'kleinverbruik_{args.year}']
    verbruik_pand_model = models.ENDPOINT_MODEL[f'verbruikpandp6_{args.year}v2']
    energy_report = models.ENDPOINT_MODEL[f'energyreports_{args.year}']
    # raport_model = models.ENDPOINT_MODEL[f'rapportp6_{args.year}']
    return args.year, kvmodel, verbruik_pand_model, energy_report


if __name__ == '__main__':
    YEAR, KVMODEL, VERBRUIK_PAND_MODEL, ENERGY_REPORT = setup()
    pand_kleinverbruik_map()
