#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=database
export PGPORT=5434
export PGUSER=cdf
export PGPASSWORD=insecure

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/import/energiearmoede/import.sql" -v "ON_ERROR_STOP=1"

# docker-compose exec database -c COPY armoede.energiearmoede FROM '/rawdata/tabel_wijk_buurt_energiearmoede.csv' WITH FORMAT csv DELIMITER ';' CSV HEADER;
