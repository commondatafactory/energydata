/*
    Energiearmoede is from micro data of 2018 with borders regions of 2020
*/

CREATE SCHEMA IF NOT EXISTS armoede;

DROP TABLE IF EXISTS armoede.energiearmoede CASCADE;

/*
manual import is done with after creating a csv from given excel email. Manual created column names in the csv for colunmns
with non present. The exel contains nice make up that does not translate nice to columns.

pgfutter --dbname cdf --username cdf --pass insecure csv tabel_wijk_buurt_energiearmoede_25012021.csv --skip-header --delimiter ";" --null-delimiter "."

result is in:

    "import".tabel_wijk_buurt_energiearmoede_25012021 twbe

*/

CREATE TABLE armoede.energiearmoede AS
SELECT
    gemeente as gemeente_naam,
    gemeente_wijk_buurt as naam,
    regio,
    CASE
        WHEN regio = 'Gemeente' THEN 'GM' || replace(trim(regiocode), '-', '')
        WHEN regio = 'Wijk' THEN 'WK' || replace(trim(regiocode), '-', '')
        WHEN regio = 'Buurt' THEN 'BU' || replace(trim(regiocode), '-', '')
    END as regio_code,
    replace(trim(woningen), ' ', '') :: integer as aantal_woningen,
    replace(trim(h50_i25_wpg), ' ', '') :: integer as h50_i25_wpg,  -- hoogste 50 gas laagste 25 inkomen woningen met gegevens.
    replace(trim(h50_i25_wpa), ' ', '') :: integer as h50_i25_wpa,  -- hoogste 50 gas laagste 25 inkomen woningen aandeel.
    replace(trim(er8p_i_wpg), ' ', '') :: integer as er8p_i_wpg,  -- energie rekening 8 % inkomen. woningen gegevens.
    replace(trim(er8p_i_wpa), ' ', '') :: integer as er8p_i_wpa,  -- energie rekening 8 % inkomen. inkomen woningen aandeel.
    replace(trim(aandeel_koopwoningen), ' ', '') :: integer as aandeel_koopwoningen,  -- aaandeel koopwoningen.
    replace(trim(aandeel_wpg), ' ', '') :: integer as financieel_wpg,  -- aandeel koopwoningen met gegevens fincieel percentage.
    replace(trim(mv5000), ' ', '') :: integer as m5000,    -- minimaal 5000
    replace(trim(mv10000), ' ', '') :: integer as m10000,  -- minimaal 10000
    replace(trim(mv15000), ' ', '') :: integer as m15000,  -- minimaal 15000
    replace(trim(mv20000), ' ', '') :: integer as m20000,  -- minimaal 20000
    replace(trim(mv25000), ' ', '') :: integer as m25000,  -- minimaal 25000
    replace(trim(mv30000), ' ', '') :: integer as m30000,  -- minimaal 30000
    replace(trim(mv35000), ' ', '') :: integer as m35000,  -- minimaal 35000
    replace(trim(mv40000), ' ', '') :: integer as m40000,  -- minimaal 40000
    replace(trim(mv45000), ' ', '') :: integer as m45000,  -- minimaal 45000
    replace(trim(mv50000), ' ', '') :: integer as m50000   -- minimaal 50000
FROM
    "import".tabel_wijk_buurt_energiearmoede_25012021 twbe
;

DROP TABLE IF EXISTS armoede.energiearmoede_wijk_2018 CASCADE;

CREATE TABLE armoede.energiearmoede_wijk_2018 AS
SELECT
    a.aantal_woningen,
    a.h50_i25_wpg,
    a.h50_i25_wpa,
    a.er8p_i_wpg,
    a.er8p_i_wpa,
    a.aandeel_koopwoningen,
    a.financieel_wpg,
    a.m5000,
    a.m10000,
    a.m15000,
    a.m20000,
    a.m25000,
    a.m30000,
    a.m35000,
    a.m40000,
    a.m45000,
    a.m50000,
    b.wijknaam,
    b.wijkcode,
    b.geom
FROM
    armoede.energiearmoede as a
    LEFT JOIN cbs.cbs_wijken_2020 as b ON a.regio_code = b.wijkcode;
