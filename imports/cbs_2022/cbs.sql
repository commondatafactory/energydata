-- Buurt
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(
                    ST_MakeValid(ST_RemoveRepeatedPoints(geom)),
                    3
                )
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        buurtcode
    FROM
        cbs2022.buurten
    WHERE
        water = 'NEE'
    GROUP BY
        buurtcode
)
SELECT
    a.*,
    -- b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs2022.cdf_buurt
FROM
    cbs2022.buurten a
    JOIN cte b ON a.buurtcode = b.buurtcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

/*
ALTER TABLE
    cbs2022.cdf_buurt_2022
ADD
    COLUMN fid SERIAL PRIMARY KEY;
*/

CREATE INDEX cbs2022cdf_buurt_2022_geom ON cbs2022.cdf_buurt USING gist (geom);

CREATE INDEX cbs2022cdf_buurt_2022_geom_3857 ON cbs2022.cdf_buurt USING gist (geom_3857);

VACUUM ANALYZE cbs2022.cdf_buurt;

-- Wijk
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        wijkcode
    FROM
        cbs2022.wijken
    WHERE
        water = 'NEE'
    GROUP BY
        wijkcode
)
SELECT
    a.*,
    -- b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs2022.cdf_wijk
FROM
    cbs2022.wijken a
    JOIN cte b ON a.wijkcode = b.wijkcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

/*
ALTER TABLE
    cbs2022.cdf_wijk
ADD
    COLUMN fid SERIAL PRIMARY KEY;
*/

CREATE INDEX cbs2022cdf_wijk_geom ON cbs2022.cdf_wijk USING gist (geom);

CREATE INDEX cbs2022cdf_wijk_geom_3857 ON cbs2022.cdf_wijk USING gist (geom_3857);

VACUUM ANALYZE cbs2022.cdf_wijk;

-- Gemeenten
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        gemeentecode
    FROM
        cbs2022.gemeenten
    WHERE
        water = 'NEE'
    GROUP BY
        gemeentecode
)
SELECT
    a.*,
    --b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs2022.cdf_gem
FROM
    cbs2022.gemeenten a
    JOIN cte b ON a.gemeentecode = b.gemeentecode
WHERE
    a.water = 'NEE'
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs2022.cdf_gem
ADD
    PRIMARY KEY (gemeentecode);

CREATE INDEX cbs2022cdf_gem_geom ON cbs2022.cdf_gem USING gist (geom);

CREATE INDEX cbs2022cdf_gem_geom_3857 ON cbs2022.cdf_gem USING gist (geom_3857);

VACUUM ANALYZE cbs2022.cdf_gem;

--ADD PROVINCIE CODE TO GEMEENTE DATA
-- -- Checks
-- SELECT count(*) FROM cbs2022.cdf_buurt_2022 WHERE NOT ST_IsValid(geom);
-- SELECT count(*) FROM cbs2022.cdf_buurt_2022 WHERE  ST_IsPolygonCCW(geom);
-- --  ST_ForcePolygonCCW
