#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=${PGDATABASE:-cdf}
export PGHOST=${PGHOST:-127.0.0.1}
export PGPORT=${PGPORT:-5433}
export PGUSER=${PGUSER:-cdf}
export PGPASSWORD=${PGPASSWORD:-insecure}
export VERSION=v2


psql -f "imports/cbs_2022/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get CBS source data if not already downloaded.
# wget -nc -O rawdata/cbs_2022.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2022_v2.zip || true
wget -nc -O rawdata/cbs_2022_${VERSION}.zip https://download.cbs.nl/regionale-kaarten/wijkbuurtkaart_2022_${VERSION}.zip || true

# unzip if not already unzipped
unzip -n rawdata/cbs_2022_${VERSION}.zip -d rawdata/

#Get spatial import datasets
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} port=${PGPORT:-5432} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" rawdata/wijkenbuurten_2022_v2.gpkg -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs2022 -lco OVERWRITE=YES

# Get province data
# docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://geodata.nationaalgeoregister.nl/cbsprovincies/wfs?request=getFeature&service=wfs&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=cbsprovincies:cbsprovincies2012&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -nln provincies2012
# ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://service.pdok.nl/kadaster/bestuurlijkegebieden/wfs/v1_0?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=bestuurlijkegebieden:Provinciegebied&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -lco OVERWRITE=YES -nln  provincies2012

# Creating and merging cbs data
psql -f "./imports/cbs_2022/cbs.sql" -v "ON_ERROR_STOP=1"
