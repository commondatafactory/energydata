#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure


#psql -h ${PGHOST} -d ${PGDATABASE} -p ${PGPORT} -U ${PGUSER} -f "imports/cbs_2021/drop_tables.sql" -v "ON_ERROR_STOP=1"

# GAS.
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://opendata:opendata@opendata.polygonentool.nl/wfs?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG:4326:&version=2.0.0&typeName=OGC_Gasnetbeheerdervlak' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=import -lco OVERWRITE=YES -nln gasnetbeheerdersvlak

# Elektriciteit
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://opendata:opendata@opendata.polygonentool.nl/wfs?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG:4326:&version=2.0.0&typeName=OGC_Elektriciteitnetbeheerdervlak' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=import -lco OVERWRITE=YES -nln elknetbeheerdersvlak

# water
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://opendata:opendata@opendata.polygonentool.nl/wfs?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG:4326:&version=2.0.0&typeName=OGC_Waternetbeheerdervlak' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=import -lco OVERWRITE=YES -nln waternetbeheerdersvlak

# warmte netten
#ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://opendata:opendata@opendata.polygonentool.nl/wfs?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG:4326:&version=2.0.0&typeName=OGC_Warmtevlak' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=import -lco OVERWRITE=YES -nln warmtevlak
