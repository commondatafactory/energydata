#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure

psql -h ${PGHOST} -d cdf -p ${PGPORT} -U ${PGUSER} -f "./imports/cbs_2017/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get data application/json

wget -nc -O rawdata/cbs_2017.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2017_v3.zip   || true

unzip -n rawdata/cbs_2017.zip -d rawdata/

#Get spatial import datasets
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" ./rawdata/WijkBuurtkaart_2017_v3.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs -lco OVERWRITE=YES

# Creating and merging cbs data
psql -h ${PGHOST} -d cdf -p ${PGPORT} -U ${PGUSER} -f "./imports/cbs_2017/cbs.sql" -v "ON_ERROR_STOP=1"
