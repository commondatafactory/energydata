#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=database
export PGPORT=5434
export PGUSER=cdf
export PGPASSWORD=insecure

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/import/cbs_2017/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get data application/json

wget -nc -O rawdata/cbs_2017.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2017_v3.zip   || true

unzip -n rawdata/cbs_2017.zip -d rawdata/


#Get spatial import datasets

docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" /data/WijkBuurtkaart_2017_v3.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco SPATIAL_INDEX=OFF -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs

# Creating and merging cbs data
docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/import/cbs_2017/cbs.sql" -v "ON_ERROR_STOP=1"
