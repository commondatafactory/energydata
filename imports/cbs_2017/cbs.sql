-- Buurt
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(
                    ST_MakeValid(ST_RemoveRepeatedPoints(geom)),
                    3
                )
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        bu_code as buurtcode
    FROM
        cbs.buurt_2017_v3
    WHERE
        water = 'NEE'
    GROUP BY
        bu_code
)
SELECT
    -- id SERIAL as fid, ??
    a.bu_naam as buurtnaam,
    b.buurtcode,
    a.wk_code as wijkcode,
    a.gm_code as gemeentecode,
    a.gm_naam as gemeentenaam,
    -- a.indelingswijziging_wijken_en_buurten,
    -- a.omgevingsadressendichtheid,
    -- a.stedelijkheid_adressen_per_km2,
    -- a.bevolkingsdichtheid_inwoners_per_km2,
    -- a.aantal_inwoners,
    -- a.aantal_huishoudens,
    -- a.gemiddelde_huishoudsgrootte,
    -- a.percentage_personen_0_tot_15_jaar,
    -- a.percentage_personen_15_tot_25_jaar,
    -- a.percentage_personen_25_tot_45_jaar,
    -- a.percentage_personen_45_tot_65_jaar,
    -- a.percentage_personen_65_jaar_en_ouder,
    -- a.percentage_eenpersoonshuishoudens,
    -- a.percentage_huishoudens_zonder_kinderen,
    -- a.percentage_huishoudens_met_kinderen,
    -- a.woningvoorraad,
    -- a.gemiddelde_woningwaarde,
    -- a.percentage_koopwoningen,
    -- a.percentage_huurwoningen,
    -- a.perc_huurwoningen_in_bezit_woningcorporaties,
    -- a.perc_huurwoningen_in_bezit_overige_verhuurders,
    -- a.percentage_woningen_met_eigendom_onbekend,
    -- a.percentage_bouwjaarklasse_tot_2000,
    -- a.percentage_bouwjaarklasse_vanaf_2000,

    a.p_hooginkp,
    a.p_laaginkp,

    -- a.gemiddeld_gasverbruik_appartement,
    -- a.gemiddeld_gasverbruik_tussenwoning,
    -- a.gemiddeld_gasverbruik_hoekwoning,
    -- a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    -- a.gemiddeld_gasverbruik_vrijstaande_woning,
    -- a.gemiddeld_gasverbruik_huurwoning,
    -- a.gemiddeld_gasverbruikkoopwoning,
    -- a.gemiddeld_elektriciteitsverbruik_totaal,
    -- a.gemiddeld_elektriciteitsverbruik_appartement,
    -- a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    -- a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    -- a.gemiddeld_inkomen_per_inwonen,
    -- a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    -- a.gem_elektriciteitsverbruik_vrijstaande_woning,
    -- a.gemiddeld_elektriciteitsverbruik_huurwoning,
    -- a.gemiddeld_elektriciteitsverbruikkoopwoning,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_buurt_2017
FROM
    cbs.buurt_2017_v3 a
    JOIN cte b ON a.bu_code = b.buurtcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_buurt_2017
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_buurt_2017_geom ON cbs.cdf_buurt_2017 USING gist (geom);

CREATE INDEX cbs_cdf_buurt_2017_geom_3857 ON cbs.cdf_buurt_2017 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_buurt_2017;

-- Wijk
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        wk_code as wijkcode
    FROM
        cbs.wijk_2017_v3
    WHERE
        water = 'NEE'
    GROUP BY
        wijkcode
)
SELECT
    a.wk_code as wijkcode,
    a.wk_naam as wijknaam,
    a.gm_code as gemeentecode,
    a.gm_naam as gemeentenaam,

    a.p_hooginkp,
    a.p_laaginkp,

    -- a.indelingswijziging_wijken_en_buurten,
    -- a.omgevingsadressendichtheid,
    -- a.stedelijkheid_adressen_per_km2,
    -- a.bevolkingsdichtheid_inwoners_per_km2,
    -- a.aantal_inwoners,
    -- a.aantal_huishoudens,
    -- a.gemiddelde_huishoudsgrootte,
    -- a.percentage_personen_0_tot_15_jaar,
    -- a.percentage_personen_15_tot_25_jaar,
    -- a.percentage_personen_25_tot_45_jaar,
    -- a.percentage_personen_45_tot_65_jaar,
    -- a.percentage_personen_65_jaar_en_ouder,
    -- a.percentage_eenpersoonshuishoudens,
    -- a.percentage_huishoudens_zonder_kinderen,
    -- a.percentage_huishoudens_met_kinderen,
    -- a.woningvoorraad,
    -- a.gemiddelde_woningwaarde,
    -- a.percentage_koopwoningen,
    -- a.percentage_huurwoningen,
    -- a.perc_huurwoningen_in_bezit_woningcorporaties,
    -- a.perc_huurwoningen_in_bezit_overige_verhuurders,
    -- a.percentage_woningen_met_eigendom_onbekend,
    -- a.percentage_bouwjaarklasse_tot_2000,
    -- a.percentage_bouwjaarklasse_vanaf_2000,
    -- a.gemiddeld_gasverbruik_appartement,
    -- a.gemiddeld_gasverbruik_tussenwoning,
    -- a.gemiddeld_gasverbruik_hoekwoning,
    -- a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    -- a.gemiddeld_gasverbruik_vrijstaande_woning,
    -- a.gemiddeld_gasverbruik_huurwoning,
    -- a.gemiddeld_gasverbruikkoopwoning,
    -- a.gemiddeld_elektriciteitsverbruik_totaal,
    -- a.gemiddeld_elektriciteitsverbruik_appartement,
    -- a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    -- a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    -- a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    -- a.gem_elektriciteitsverbruik_vrijstaande_woning,
    -- a.gemiddeld_elektriciteitsverbruik_huurwoning,
    -- a.gemiddeld_elektriciteitsverbruikkoopwoning,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857 INTO cbs.cdf_wijk_2017
FROM
    cbs.wijk_2017_v3 a
    JOIN cte b ON a.wk_code = b.wijkcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_wijk_2017
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_wijk_2017_geom ON cbs.cdf_wijk_2017 USING gist (geom);

CREATE INDEX cbs_cdf_wijk_2017_geom_3857 ON cbs.cdf_wijk_2017 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_wijk_2017;

-- Gemeenten
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        gm_code as gemeentecode
    FROM
        cbs.gemeente_2017_v3
    WHERE
        water = 'NEE'
    GROUP BY
        gemeentecode
)
SELECT
    a.gm_code as gemeentecode,
    a.gm_naam as gemeentenaam,

    a.p_hooginkp,
    a.p_laaginkp,

    -- a.omgevingsadressendichtheid,
    -- a.stedelijkheid_adressen_per_km2,
    -- a.bevolkingsdichtheid_inwoners_per_km2,
    -- a.aantal_inwoners,
    -- a.aantal_huishoudens,
    -- a.gemiddelde_huishoudsgrootte,
    -- a.percentage_personen_0_tot_15_jaar,
    -- a.percentage_personen_15_tot_25_jaar,
    -- a.percentage_personen_25_tot_45_jaar,
    -- a.percentage_personen_45_tot_65_jaar,
    -- a.percentage_personen_65_jaar_en_ouder,
    -- a.percentage_eenpersoonshuishoudens,
    -- a.percentage_huishoudens_zonder_kinderen,
    -- a.percentage_huishoudens_met_kinderen,
    -- a.woningvoorraad,
    -- a.gemiddelde_woningwaarde,
    -- a.percentage_koopwoningen,
    -- a.percentage_huurwoningen,
    -- a.perc_huurwoningen_in_bezit_woningcorporaties,
    -- a.perc_huurwoningen_in_bezit_overige_verhuurders,
    -- a.percentage_woningen_met_eigendom_onbekend,
    -- a.percentage_bouwjaarklasse_tot_2000,
    -- a.percentage_bouwjaarklasse_vanaf_2000,
    -- a.gemiddeld_gasverbruik_appartement,
    -- a.gemiddeld_gasverbruik_tussenwoning,
    -- a.gemiddeld_gasverbruik_hoekwoning,
    -- a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    -- a.gemiddeld_gasverbruik_vrijstaande_woning,
    -- a.gemiddeld_gasverbruik_huurwoning,
    -- a.gemiddeld_gasverbruikkoopwoning,
    -- a.gemiddeld_elektriciteitsverbruik_totaal,
    -- a.gemiddeld_elektriciteitsverbruik_appartement,
    -- a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    -- a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    -- a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    -- a.gem_elektriciteitsverbruik_vrijstaande_woning,
    -- a.gemiddeld_elektriciteitsverbruik_huurwoning,
    -- a.gemiddeld_elektriciteitsverbruikkoopwoning,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857 INTO cbs.cdf_gem_2017
FROM
    cbs.gemeente_2017_v3 a
    JOIN cte b ON a.gm_code = b.gemeentecode
WHERE
    a.water = 'NEE'
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_gem_2017
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_gem_2017_geom ON cbs.cdf_gem_2017 USING gist (geom);

CREATE INDEX cbs_cdf_gem_2017_geom_3857 ON cbs.cdf_gem_2017 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_gem_2017;

--ADD PROVINCIE CODE TO GEMEENTE DATA
-- -- Checks
-- SELECT count(*) FROM cbs.cdf_buurt_2017 WHERE NOT ST_IsValid(geom);
-- SELECT count(*) FROM cbs.cdf_buurt_2017 WHERE  ST_IsPolygonCCW(geom);
-- --  ST_ForcePolygonCCW
