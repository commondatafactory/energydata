drop table if exists export.cbs_100_2018;

select
    *,
    ST_Transform(g.geom, 3857) :: geometry(Polygon, 3857) as geom_3857
into export.cbs_100_2018
from cbs.cbs_vk100_2018 g;

create index on export.cbs_100_2018 (fid);
create index on export.cbs_100_2018 using gist (geom_3857);


drop table if exists export.cbs_500_2018;

select
    *,
    ST_Transform(g.geom, 3857) :: geometry(Polygon, 3857) as geom_3857
into export.cbs_500_2018
from cbs.cbs_vk500_2018 g;

create index on export.cbs_500_2018 (fid);
create index on export.cbs_500_2018 using gist (geom_3857);
