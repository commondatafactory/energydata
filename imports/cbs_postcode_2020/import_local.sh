#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5432
export PGUSER=cdf
export PGPASSWORD=insecure


#psql -h ${PGHOST} -d ${PGDATABASE} -p ${PGPORT} -U ${PGUSER} -f "imports/cbs_2022/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get CBS source data if not already downloaded.
wget -nc -O rawdata/cbspostcode/CBS_pc6_2020.zip https://download.cbs.nl/postcode/CBS-PC6-2020-v1.zip  || true

# unzip if not already unzipped
unzip -n rawdata/cbspostcode/CBS_pc6_2020.zip -d rawdata/cbspostcode

# Get pc6 cbs data data
# docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://geodata.nationaalgeoregister.nl/cbsprovincies/wfs?request=getFeature&service=wfs&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=cbsprovincies:cbsprovincies2012&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -nln provincies2012
# ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://service.pdok.nl/kadaster/bestuurlijkegebieden/wfs/v1_0?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=bestuurlijkegebieden:Provinciegebied&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -lco OVERWRITE=YES -nln  provincies2012
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" ./rawdata/cbspostcode/CBS_pc6_2020_v1.shp -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt MULTIPOLYGON -nlt CONVERT_TO_LINEAR -nlt PROMOTE_TO_MULTI -progress -lco SCHEMA=cbspostcode -lco OVERWRITE=YES


# Creating and merging cbs data
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./imports/cbs_postcode/import.sql" -v "ON_ERROR_STOP=1"
