#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure


psql -d ${PGDATABASE:-cdf} -f "imports/cbs_2018/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get data application/json
# https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data/wijk-en-buurtkaart-2018
wget -nc -O rawdata/cbs_2018.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2018_v3.zip   || true

unzip -n rawdata/cbs_2018.zip -d rawdata/

# Get spatial import datasets
# ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST:-database} port=${PGPORT:-5432} user=${PGUSER:-cdf} dbname=${PGDATABASE:-cdf} password=${PGPASSWORD:-insecure}" ./rawdata/WijkBuurtkaart_2018_v3.gpkg  -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt POLYGON

ogr2ogr -f PostgreSQL PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}"  ./rawdata/WijkBuurtkaart_2018_v3.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -nlt MULTIPOLYGON -nlt CONVERT_TO_LINEAR -nlt PROMOTE_TO_MULTI -progress -lco SCHEMA=cbs -lco OVERWRITE=YES

# Creating and merging cbs data
psql -d ${PGDATABASE:-cdf} -f "imports/cbs_2018/cbs.sql" -v "ON_ERROR_STOP=1"
