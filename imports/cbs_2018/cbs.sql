-- Buurt
drop table if exists cbs.cdf_buurt_2018;
WITH cte AS (
    SELECT
    ST_Multi(
      ST_Union(
        ST_CollectionExtract(
            ST_MakeValid(
              ST_RemoveRepeatedPoints(
                  ST_Setsrid(
                      ST_CurveToLine(geom)
                  , 28992)
              )
            )
        , 3)
       )
    ) :: geometry(MultiPolygon, 28992) AS geom,
        buurtcode
    FROM
        cbs.cbs_buurten_2018
    WHERE
        water = 'NEE'
    GROUP BY
        buurtcode
)
SELECT
    -- id SERIAL as fid, ??
    a.buurtnaam,
    a.buurtcode,
    a.wijkcode,
    a.gemeentecode,
    a.gemeentenaam,
    a.indelingswijziging_wijken_en_buurten,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.aantal_huishoudens,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,

    a.aantal_inkomensontvangers,
    a.gemiddeld_inkomen_per_inwoner,
    a.gemiddeld_inkomen_per_inkomensontvanger,
    a.percentage_personen_met_laag_inkomen,
    a.percentage_personen_met_hoog_inkomen,
    a.percentage_huishoudens_met_laag_inkomen,
    a.percentage_huishoudens_met_hoog_inkomen,

    a.aantal_bedrijven_landbouw_bosbouw_visserij,
    a.aantal_bedrijven_nijverheid_energie,
    a.aantal_bedrijven_handel_en_horeca,
    a.aantal_bedrijven_vervoer_informatie_communicatie,
    a.aantal_bedrijven_financieel_onroerend_goed,
    a.aantal_bedrijven_zakelijke_dienstverlening,
    a.aantal_bedrijven_cultuur_recreatie_overige,
    a.aantal_bedrijfsvestigingen,

    a.percentage_huishoudens_met_lage_koopkracht,
    a.aantal_personen_met_een_ao_uitkering_totaal,
    a.aantal_personen_met_een_ww_uitkering_totaal,
    a.aantal_personen_met_een_algemene_bijstandsuitkering_totaal,
    a.aantal_personen_met_een_aow_uitkering_totaal,

    a.totaal_diefstal,
    a.vernieling_misdrijf_tegen_openbare_orde,
    a.gewelds_en_seksuele_misdrijven,

    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,

    a.woningvoorraad,
    a.gemiddelde_woningwaarde,
    a.percentage_koopwoningen,
    a.percentage_huurwoningen,
    a.perc_huurwoningen_in_bezit_woningcorporaties,
    a.perc_huurwoningen_in_bezit_overige_verhuurders,
    a.percentage_woningen_met_eigendom_onbekend,

    a.percentage_bouwjaarklasse_tot_2000,
    a.percentage_bouwjaarklasse_vanaf_2000,
    a.gemiddeld_gasverbruik_appartement,
    a.gemiddeld_gasverbruik_tussenwoning,
    a.gemiddeld_gasverbruik_hoekwoning,
    a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    a.gemiddeld_gasverbruik_vrijstaande_woning,
    a.gemiddeld_gasverbruik_huurwoning,
    a.gemiddeld_gasverbruikkoopwoning,
    a.gemiddeld_elektriciteitsverbruik_totaal,
    a.gemiddeld_elektriciteitsverbruik_appartement,
    a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    a.gem_elektriciteitsverbruik_vrijstaande_woning,
    a.gemiddeld_elektriciteitsverbruik_huurwoning,
    a.gemiddeld_elektriciteitsverbruikkoopwoning,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_buurt_2018
FROM
    cbs.cbs_buurten_2018 a
    JOIN cte b ON a.buurtcode = b.buurtcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_buurt_2018
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_buurt_2018_geom ON cbs.cdf_buurt_2018 USING gist (geom);

CREATE INDEX cbs_cdf_buurt_2018_geom_3857 ON cbs.cdf_buurt_2018 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_buurt_2018;

-- Wijk
drop table if exists cbs.cdf_wijk_2018;
WITH cte AS (
    SELECT
    ST_Multi(
      ST_Union(
        ST_CollectionExtract(
            ST_MakeValid(
              ST_RemoveRepeatedPoints(
                  ST_Setsrid(
                      ST_CurveToLine(geom)
                  , 28992)
              )
            )
        , 3)
       )
    ) :: geometry(MultiPolygon, 28992) AS geom,
        wijkcode
    FROM
        cbs.cbs_wijken_2018
    WHERE
        water = 'NEE'
    GROUP BY
        wijkcode
)
SELECT
    a.wijkcode,
    a.wijknaam,
    a.gemeentecode,
    a.gemeentenaam,
    a.indelingswijziging_wijken_en_buurten,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.aantal_huishoudens,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,

    a.aantal_inkomensontvangers,
    a.gemiddeld_inkomen_per_inwoner,
    a.gemiddeld_inkomen_per_inkomensontvanger,
    a.percentage_personen_met_laag_inkomen,
    a.percentage_personen_met_hoog_inkomen,
    a.percentage_huishoudens_met_laag_inkomen,
    a.percentage_huishoudens_met_hoog_inkomen,

    a.aantal_bedrijven_landbouw_bosbouw_visserij,
    a.aantal_bedrijven_nijverheid_energie,
    a.aantal_bedrijven_handel_en_horeca,
    a.aantal_bedrijven_vervoer_informatie_communicatie,
    a.aantal_bedrijven_financieel_onroerend_goed,
    a.aantal_bedrijven_zakelijke_dienstverlening,
    a.aantal_bedrijven_cultuur_recreatie_overige,
    a.aantal_bedrijfsvestigingen,

    a.percentage_huishoudens_met_lage_koopkracht,
    a.aantal_personen_met_een_ao_uitkering_totaal,
    a.aantal_personen_met_een_ww_uitkering_totaal,
    a.aantal_personen_met_een_algemene_bijstandsuitkering_totaal,
    a.aantal_personen_met_een_aow_uitkering_totaal,

    a.totaal_diefstal,
    a.vernieling_misdrijf_tegen_openbare_orde,
    a.gewelds_en_seksuele_misdrijven,

    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,

    a.woningvoorraad,
    a.gemiddelde_woningwaarde,
    a.percentage_koopwoningen,
    a.percentage_huurwoningen,
    a.perc_huurwoningen_in_bezit_woningcorporaties,
    a.perc_huurwoningen_in_bezit_overige_verhuurders,
    a.percentage_woningen_met_eigendom_onbekend,

    a.percentage_bouwjaarklasse_tot_2000,
    a.percentage_bouwjaarklasse_vanaf_2000,
    a.gemiddeld_gasverbruik_appartement,
    a.gemiddeld_gasverbruik_tussenwoning,
    a.gemiddeld_gasverbruik_hoekwoning,
    a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    a.gemiddeld_gasverbruik_vrijstaande_woning,
    a.gemiddeld_gasverbruik_huurwoning,
    a.gemiddeld_gasverbruikkoopwoning,
    a.gemiddeld_elektriciteitsverbruik_totaal,
    a.gemiddeld_elektriciteitsverbruik_appartement,
    a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    a.gem_elektriciteitsverbruik_vrijstaande_woning,
    a.gemiddeld_elektriciteitsverbruik_huurwoning,
    a.gemiddeld_elektriciteitsverbruikkoopwoning,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_wijk_2018
FROM
    cbs.cbs_wijken_2018 a
    JOIN cte b ON a.wijkcode = b.wijkcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";
ALTER TABLE
    cbs.cdf_wijk_2018
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_wijk_2018_geom ON cbs.cdf_wijk_2018 USING gist (geom);
CREATE INDEX cbs_cdf_wijk_2018_geom_3857 ON cbs.cdf_wijk_2018 USING gist (geom_3857);
VACUUM ANALYZE cbs.cdf_wijk_2018;


drop table if exists cbs.cdf_gem_2018;
-- Gemeenten
WITH cte AS (
    SELECT
    ST_Multi(
      ST_Union(
        ST_CollectionExtract(
            ST_MakeValid(
              ST_RemoveRepeatedPoints(
                  ST_Setsrid(
                      ST_CurveToLine(geom)
                  , 28992)
              )
            )
        , 3)
       )
    ) :: geometry(MultiPolygon, 28992) AS geom,
        gemeentecode
    FROM
        cbs.gemeenten2018
    WHERE
        water = 'NEE'
    GROUP BY
        gemeentecode
)
SELECT
    a.gemeentecode,
    a.gemeentenaam,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.aantal_huishoudens,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,

    a.aantal_inkomensontvangers,
    a.gemiddeld_inkomen_per_inwoner,
    a.gemiddeld_inkomen_per_inkomensontvanger,
    a.percentage_personen_met_laag_inkomen,
    a.percentage_personen_met_hoog_inkomen,
    a.percentage_huishoudens_met_laag_inkomen,
    a.percentage_huishoudens_met_hoog_inkomen,

    a.aantal_bedrijven_landbouw_bosbouw_visserij,
    a.aantal_bedrijven_nijverheid_energie,
    a.aantal_bedrijven_handel_en_horeca,
    a.aantal_bedrijven_vervoer_informatie_communicatie,
    a.aantal_bedrijven_financieel_onroerend_goed,
    a.aantal_bedrijven_zakelijke_dienstverlening,
    a.aantal_bedrijven_cultuur_recreatie_overige,
    a.aantal_bedrijfsvestigingen,

    a.percentage_huishoudens_met_lage_koopkracht,
    a.aantal_personen_met_een_ao_uitkering_totaal,
    a.aantal_personen_met_een_ww_uitkering_totaal,
    a.aantal_personen_met_een_algemene_bijstandsuitkering_totaal,
    a.aantal_personen_met_een_aow_uitkering_totaal,

    a.totaal_diefstal,
    a.vernieling_misdrijf_tegen_openbare_orde,
    a.gewelds_en_seksuele_misdrijven,

    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,

    a.woningvoorraad,
    a.gemiddelde_woningwaarde,
    a.percentage_koopwoningen,
    a.percentage_huurwoningen,
    a.perc_huurwoningen_in_bezit_woningcorporaties,
    a.perc_huurwoningen_in_bezit_overige_verhuurders,
    a.percentage_woningen_met_eigendom_onbekend,
    a.percentage_bouwjaarklasse_tot_2000,
    a.percentage_bouwjaarklasse_vanaf_2000,
    a.gemiddeld_gasverbruik_appartement,
    a.gemiddeld_gasverbruik_tussenwoning,
    a.gemiddeld_gasverbruik_hoekwoning,
    a.gemiddeld_gasverbruik_2_onder_1_kap_woning,
    a.gemiddeld_gasverbruik_vrijstaande_woning,
    a.gemiddeld_gasverbruik_huurwoning,
    a.gemiddeld_gasverbruikkoopwoning,
    a.gemiddeld_elektriciteitsverbruik_totaal,
    a.gemiddeld_elektriciteitsverbruik_appartement,
    a.gemiddeld_elektriciteitsverbruik_tussenwoning,
    a.gemiddeld_elektriciteitsverbruik_hoekwoning,
    a.gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    a.gem_elektriciteitsverbruik_vrijstaande_woning,
    a.gemiddeld_elektriciteitsverbruik_huurwoning,
    a.gemiddeld_elektriciteitsverbruikkoopwoning,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_gem_2018
FROM
    cbs.gemeenten2018 a
    JOIN cte b ON a.gemeentecode = b.gemeentecode
WHERE
    a.water = 'NEE'
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_gem_2018
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_gem_2018_geom ON cbs.cdf_gem_2018 USING gist (geom);

CREATE INDEX cbs_cdf_gem_2018_geom_3857 ON cbs.cdf_gem_2018 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_gem_2018;

--ADD PROVINCIE CODE TO GEMEENTE DATA
-- -- Checks
-- SELECT count(*) FROM cbs.cdf_buurt_2018 WHERE NOT ST_IsValid(geom);
-- SELECT count(*) FROM cbs.cdf_buurt_2018 WHERE  ST_IsPolygonCCW(geom);
-- --  ST_ForcePolygonCCW
