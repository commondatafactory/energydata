/*
Import
*/
CREATE SCHEMA IF NOT EXISTS kookgas;

DROP TABLE IF EXISTS kookgas.aansluitingen CASCADE;

/*
manual import is done with after creating a csv from given excel email. Manual created column names in the csv for colunmns
with non present. The exel contains nice make up that does not translate nice to columns.

pgfutter --dbname cdf --username cdf --pass insecure csv source.csv --skip-header --delimiter ";" --null-delimiter "."

result is in:

    "import".woningen_met_kookgasaansluitingen_2020

*/

CREATE TABLE kookgas.aansluitingen AS
SELECT
	buurtnaam,
	'BU' || trim(buurtcode) as buurtcode,
	replace(trim(percentage_kookgaswoningen), '%', '') ::integer as percentage_kookgaswoningen
FROM
	"import".woningen_met_kookgasaansluitingen_2020 kga
;

DROP TABLE IF EXISTS kookgas.aansluitingen_buurt_2020 CASCADE;

CREATE TABLE kookgas.aansluitingen_buurt_2020 AS
SELECT
	--a.buurtnaam
	--,a.buurtcode
	a.percentage_kookgaswoningen
	,b.buurtnaam
	,b.buurtcode
	,b.geom
FROM kookgas.aansluitingen as a
LEFT JOIN cbs.cdf_buurt_2020 as b ON a.buurtcode = b.buurtcode
