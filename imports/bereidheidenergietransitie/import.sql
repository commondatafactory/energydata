/*
    Energiearmoede is from micro data of 2018 with borders regions of 2020
*/

CREATE SCHEMA IF NOT EXISTS bereidheid;

DROP TABLE IF EXISTS bereidheid.energietransitie CASCADE;

/*
manual import is done with after creating a csv from given excel email. Manual created column names in the csv for colunmns
with non present. The exel contains nice make up that does not translate nice to columns.

pgfutter --port 5433 --dbname cdf --username cdf  --pass insecure csv -d ';' Bereidheid\ Energietransitiemaatregelen\ 2018.csv


result is in:

    "import".tabel_wijk_buurt_*

*/


--CREATE TABLE bereidheid.energietransitie AS

select b.*, geom 
into bereidheid.energiemaatregelen_wijk
from (
SELECT
    gemeente as gemeente_naam,
    naam_van_de_regio as naam,
    soort_regio,
    CASE
       -- WHEN soort_regio = 'Gemeente' THEN replace(trim(regiocode), '-', '')
        WHEN soort_regio = 'Wijk' THEN replace(replace(trim(regiocode), '-', ''), 'WC', 'WK')
       -- WHEN soort_regio = 'Buurt' THEN replace(trim(regiocode), '-', '')
    END as regio_code,
    --regiocode,
    bereidmits,
    nietbereid

FROM
	import.bereidheid_energietransitiemaatregelen_2018 b
) b
left outer join
	--cbs.cbs_wijken_2021 cw on (cw.wijkcode = b.regio_code)
    tiles.admin_areas_2021_10_13 t on t.code  = regio_code 
where geom is not null and soort_regio = 'Wijk'


select b.*, geom 
into bereidheid.energiemaatregelen_gemeente
from (
SELECT
    gemeente as gemeente_naam,
    naam_van_de_regio as naam,
    soort_regio,
    CASE
        WHEN soort_regio = 'Gemeente' THEN replace(trim(regiocode), '-', '')
       -- WHEN soort_regio = 'Wijk' THEN replace(replace(trim(regiocode), '-', ''), 'WC', 'WK')
       -- WHEN soort_regio = 'Buurt' THEN replace(trim(regiocode), '-', '')
    END as regio_code,
    --regiocode,
    bereidmits,
    nietbereid
FROM
	import.bereidheid_energietransitiemaatregelen_2018 b
) b
left outer join
	--cbs.cbs_wijken_2021 cw on (cw.wijkcode = b.regio_code)
    tiles.admin_areas_2021_6_9 t on t.code  = regio_code 
where geom is not null and soort_regio = 'Gemeente'



select * from cbs.cbs_buurten_2021 cb ;
