CREATE SCHEMA IF NOT EXISTS bereidheid;

/* assumes pgfutter import of bereidheid-energiemaatregelen-2021.csv */


SELECT * FROM import.bereidheid_energietransitiemaatregelen_2021 be;


drop table bereidheid.energiemaatregelen_gemeente;

create table bereidheid.energiemaatregelen_2021 as (
SELECT
	gemeentenaam,
	regionaam,
	soort_regio,
	regiocode,
	type_eigendom,
	replace(huishoudens_aantal, ' ', '')::int as huishoudens_aantal,
	comfort_geisoleerd_indicatie,
	milieu_belangrijk_betalen,
	al_energiezuinig
FROM "import".bereidheid_energietransitiemaatregelen_2021
)

create index on bereidheid.energiemaatregelen_2021 (regiocode);

/* tegola sql preparation */

drop table bereidheid.energiemaatregelen_gemeente_2021;

create table bereidheid.energiemaatregelen_gemeente_2021 as (
SELECT
    --g.fid,
    name,
    code,

	gemeentenaam,
	regionaam,
	soort_regio,
	--regiocode,
	type_eigendom,
	huishoudens_aantal,
	comfort_geisoleerd_indicatie,
	milieu_belangrijk_betalen,
	al_energiezuinig,
	g.geom
FROM bereidheid.energiemaatregelen_2021 b
left outer join tiles.admin_areas_2022_6_9 g on g.code = b.regiocode
where b.soort_regio = 'Gemeente' and g.code is not null
)

alter table bereidheid.energiemaatregelen_gemeente_2021 add column fid serial primary key;

select * from bereidheid.energiemaatregelen_gemeente_2021 eg;

drop table bereidheid.energiemaatregelen_wijk_2021;

create table bereidheid.energiemaatregelen_wijk_2021 as (
SELECT
    --g.fid,
    name,
    code,
    regiocode,
	regionaam,
	soort_regio,
	--regiocode,
	type_eigendom,
	huishoudens_aantal,
	comfort_geisoleerd_indicatie,
	milieu_belangrijk_betalen,
	al_energiezuinig,
	g.geom
FROM bereidheid.energiemaatregelen_2021 b
left outer join tiles.admin_areas_2022_10_13 g on g.code = b.regiocode
where b.soort_regio = 'Wijk' and g.code is not null
)

select * from tiles.admin_labels_2022_10_13 al 
where code = 'WK049806';

alter table bereidheid.energiemaatregelen_wijk_2021 add column fid serial primary key;

select * from bereidheid.energiemaatregelen_wijk_2021;