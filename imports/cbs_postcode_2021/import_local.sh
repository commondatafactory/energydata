#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure


#psql -h ${PGHOST} -d ${PGDATABASE} -p ${PGPORT} -U ${PGUSER} -f "imports/cbs_2022/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get CBS source data if not already downloaded.
wget -nc -O rawdata/cbspostcode/CBS_pc6_2021.zip https://download.cbs.nl/postcode/2023-cbs_pc6_2021_v2.zip || true

# unzip
unzip -n rawdata/cbspostcode/CBS_pc6_2021.zip -d rawdata/cbspostcode

# ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" rawdata/WijkBuurtkaart_2022_v0.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs -lco OVERWRITE=YES

# Get pc6 cbs data data
# docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://geodata.nationaalgeoregister.nl/cbsprovincies/wfs?request=getFeature&service=wfs&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=cbsprovincies:cbsprovincies2012&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -nln provincies2012
# ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://service.pdok.nl/kadaster/bestuurlijkegebieden/wfs/v1_0?request=getFeature&service=WFS&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=bestuurlijkegebieden:Provinciegebied&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -lco SCHEMA=cbs -lco OVERWRITE=YES -nln  provincies2012

#Get spatial import datasets
ogr2ogr -f PostgreSQL "PG:host=${PGHOST} port=${PGPORT:-5432} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" ./rawdata/cbspostcode/cbs_pc6_2021_v2.gpkg -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -nlt MULTIPOLYGON -nlt CONVERT_TO_LINEAR -nlt PROMOTE_TO_MULTI -progress -lco SCHEMA=cbspostcode -lco OVERWRITE=YES


# Creating and merging cbs data
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./imports/cbs_postcode_2021/clean.sql" -v "ON_ERROR_STOP=1"
