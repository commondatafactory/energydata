/*
 * Create P6 postcode geo data from CBS P6 data. 
 * using building/pand geometries.
 * 
 */

/* create buffers around involved pc6 areas.*/
select 
	cpv.pc6, 
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	)
into cbspostcode.cbs_pc6_2020_buffer
from cbspostcode.cbs_pc6_2020_v1 cpv 
left outer join baghelp.pand_postcode pp on (pp.postcode = cpv.pc6)
left outer join bagv2_062022.pandactueelbestaand p on (p.identificatie = pp.pid)
group by pc6

create index on cbspostcode.cbs_pc6_2020_buffer (pc6);


/* combine pandend with pc6 data postcode */
select pp.pid, cpv.*
into cbspostcode.pand_cbs_p6
from cbspostcode.cbs_pc6_2020_v1 cpv
left outer join baghelp.pand_postcode pp on (pp.postcode = cpv.pc6)

create index on cbspostcode.pand_cbs_p6 (pid);