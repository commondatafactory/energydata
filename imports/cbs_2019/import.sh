#!/bin/bash
set -u
set -e
set -x

# Available connection Variables
# export PGDATABASE=cdf
# export PGHOST=database
# export PGPORT=5434
# export PGUSER=cdf
# export PGPASSWORD=insecure

docker-compose exec database psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "/import/cbs_2019/drop_tables.sql" -v "ON_ERROR_STOP=1"
# Get data application/json

# https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data/wijk-en-buurtkaart-2019
wget -nc -O rawdata/cbs_2019.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2019_v3.zip || true
unzip -n rawdata/cbs_2019.zip -d rawdata/


# Get spatial import datasets
docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST:-database} port=${PGPORT:-5432} user=${PGUSER:-cdf} dbname=${PGDATABASE:-cdf} password=${PGPASSWORD:-insecure}" /data/WijkBuurtkaart_2019_v3.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco SPATIAL_INDEX=OFF -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs

# Creating and merging cbs data
docker-compose exec database psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "/import/cbs_2019/cbs.sql" -v "ON_ERROR_STOP=1"
