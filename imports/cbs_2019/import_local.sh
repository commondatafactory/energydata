#!/bin/bash
set -u
set -e
set -x

# Available connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure
export CBSVERSION=3

psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./imports/cbs_2019/drop_tables.sql" -v "ON_ERROR_STOP=1"
# Get data application/json

# https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data/wijk-en-buurtkaart-2019
wget -nc -O rawdata/cbs_2019v${CBSVERSION}.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2019_v${CBSVERSION}.zip || true
unzip -n rawdata/cbs_2019v${CBSVERSION}.zip -d rawdata/

# Get spatial import datasets NOTE path changes on cbs version...
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST:-database} port=${PGPORT:-5432} user=${PGUSER:-cdf} dbname=${PGDATABASE:-cdf} password=${PGPASSWORD:-insecure}" ./rawdata/WijkBuurtkaart_2019_v3/WijkBuurtkaart_2019_v3.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs -lco OVERWRITE=YES


# Creating and merging cbs data
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./imports/cbs_2019/cbs.sql" -v "ON_ERROR_STOP=1"
