DROP table if exists cbs2021.cdf_buurt;
-- Buurt
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(
                    ST_MakeValid(ST_RemoveRepeatedPoints(geom)),
                    3
                )
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        buurtcode
    FROM
        cbs2021.buurten
    WHERE
        water = 'NEE'
    GROUP BY
        buurtcode
)
SELECT
    a.*,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs2021.cdf_buurt
FROM
    cbs2021.buurten a
    JOIN cte b ON a.buurtcode = b.buurtcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

/*
ALTER TABLE
cbs2021.cdf_buurt
ADD
    COLUMN fid SERIAL PRIMARY KEY;
*/

CREATE INDEX cbs_cdf_buurt_2021_geom ON cbs2021.cdf_buurt USING gist (geom);

CREATE INDEX cbs_cdf_buurt_2021_geom_3857 ON cbs2021.cdf_buurt USING gist (geom_3857);

VACUUM ANALYZE cbs2021.cdf_buurt;

DROP table if exists cbs2021.cdf_wijk;
-- Wijk
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        wijkcode
    FROM
        cbs2021.wijken
    WHERE
        water = 'NEE'
    GROUP BY
        wijkcode
)
SELECT
    a.*,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs2021.cdf_wijk
FROM
    cbs2021.wijken a
    JOIN cte b ON a.wijkcode = b.wijkcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";


/*
ALTER TABLE
    cbs2021.cdf_wijk
ADD
    COLUMN fid SERIAL PRIMARY KEY;
*/

CREATE INDEX cbs_cdf_wijk_2021_geom ON cbs2021.cdf_wijk USING gist (geom);

CREATE INDEX cbs_cdf_wijk_2021_geom_3857 ON cbs2021.cdf_wijk USING gist (geom_3857);

VACUUM ANALYZE cbs2021.cdf_wijk;

drop table if exists cbs2021.cdf_gem;
-- Gemeenten
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        gemeentecode
    FROM
        cbs2021.gemeenten
    WHERE
        water = 'NEE'
    GROUP BY
        gemeentecode
)
SELECT
    a.*,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs2021.cdf_gem
FROM
    cbs2021.gemeenten a
    JOIN cte b ON a.gemeentecode = b.gemeentecode
WHERE
    a.water = 'NEE'
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs2021.cdf_gem
ADD
    PRIMARY KEY (gemeentecode);

CREATE INDEX cbs_cdf_gem_2021_geom ON cbs2021.cdf_gem USING gist (geom);

CREATE INDEX cbs_cdf_gem_2021_geom_3857 ON cbs2021.cdf_gem USING gist (geom_3857);

VACUUM ANALYZE cbs2021.cdf_gem;
