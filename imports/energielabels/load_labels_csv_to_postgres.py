"""
Load energie labels csv into database
"""

import logging
import glob

import pandas as pd
# import os

import settings
import setup_db
import transform_data

log = logging.getLogger(__name__)


def load_csv(name, tabelName, replace=False):
    """
    Open csv file
    """

    with open(name) as csvfile:
        df = pd.read_csv(csvfile, sep=';')
        dtypedict = transform_data.get_columntypes(df.dtypes)
        engine = setup_db.get_engine()
        log.debug("labels csv data shape %s", df.shape)

        policy = 'append'
        if replace:
            policy = 'replace'

        df.to_sql(
            tabelName,
            engine,
            if_exists=policy,
            schema='labels',
            index=True,
            chunksize=15000,
            dtype=dtypedict,
        )


if __name__ == '__main__':
    geregistreerd = list(
      glob.glob(f'rawdata/energylabels/Geregistreerd*'))
    if len(geregistreerd) == 0:
      log.error('no csv files found..')

    # Registered enery labels
    for filename in geregistreerd:
       log.info('loading %s', filename)
       load_csv(filename, 'energielabel_definitief', replace=False)
       log.info(
           'Succesfully loaded into table energielabel_definitief: %s',
           filename)

    voorlopig = list(
        glob.glob(f'rawdata/energylabels/Voorlopig*'))

    if len(voorlopig) == 0:
        log.error('no csv files found..')

    # Generally calculated energy labels
    for filename in voorlopig:
        log.info('loading %s', filename)
        load_csv(filename, 'energielabel_voorlopig', replace=False)
        log.info(
            'Succesfully loaded into table energielabel_voorlopig: %s ',
            filename)
