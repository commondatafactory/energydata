"""
General settings.

logging.
"""

import logging

logging.basicConfig(
    # filename='application.log',
    level=logging.DEBUG,
    format='[%(asctime)s] %(levelname)-8s-{%(pathname)s:%(lineno)d} - %(message)s',  # noqa
    datefmt='%Y-%m-%d %H:%M:%S',
)
