#-------------------------------------------------------------------------------
# EpbdParser
#
# Python programma waarmee het totaalbestand van EPBD kan worden omgezet van
# xml naar csv. Het programma kan worden aangepast door de waarde InputFile en
# OutputFile naar de gewenste bestanden te laten wijzen.
#
# Voor het draaien van het programma is Python 2.7 nodig.
#
# Het programma dient als voorbeeld voor hoe de verwerking van zeer grote
# xml bestanden kan worden opgezet. Aan het gebruik van dit programma kunnen
# geen rechten worden ontleend.
#-------------------------------------------------------------------------------

import sys
import xml.sax

#-------------------------------------------------------------------------------
# Configuratie
#-------------------------------------------------------------------------------

InputFile = "D:\\folder\\v2016xx01\\v2016xx02.xml"
OutputFile = "D:\\folder\\v2016xx01\\v2016xx02.csv"
#-------------------------------------------------------------------------------
# EpbdErrorHandler
#-------------------------------------------------------------------------------
class EpbdErrorHandler(xml.sax.ErrorHandler):
    def error(self, exception):
        print(exception)

    def fatalError(self, exception):
        print(exception)

#-------------------------------------------------------------------------------
# EpbdContentHandler
#-------------------------------------------------------------------------------
class EpbdContentHandler(xml.sax.ContentHandler):
    Kolommen = ["PandVanMeting_postcode",
                "PandVanMeting_huisnummer",
                "PandVanMeting_huisnummer_toev",
                "PandVanMeting_bagverblijfsobjectid",
                "PandVanMeting_opnamedatum",
                "PandVanMeting_berekeningstype",
                "PandVanMeting_energieprestatieindex",
                "PandVanMeting_energieklasse",
                "PandVanMeting_gebouwklasse",
                "Meting_geldig_tot",
                "Pand_registratiedatum",
                "Pand_postcode",
                "Pand_huisnummer",
                "Pand_huisnummer_toev",
                "Pand_gebouwtype",
                "Pand_gebouwsubtype"]
    #---------------------------------------------------------------------------
    # aangeroepen bij de start van het document
    #---------------------------------------------------------------------------
    def startDocument(self):
        # gebruik het begin van het document om een csv bestand te openen en een
        # buffer aan te maken
        self.output = open(OutputFile, "w")
        # als deze vlag waar wordt dan wordt data weg geschreven
        self.isdata = False
        # deze waarde wordt gebruikt om te bepalen welk element nu verwerkt wordt
        # wordt gezet bij start element events, wordt gewist bij end element events
        self.current = ""
        # gebruik een dictionary object als buffer aangezien sommige tags niet altijd voorkomen
        self.data = {}
        for name in self.Kolommen:
            self.data[name] = ""
        buffer = ""
        # schrijf de namen van de kolommen (in de volgorde zoals ze voorkomen in de buffer)
        # als eerste regel weg in het csv bestand
        for name in self.data.iterkeys():
            buffer += name + "; "

        self.output.write(buffer + "\n")
        self.output.flush()

    #---------------------------------------------------------------------------
    # aangeroepen bij de start van een nieuwe tag
    #---------------------------------------------------------------------------
    def startElement(self, name, attrs):

        if (name == "LaatstVerwerkteMutatieVolgnummer"):
            pass
        elif (name in self.Kolommen):
            # alleen bij deze tags schrijven we data echt weg naar de csv file
            self.isdata = True
            self.current = name

        elif (name == "Pandcertificaat"):
            # begin van een nieuwe rij in het csv bestand
            # buffer opnieuw initialiseren
            self.buffer = ""

    #---------------------------------------------------------------------------
    # aangeroepen na lezen content van een tag
    #---------------------------------------------------------------------------
    def characters(self, content):
        # schrijf de waarde weg in de buffer indien het mag
        if (self.isdata):
            self.data[self.current] +=  content

    #---------------------------------------------------------------------------
    # aangeroepen bij het einde van een tag
    #---------------------------------------------------------------------------
    def endElement(self, name):
        if (name == "Pandcertificaat"):
            # zet alle data in een buffer
            buffer = ""
            for value in self.data.itervalues():
                buffer += value + "; "
            # print de buffer naar het scherm (visuele controle)
            # print(buffer)
            # schrijf de buffer weg naar de csv file
            self.output.write(buffer + "\n")
            self.output.flush()
            # initialiseer de buffer opnieuw door alle waardes leeg te maken
            for name in self.data.iterkeys():
                self.data[name] = ""
        # na sluiten van een tag altijd de current waarde leeg maken
        self.current = ""
        # na sluiten van een tag altijd de vlag voor wegschrijven van data uitzetten
        self.isdata = False

    #---------------------------------------------------------------------------
    # aangeroepen bij het einde van het document
    #---------------------------------------------------------------------------
    def endDocument(self):
        # gebruik het einde van het document om het csv bestand te sluiten
        self.output.close()

#-------------------------------------------------------------------------------
# start programma
#-------------------------------------------------------------------------------
def main():
    # parser object aanmaken
    parser = xml.sax.make_parser()
    # voeg objecten toe voor verwerking van de tags en error afhandeling
    parser.setContentHandler(EpbdContentHandler())
    parser.setErrorHandler(EpbdErrorHandler())
    # parse het bron bestand
    f = open(InputFile, "r",0)
    src = xml.sax.xmlreader.InputSource()
    src.setByteStream(f)
    src.setEncoding("UTF-8")
    parser.parse(src)

if __name__ == '__main__':
    main()
