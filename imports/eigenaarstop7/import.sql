-- CREATE SCHEMA IF NOT EXISTS kookgas;

-- DROP TABLE IF EXISTS kookgas.aansluitingen CASCADE;

/*
manual import is done with after creating a csv from given excel email. Manual created column names in the csv for colunmns
with non present. The exel contains nice make up that does not translate nice to columns.

pgfutter --dbname cdf --username cdf --pass insecure csv source.csv --skip-header --delimiter ";" --null-delimiter "."

import result is in:

    "import".woningen_met_kookgasaansluitingen_2022 or (source name).

*/


/*
grooteigenaarpct
wijk_code
wijk_naam
aantal_vbo_buurt
aantal_ger_buurt
naam_1
aantal_vbo_1
naam_2
aantal_vbo_2
naam_3
aantal_vbo_3
naam_4
aantal_vbo_4
naam_5
aantal_vbo_5
naam_6
aantal_vbo_6
naam_7
aantal_vbo_7
wijknaam
wijkcode
*/

DROP TABLE IF EXISTS eigenarentop7.buurten2022 CASCADE;

CREATE TABLE eigenarentop7.buurten2022 AS
select
    ((a.aantal_vbo_1::float + a.aantal_vbo_2::float + a.aantal_vbo_3::float + a.aantal_vbo_4::float + a.aantal_vbo_5::float + a.aantal_vbo_6::float + a.aantal_vbo_7::float) /  a.aantal_vbo_buurt::float * 100) ::int as grooteigenaarpct
        ,a.aantal_vbo_buurt
        ,a.aantal_ger_buurt
        ,a.naam_1
        ,a.aantal_vbo_1
        ,a.naam_2
        ,a.aantal_vbo_2
        ,a.naam_3
        ,a.aantal_vbo_3
        ,a.naam_4
        ,a.aantal_vbo_4
        ,a.naam_5
        ,a.aantal_vbo_5
        ,a.naam_6
        ,a.aantal_vbo_6
        ,a.naam_7
        ,a.aantal_vbo_7

	,w.buurtnaam
	,w.buurtcode
	,w.geom
	,w.fid
FROM "import".vng_top7_buurten a
LEFT JOIN cbs.cbs_buurten_2021 w ON a.buurt_code = w.buurtcode
where w.buurtcode is not null and w.water = 'NEE';

select count(*) from eigenarentop7.buurten2022;
select count(*) from import.vng_top7_buurten;


DROP TABLE IF EXISTS eigenarentop7.wijken2022 CASCADE;

CREATE TABLE eigenarentop7.wijken2022 AS
select
    ((a.aantal_vbo_1::float + a.aantal_vbo_2::float + a.aantal_vbo_3::float + a.aantal_vbo_4::float + a.aantal_vbo_5::float + a.aantal_vbo_6::float + a.aantal_vbo_7::float) /  a.aantal_vbo_buurt::float * 100) ::int as grooteigenaarpct
        ,a.aantal_vbo_buurt
        ,a.aantal_ger_buurt
        ,a.naam_1
        ,a.aantal_vbo_1
        ,a.naam_2
        ,a.aantal_vbo_2
        ,a.naam_3
        ,a.aantal_vbo_3
        ,a.naam_4
        ,a.aantal_vbo_4
        ,a.naam_5
        ,a.aantal_vbo_5
        ,a.naam_6
        ,a.aantal_vbo_6
        ,a.naam_7
        ,a.aantal_vbo_7

	,w.wijknaam
	,w.wijkcode
	,w.geom
	,w.fid
FROM "import".vng_top7_wijken a
LEFT JOIN cbs.cbs_wijken_2021 w ON a.wijk_code = w.wijkcode
where w.wijkcode is not null and w.water = 'NEE';


DROP TABLE IF EXISTS eigenarentop7.wijken2022 CASCADE;

CREATE TABLE eigenarentop7.wijken2022 AS
select
    ((a.aantal_vbo_1::float + a.aantal_vbo_2::float + a.aantal_vbo_3::float + a.aantal_vbo_4::float + a.aantal_vbo_5::float + a.aantal_vbo_6::float + a.aantal_vbo_7::float) /  a.aantal_vbo_buurt::float * 100) ::int as grooteigenaarpct
        ,a.aantal_vbo_buurt
        ,a.aantal_ger_buurt
        ,a.naam_1
        ,a.aantal_vbo_1
        ,a.naam_2
        ,a.aantal_vbo_2
        ,a.naam_3
        ,a.aantal_vbo_3
        ,a.naam_4
        ,a.aantal_vbo_4
        ,a.naam_5
        ,a.aantal_vbo_5
        ,a.naam_6
        ,a.aantal_vbo_6
        ,a.naam_7
        ,a.aantal_vbo_7

	,w.wijknaam
	,w.wijkcode
	,w.geom
	,w.fid
FROM "import".vng_top7_wijken a
LEFT JOIN cbs.cbs_wijken_2021 w ON a.wijk_code = w.wijkcode
where w.wijkcode is not null and w.water = 'NEE';

-- check.
SELECT count(*) from "import".vng_top7_wijken;
SELECT count(*) from eigenarentop7.wijken2022 w;


DROP TABLE IF EXISTS eigenarentop7.gemeenten2022 CASCADE;

CREATE TABLE eigenarentop7.gemeenten2022 AS
select
    ((a.aantal_vbo_1::float + a.aantal_vbo_2::float + a.aantal_vbo_3::float + a.aantal_vbo_4::float + a.aantal_vbo_5::float + a.aantal_vbo_6::float + a.aantal_vbo_7::float) /  a.aantal_vbo_gemeente::float * 100) ::int as grooteigenaarpct
        ,a.aantal_vbo_gemeente
        ,a.aantal_ger_gemeente
        ,a.naam_1
        ,a.aantal_vbo_1
        ,a.naam_2
        ,a.aantal_vbo_2
        ,a.naam_3
        ,a.aantal_vbo_3
        ,a.naam_4
        ,a.aantal_vbo_4
        ,a.naam_5
        ,a.aantal_vbo_5
        ,a.naam_6
        ,a.aantal_vbo_6
        ,a.naam_7
        ,a.aantal_vbo_7

        ,g.gemeentenaam
        ,g.gemeentecode
        ,g.geom
        ,g.fid

FROM "import".vng_top7_gemeenten a
LEFT JOIN cbs.gemeenten2021 g ON a.gemeente_code = g.gemeentecode
where g.gemeentecode is not null and g.water = 'NEE';

select count(*) from "import".vng_top7_gemeenten;
select count(*) from eigenarentop7.gemeenten2022 g;


--select * from "import".vng_top7_gemeenten g where  g.gemeente_code not in (select g2.gemeentecode from cbs.gemeenten2022 g2)
