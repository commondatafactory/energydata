#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=127.0.0.1
export PGPORT=5433
export PGUSER=cdf
export PGPASSWORD=insecure


# Creating and merging cbs data
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./imports/eigenaarstop7/import.sql" -v "ON_ERROR_STOP=1"
