-- Buurt
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(
                    ST_MakeValid(ST_RemoveRepeatedPoints(geom)),
                    3
                )
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        buurtcode
    FROM
        cbs.cbs_buurten_2020
    WHERE
        water = 'NEE'
    GROUP BY
        buurtcode
)
SELECT
    -- id SERIAL as fid, ??
    a.buurtcode,
    a.buurtnaam,
    a.wijkcode,
    -- a.wijknaam,
    a.gemeentecode,
    a.gemeentenaam,
    a.indelingswijziging_wijken_en_buurten,
    a.water,
    a.meest_voorkomende_postcode,
    a.dekkingspercentage,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.mannen,
    a.vrouwen,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_ongehuwd,
    a.percentage_gehuwd,
    a.percentage_gescheid,
    a.percentage_verweduwd,
    a.aantal_huishoudens,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_westerse_migratieachtergrond,
    a.percentage_niet_westerse_migratieachtergrond,
    a.percentage_uit_marokko,
    a.percentage_uit_nederlandse_antillen_en_aruba,
    a.percentage_uit_suriname,
    a.percentage_uit_turkije,
    a.percentage_overige_nietwestersemigratieachtergrond,
    a.oppervlakte_totaal_in_ha,
    a.oppervlakte_land_in_ha,
    a.oppervlakte_water_in_ha,

    aantal_bedrijven_landbouw_bosbouw_visserij,
    aantal_bedrijven_nijverheid_energie,
    aantal_bedrijven_handel_en_horeca,
    aantal_bedrijven_vervoer_informatie_communicatie,
    aantal_bedrijven_financieel_onroerend_goed,
    aantal_bedrijven_zakelijke_dienstverlening,
    aantal_bedrijven_cultuur_recreatie_overige,
    aantal_bedrijfsvestigingen,

    woningvoorraad,
    gemiddelde_woningwaarde,
    percentage_eengezinswoning,
    percentage_meergezinswoning,
    percentage_bewoond,
    percentage_koopwoningen,
    percentage_huurwoningen,
    perc_huurwoningen_in_bezit_woningcorporaties,
    perc_huurwoningen_in_bezit_overige_verhuurders,
    percentage_woningen_met_eigendom_onbekend,
    percentage_bouwjaarklasse_tot_2000,
    percentage_bouwjaarklasse_vanaf_2000,
    percentage_leegstand_woningen,

    gemiddeld_gasverbruik_totaal,
    gemiddeld_gasverbruik_appartement,
    gemiddeld_gasverbruik_tussenwoning,
    gemiddeld_gasverbruik_hoekwoning,
    gemiddeld_gasverbruik_2_onder_1_kap_woning,
    gemiddeld_gasverbruik_vrijstaande_woning,
    gemiddeld_gasverbruik_huurwoning,
    gemiddeld_gasverbruikkoopwoning,
    gemiddeld_elektriciteitsverbruik_totaal,
    gemiddeld_elektriciteitsverbruik_appartement,
    gemiddeld_elektriciteitsverbruik_tussenwoning,
    gemiddeld_elektriciteitsverbruik_hoekwoning,
    gem_elektriciteitsverbruik_2_onder_1_kap_woning,
    gem_elektriciteitsverbruik_vrijstaande_woning,
    gemiddeld_elektriciteitsverbruik_huurwoning,
    gemiddeld_elektriciteitsverbruikkoopwoning,

    aantal_personen_met_een_ao_uitkering_totaal,
    aantal_personen_met_een_ww_uitkering_totaal,
    aantal_personen_met_een_algemene_bijstandsuitkering_totaal,
    aantal_personen_met_een_aow_uitkering_totaal,
    personenautos_totaal,
    personenautos_per_huishouden,
    personenautos_per_km2,
    motortweewielers_totaal,
    aantal_personenautos_met_brandstof_benzine,
    aantal_personenautos_met_overige_brandstof,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_buurt_2020
FROM
    cbs.cbs_buurten_2020 a
    JOIN cte b ON a.buurtcode = b.buurtcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

/*
ALTER TABLE
    cbs.cdf_buurt_2020
ADD
    COLUMN fid SERIAL PRIMARY KEY;
*/

CREATE INDEX cbs_cdf_buurt_2020_geom ON cbs.cdf_buurt_2020 USING gist (geom);

CREATE INDEX cbs_cdf_buurt_2020_geom_3857 ON cbs.cdf_buurt_2020 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_buurt_2020;

-- Wijk
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        wijkcode
    FROM
        cbs.cbs_wijken_2020
    WHERE
        water = 'NEE'
    GROUP BY
        wijkcode
)
SELECT
    a.wijkcode,
    a.wijknaam,
    a.gemeentecode,
    a.gemeentenaam,
    a.indelingswijziging_wijken_en_buurten,
    a.water,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.mannen,
    a.vrouwen,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_ongehuwd,
    a.percentage_gehuwd,
    a.percentage_gescheid,
    a.percentage_verweduwd,
    a.aantal_huishoudens,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_westerse_migratieachtergrond,
    a.percentage_niet_westerse_migratieachtergrond,
    a.percentage_uit_marokko,
    a.percentage_uit_nederlandse_antillen_en_aruba,
    a.percentage_uit_suriname,
    a.percentage_uit_turkije,
    a.percentage_overige_nietwestersemigratieachtergrond,
    a.oppervlakte_totaal_in_ha,
    a.oppervlakte_land_in_ha,
    a.oppervlakte_water_in_ha,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_wijk_2020
FROM
    cbs.cbs_wijken_2020 a
    JOIN cte b ON a.wijkcode = b.wijkcode
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_wijk_2020
ADD
    COLUMN fid SERIAL PRIMARY KEY;

CREATE INDEX cbs_cdf_wijk_2020_geom ON cbs.cdf_wijk_2020 USING gist (geom);

CREATE INDEX cbs_cdf_wijk_2020_geom_3857 ON cbs.cdf_wijk_2020 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_wijk_2020;

-- Gemeenten
WITH cte AS (
    SELECT
        ST_Multi(
            ST_Union(
                ST_CollectionExtract(ST_MakeValid(ST_RemoveRepeatedPoints(geom)), 3)
            )
        ) :: geometry(MultiPolygon, 28992) AS geom,
        gemeentecode
    FROM
        cbs.gemeenten2020
    WHERE
        water = 'NEE'
    GROUP BY
        gemeentecode
)
SELECT
    a.gemeentecode,
    a.gemeentenaam,
    a.water,
    a.omgevingsadressendichtheid,
    a.stedelijkheid_adressen_per_km2,
    a.bevolkingsdichtheid_inwoners_per_km2,
    a.aantal_inwoners,
    a.mannen,
    a.vrouwen,
    a.percentage_personen_0_tot_15_jaar,
    a.percentage_personen_15_tot_25_jaar,
    a.percentage_personen_25_tot_45_jaar,
    a.percentage_personen_45_tot_65_jaar,
    a.percentage_personen_65_jaar_en_ouder,
    a.percentage_ongehuwd,
    a.percentage_gehuwd,
    a.percentage_gescheid,
    a.percentage_verweduwd,
    a.aantal_huishoudens,
    a.percentage_eenpersoonshuishoudens,
    a.percentage_huishoudens_zonder_kinderen,
    a.percentage_huishoudens_met_kinderen,
    a.gemiddelde_huishoudsgrootte,
    a.percentage_westerse_migratieachtergrond,
    a.percentage_niet_westerse_migratieachtergrond,
    a.percentage_uit_marokko,
    a.percentage_uit_nederlandse_antillen_en_aruba,
    a.percentage_uit_suriname,
    a.percentage_uit_turkije,
    a.percentage_overige_nietwestersemigratieachtergrond,
    a.oppervlakte_totaal_in_ha,
    a.oppervlakte_land_in_ha,
    a.oppervlakte_water_in_ha,
    b.geom,
    ST_Transform(b.geom, 3857) :: geometry(MultiPolygon, 3857) as geom_3857
INTO cbs.cdf_gem_2020
FROM
    cbs.gemeenten2020 a
    JOIN cte b ON a.gemeentecode = b.gemeentecode
WHERE
    a.water = 'NEE'
ORDER BY
    ST_GeoHash(ST_Transform(ST_Envelope(b.geom), 4326), 6) COLLATE "C";

ALTER TABLE
    cbs.cdf_gem_2020
ADD
    PRIMARY KEY (gemeentecode);

CREATE INDEX cbs_cdf_gem_2020_geom ON cbs.cdf_gem_2020 USING gist (geom);

CREATE INDEX cbs_cdf_gem_2020_geom_3857 ON cbs.cdf_gem_2020 USING gist (geom_3857);

VACUUM ANALYZE cbs.cdf_gem_2020;

--ADD PROVINCIE CODE TO GEMEENTE DATA
-- -- Checks
-- SELECT count(*) FROM cbs.cdf_buurt_2020 WHERE NOT ST_IsValid(geom);
-- SELECT count(*) FROM cbs.cdf_buurt_2020 WHERE  ST_IsPolygonCCW(geom);
-- --  ST_ForcePolygonCCW
