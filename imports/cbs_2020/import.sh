#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=database
export PGPORT=5434
export PGUSER=cdf
export PGPASSWORD=insecure

docker-compose exec database psql -h database -d cdf -p 5432 -U cdf -f "/import/cbs_2020/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get data application/json if not already downloaded.
wget -nc -O rawdata/cbs_2020v2.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2020_v2.zip || true

# unzip if not already unzipped
unzip -n rawdata/cbs_2020v2.zip -d rawdata/


#Get spatial import datasets
docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" /data/WijkBuurtkaart_2020_v2.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco SPATIAL_INDEX=OFF -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs

# Get province data
# docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://geodata.nationaalgeoregister.nl/cbsprovincies/wfs?request=getFeature&service=wfs&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=cbsprovincies:cbsprovincies2012&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco SPATIAL_INDEX=OFF -lco PRECISION=NO -lco SCHEMA=cbs -nln provincies2012

docker-compose run gdal ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST} user=${PGUSER} dbname=${PGDATABASE} password=${PGPASSWORD}" 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wfs?request=getFeature&service=wfs&outputFormat=application/json&srs=urn:ogc:def:crs:EPSG::28992&version=2.0.0&typeName=bestuurlijkegrenzen:provincies&propertyName=*' -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco SPATIAL_INDEX=OFF -lco PRECISION=NO -lco SCHEMA=cbs -nln provincies2012

# Creating and merging cbs data
docker-compose exec database psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "/import/cbs_2020/cbs.sql" -v "ON_ERROR_STOP=1"
