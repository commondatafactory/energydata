/* clean null values from table. */


update cbspostcode.cbs_pc6_2023
set
    aantal_inwoners = NULLIF(aantal_inwoners, -99997),
    aantal_mannen = NULLIF(aantal_mannen, -99997),
    aantal_vrouwen = NULLIF(aantal_vrouwen, -99997),
    aantal_inwoners_0_tot_15_jaar = NULLIF(aantal_inwoners_0_tot_15_jaar, -99997),
    aantal_inwoners_15_tot_25_jaar = NULLIF(aantal_inwoners_15_tot_25_jaar, -99997),
    aantal_inwoners_25_tot_45_jaar = NULLIF(aantal_inwoners_25_tot_45_jaar, -99997),
    aantal_inwoners_45_tot_65_jaar = NULLIF(aantal_inwoners_45_tot_65_jaar, -99997),
    aantal_inwoners_65_jaar_en_ouder = NULLIF(aantal_inwoners_65_jaar_en_ouder, -99997),
    percentage_geb_nederland_herkomst_nederland = NULLIF(percentage_geb_nederland_herkomst_nederland, -99997),
    percentage_geb_nederland_herkomst_overig_europa = NULLIF(percentage_geb_nederland_herkomst_overig_europa, -99997),
    percentage_geb_nederland_herkomst_buiten_europa = NULLIF(percentage_geb_nederland_herkomst_buiten_europa, -99997),
    percentage_geb_buiten_nederland_herkomst_europa = NULLIF(percentage_geb_buiten_nederland_herkomst_europa, -99997),
    percentage_geb_buiten_nederland_herkmst_buiten_europa = NULLIF(percentage_geb_buiten_nederland_herkmst_buiten_europa, -99997),
    aantal_part_huishoudens = NULLIF(aantal_part_huishoudens, -99997),
    aantal_eenpersoonshuishoudens = NULLIF(aantal_eenpersoonshuishoudens, -99997),
    aantal_meerpersoonshuishoudens_zonder_kind = NULLIF(aantal_meerpersoonshuishoudens_zonder_kind, -99997),
    aantal_eenouderhuishoudens = NULLIF(aantal_eenouderhuishoudens, -99997),
    aantal_tweeouderhuishoudens = NULLIF(aantal_tweeouderhuishoudens, -99997),
    gemiddelde_huishoudensgrootte = NULLIF(gemiddelde_huishoudensgrootte, -99997),
    aantal_woningen = NULLIF(aantal_woningen, -99997),
    aantal_woningen_bouwjaar_voor_1945 = NULLIF(aantal_woningen_bouwjaar_voor_1945, -99997),
    aantal_woningen_bouwjaar_45_tot_65 = NULLIF(aantal_woningen_bouwjaar_45_tot_65, -99997),
    aantal_woningen_bouwjaar_65_tot_75 = NULLIF(aantal_woningen_bouwjaar_65_tot_75, -99997),
    aantal_woningen_bouwjaar_75_tot_85 = NULLIF(aantal_woningen_bouwjaar_75_tot_85, -99997),
    aantal_woningen_bouwjaar_85_tot_95 = NULLIF(aantal_woningen_bouwjaar_85_tot_95, -99997),
    aantal_woningen_bouwjaar_95_tot_05 = NULLIF(aantal_woningen_bouwjaar_95_tot_05, -99997),
    aantal_woningen_bouwjaar_05_tot_15 = NULLIF(aantal_woningen_bouwjaar_05_tot_15, -99997),
    aantal_woningen_bouwjaar_15_en_later = NULLIF(aantal_woningen_bouwjaar_15_en_later, -99997),
    aantal_meergezins_woningen = NULLIF(aantal_meergezins_woningen, -99997),
    percentage_koopwoningen = NULLIF(percentage_koopwoningen, -99997),
    percentage_huurwoningen = NULLIF(percentage_huurwoningen, -99997),
    aantal_huurwoningen_in_bezit_woningcorporaties = NULLIF(aantal_huurwoningen_in_bezit_woningcorporaties, -99997),
    aantal_niet_bewoonde_woningen = NULLIF(aantal_niet_bewoonde_woningen, -99997),
    gemiddelde_woz_waarde_woning = NULLIF(gemiddelde_woz_waarde_woning, -99997),
    aantal_personen_met_uitkering_onder_aowlft = NULLIF(aantal_personen_met_uitkering_onder_aowlft, -99997);


create index on cbspostcode.cbs_pc6_2023 (postcode);
