drop table if exists export.cbs_100_2019;

select
    *,
    ST_Transform(g.geom, 3857) :: geometry(Polygon, 3857) as geom_3857
into export.cbs_100_2019
from cbs.cbs_vk100_2019 g;

create index on export.cbs_100_2019 (fid);
create index on export.cbs_100_2019 using gist (geom_3857);


drop table if exists export.cbs_500_2019;

select
    *,
    ST_Transform(g.geom, 3857) :: geometry(Polygon, 3857) as geom_3857
into export.cbs_500_2019
from cbs.cbs_vk500_2019 g;

create index on export.cbs_500_2019 (fid);
create index on export.cbs_500_2019 using gist (geom_3857);
