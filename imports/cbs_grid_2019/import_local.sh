#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE=cdf
export PGHOST=database
export PGPORT=5432
export PGUSER=cdf
export PGPASSWORD=insecure

psql -h ${PGHOST} -d ${PGDATABASE} -p 5432 -U cdf -f "./imports/cbs_grid_2020/drop_tables.sql" -v "ON_ERROR_STOP=1"

# Get data application/json if not already downloaded.
wget -nc -O rawdata/cbs/cbs_500_2019.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/vierkanten/500/2021-cbs_vk500_2019_v2.zip || true
wget -nc -O rawdata/cbs/cbs_100_2019.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/vierkanten/100/2021-cbs_vk100_2019_v2.zip || true

# Unzip if not already unzipped
unzip -n rawdata/cbs/cbs_500_2019.zip -d rawdata/cbs/500/
unzip -n rawdata/cbs/cbs_100_2019.zip -d rawdata/cbs/100/

unzip -n rawdata/cbs/500/cbs_vk500_2019_v2-geopackage.zip -d rawdata/cbs/500
unzip -n rawdata/cbs/100/cbs_vk100_2019_v2-geopackage.zip -d rawdata/cbs/100

# Get spatial import datasets
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST:-database} port=${PGPORT:-5432} user=${PGUSER:-cdf} dbname=${PGDATABASE:-cdf} password=${PGPASSWORD:-insecure}" rawdata/cbs/500/cbs_vk500_2019.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco OVERWRITE=YES -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs
ogr2ogr -f "PostgreSQL" PG:"host=${PGHOST:-database} port=${PGPORT:-5432} user=${PGUSER:-cdf} dbname=${PGDATABASE:-cdf} password=${PGPASSWORD:-insecure}" rawdata/cbs/100/cbs_vk100_2019.gpkg -skipfailures -overwrite --config PG_USE_COPY YES -gt 65536 -lco GEOMETRY_NAME=geom -lco OVERWRITE=YES -lco PRECISION=NO -nlt POLYGON -lco SCHEMA=cbs

# Creating 3857 cbs data
psql -h ${PGHOST:-database} -d ${PGDATABASE:-cdf} -p ${PGPORT:-5432} -U ${PGUSER:-cdf} -f "./imports/cbs_grid_2019/grid.sql" -v "ON_ERROR_STOP=1"
